/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui

import de.localtoast.palaver.Settings
import de.localtoast.palaver.gui.chatroom.ChatRoomPanel
import de.localtoast.palaver.gui.login.LoginCredentialsDialog
import de.localtoast.palaver.gui.sidebar.ContactPane
import de.localtoast.palaver.gui.sidebar.SettingsListView
import de.localtoast.palaver.gui.stylesheet.PalaverStyle
import de.localtoast.palaver.gui.stylesheet.SideBarStyle
import de.localtoast.palaver.sys.SyncController
import de.localtoast.palaver.sys.login.MatrixConnector
import io.kamax.matrix.client.MatrixClientRequestException
import javafx.application.Platform
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.image.Image
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseEvent
import javafx.scene.layout.HBox
import javafx.scene.layout.Pane
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import org.slf4j.LoggerFactory
import tornadofx.View
import tornadofx.addClass
import tornadofx.box
import tornadofx.get
import tornadofx.hbox
import tornadofx.hboxConstraints
import tornadofx.imageview
import tornadofx.px
import tornadofx.stackpane
import tornadofx.style
import tornadofx.vbox
import tornadofx.vboxConstraints

class MainWindow : View() {
    private val contactPane: ContactPane by inject()
    private val chatRoomPanel: ChatRoomPanel by inject()

    private val settingsListView: SettingsListView
    private val settingsMainArea = HBox()

    private val loginDialog: LoginCredentialsDialog by inject()
    private val matrixConnector: MatrixConnector by inject()
    private val uiController: UiController by inject()
    private val inputListener: InputListener by inject()

    private val logoPane = LogoPane()

    private val logger = LoggerFactory.getLogger(this.javaClass.name)

    /*
     * Pane without layout for freely positioning overlay windows.
     */
    val overlayPane = Pane().apply {
        isPickOnBounds = false
    }

    init {
        settingsListView = find(mapOf(SettingsListView::settingsMainArea to settingsMainArea))
    }

    val mainView = stackpane {
        vboxConstraints {
            vGrow = Priority.ALWAYS
        }

        val sidebar = VBox().apply {
            addClass(SideBarStyle.paneBackground)
            alignment = Pos.TOP_LEFT

            vboxConstraints {
                vGrow = Priority.ALWAYS
            }

            add(logoPane)
        }

        val mainArea = HBox().apply {
            hboxConstraints {
                hGrow = Priority.ALWAYS
            }
            add(chatRoomPanel.root)
        }

        val toggleButtonRight = HBox()

        val elementWidth = 70.0

        val toggleButtonLeft = HBox().apply {
            style {
                padding = box(10.px, 0.px, 0.px, 0.px)
            }
            alignment = Pos.CENTER
            hbox {
                prefWidth = elementWidth * 3
                minWidth = elementWidth * 3
                maxWidth = elementWidth * 3

                vbox(3.0) {
                    alignment = Pos.CENTER
                    imageview(Image("/de/localtoast/palaver/icons/actions/material.io/twotone_chat_white_24dp.png"))

                    prefWidth = elementWidth
                    minWidth = elementWidth
                    maxWidth = elementWidth
                }

                hbox {
                    imageview {
                        image = Image("/de/localtoast/palaver/gui/toggleButton_left.png")
                        setOnMousePressed {
                            moveToggleToRight(sidebar, mainArea, toggleButtonRight)
                        }
                    }
                    prefWidth = elementWidth
                    minWidth = elementWidth
                    maxWidth = elementWidth
                }

                vbox(3.0) {
                    alignment = Pos.CENTER

                    imageview(Image("/de/localtoast/palaver/icons/actions/material.io/twotone_settings_white_24dp.png")) {
                        opacity = 0.4
                    }

                    prefWidth = elementWidth
                    minWidth = elementWidth
                    maxWidth = elementWidth
                    setOnMousePressed {
                        moveToggleToRight(sidebar, mainArea, toggleButtonRight)
                    }
                }
            }
        }

        toggleButtonRight.apply {
            style {
                padding = box(10.px, 0.px, 0.px, 0.px)
            }
            alignment = Pos.CENTER
            hbox {
                prefWidth = elementWidth * 3
                minWidth = elementWidth * 3
                maxWidth = elementWidth * 3

                vbox(3.0) {
                    alignment = Pos.CENTER

                    imageview(Image("/de/localtoast/palaver/icons/actions/material.io/twotone_chat_white_24dp.png")) {
                        opacity = 0.4
                    }

                    prefWidth = elementWidth
                    minWidth = elementWidth
                    maxWidth = elementWidth
                    setOnMousePressed {
                        moveToggleToLeft(sidebar, mainArea, toggleButtonLeft)
                    }
                }
                hbox {
                    imageview {
                        image = Image("/de/localtoast/palaver/gui/toggleButton_right.png")
                        setOnMousePressed {
                            moveToggleToLeft(sidebar, mainArea, toggleButtonLeft)
                        }
                        prefWidth = elementWidth
                        minWidth = elementWidth
                        maxWidth = elementWidth
                    }
                }
                vbox(3.0) {
                    alignment = Pos.CENTER

                    imageview(Image("/de/localtoast/palaver/icons/actions/material.io/twotone_settings_white_24dp.png"))

                    prefWidth = elementWidth
                    minWidth = elementWidth
                    maxWidth = elementWidth
                }
            }
        }

        sidebar.add(toggleButtonLeft)
        sidebar.add(contactPane.root)

        hbox {
            add(sidebar)
            add(mainArea)
        }

        add(overlayPane)
    }

    private fun moveToggleToRight(sidebar: VBox, mainArea: HBox, toggleButtonRight: Node) {
        sidebar.children.clear()
        mainArea.children.clear()

        sidebar.children.add(logoPane)
        sidebar.children.add(toggleButtonRight)
        sidebar.children.add(settingsListView.root)
        mainArea.children.add(settingsMainArea)
    }

    private fun moveToggleToLeft(sidebar: VBox, mainArea: HBox, toggleButtonLeft: Node) {
        sidebar.children.clear()
        mainArea.children.clear()

        sidebar.children.add(logoPane)
        sidebar.children.add(toggleButtonLeft)
        sidebar.children.add(contactPane.root)
        mainArea.children.add(chatRoomPanel.root)
    }

    override val root = vbox {
        title = "Palaver"
        hboxConstraints {
            hGrow = Priority.ALWAYS
        }
        addClass(PalaverStyle.mainWindow)
        add(mainView)

        addEventFilter(MouseEvent.ANY) {
            Platform.runLater {
                inputListener.input()
            }
            if (it.eventType == MouseEvent.MOUSE_PRESSED && overlayPane.children.isNotEmpty()) {
                val child = overlayPane.children.first()
                if (!child.localToScene(child.layoutBounds).contains(it.sceneX, it.sceneY)) {
                    // click outside of the readMarkerOverlay panel
                    overlayPane.children.clear()
                }
            }
        }

        addEventFilter(KeyEvent.KEY_PRESSED) {
            inputListener.input()
        }
    }

    fun initConnection() {
        val token = Settings.accessToken
        if (token.isNotEmpty()) {
            val progressIndicator = uiController.createProgressIndicator(messages["connectingToServer"])
            uiController.showProgressCircle(mainView, progressIndicator)
            runAsync(daemon = true) {
                try {
                    matrixConnector.initializeFromSettings()
                } catch (e: IllegalArgumentException) {
                    logger.debug("Can't use login data provided by settings file", e)
                    Platform.runLater {
                        mainView.add(loginDialog.root)
                    }
                }
                val syncSuccessful = try {
                    tornadofx.find<SyncController>().startSyncing()
                } catch (e: MatrixClientRequestException) {
                    logger.error("Something went wrong during the initial sync", e)
                    false
                }
                if (!syncSuccessful) {
                    logger.error("First sync was not successful")
                    Platform.runLater {
                        mainView.add(loginDialog.root)
                    }
                }
            } ui {
                uiController.hideProgressCircle(mainView, progressIndicator, { contactPane.selectInitialItem() })
            }
        } else {
            mainView.add(loginDialog.root)
        }
    }
}
