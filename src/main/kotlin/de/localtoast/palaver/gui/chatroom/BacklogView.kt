/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom

import de.localtoast.palaver.gui.InputListener
import de.localtoast.palaver.gui.OverlayWithPointer
import de.localtoast.palaver.gui.chatroom.backlog.BacklogCell
import de.localtoast.palaver.gui.chatroom.backlog.BacklogCellFragment
import de.localtoast.palaver.gui.chatroom.backlog.BacklogRow
import de.localtoast.palaver.gui.chatroom.backlog.EventRow
import de.localtoast.palaver.gui.chatroom.backlog.SelectionIndex
import de.localtoast.palaver.gui.chatroom.backlog.SelectionManager
import de.localtoast.palaver.gui.stylesheet.ChatRoomStyle
import de.localtoast.palaver.gui.text.TextNode
import de.localtoast.palaver.sys.HtmlOperations
import de.localtoast.palaver.sys.PalaverHttpClient
import de.localtoast.palaver.sys.chatroom.RoomRepository
import de.localtoast.palaver.sys.events.Event
import de.localtoast.palaver.sys.events.MatrixEventProcessor
import io.kamax.matrix.client.MatrixClientRequestException
import javafx.application.Platform
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseEvent
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import javafx.scene.layout.StackPane
import org.fxmisc.flowless.Cell
import org.fxmisc.flowless.VirtualFlow
import org.fxmisc.flowless.VirtualizedScrollPane
import org.slf4j.LoggerFactory
import tornadofx.Fragment
import tornadofx.addClass
import tornadofx.get
import tornadofx.hbox
import tornadofx.hboxConstraints
import tornadofx.label
import tornadofx.onChange
import tornadofx.vbox
import tornadofx.vboxConstraints
import java.util.Timer
import java.util.concurrent.Semaphore
import kotlin.concurrent.timerTask

class BacklogView : Fragment() {
    companion object {
        const val paramId = "id"
        const val notificationAreaHeight = 30.0
    }

    private val logger = LoggerFactory.getLogger(this.javaClass.name)

    private val client: PalaverHttpClient by inject()
    private val eventProcessor: MatrixEventProcessor by inject()
    private val inputListener: InputListener by inject()

    private val roomId = params[paramId] as String
    private val room = RoomRepository.rooms.getValue(roomId)

    private val fetchingBacklog = Semaphore(1)
    private var processing = false

    private var scrolledToBottom = true

    private val notificationArea = StackPane().apply {
        val height = notificationAreaHeight
        prefHeight = height
        minHeight = height
    }
    private val userPanel = find<UserPanel>(mapOf(UserPanel.paramRoomId to roomId))
    private val inputBox = find<SendMessageInputBox>(mapOf(SendMessageInputBox.paramRoomId to roomId,
        SendMessageInputBox.paramNotificationArea to notificationArea))

    private var overlayWindow: Pair<EventRow, Pair<OverlayWithPointer, Int>>? = null

    private fun forwardTextInput(it: KeyEvent) {
        if (!inputBox.hasFocus()) {
            if (!it.isAltDown && !it.isControlDown && !it.isMetaDown && !it.isShortcutDown) {
                inputBox.sendCharacterToInputBox(it.character)
                inputBox.requestFocus()
            }
        }
    }

    private val fetchingBacklogPanel = find<BacklogFetchingNotificationPanel>().apply {
        root.setOnMouseClicked {
            inputBox.requestFocus()
        }
    }

    private val backlogGuiList =
        VirtualFlow.createVertical(room.backlogManager.rows) { row -> renderCell(row) }

    private val selectionManager = SelectionManager(backlogGuiList)

    override val root = hbox {
        vboxConstraints {
            vGrow = Priority.ALWAYS
        }

        vbox {
            hboxConstraints {
                hGrow = Priority.ALWAYS
            }
            setOnKeyTyped {
                forwardTextInput(it)
            }

            var currentTopicVBox = TextNode(HtmlOperations.makeTextToLinks(room.topic.value?.topic))
            val vBox = vbox {
                hboxConstraints {
                    hGrow = Priority.ALWAYS
                }
                addClass(ChatRoomStyle.topicBox)

                hbox {
                    setOnMouseClicked {
                        inputBox.requestFocus()
                    }

                    label {
                        text = room.name.value
                        room.name.onChange {
                            Platform.runLater {
                                if (it != null) {
                                    text = it
                                }
                            }
                        }

                        addClass(ChatRoomStyle.topicBoxRoomName)
                    }
                }

                add(currentTopicVBox)
            }

            room.topic.onChange {
                Platform.runLater {
                    val newTopicVBox = TextNode(HtmlOperations.makeTextToLinks(room.topic.value?.topic))
                    val children = vBox.children
                    children.add(children.indexOf(currentTopicVBox), newTopicVBox)
                    children.remove(currentTopicVBox)
                    currentTopicVBox = newTopicVBox
                }
            }

            add(fetchingBacklogPanel)

            hbox {
                vboxConstraints {
                    vGrow = Priority.ALWAYS
                }
                addClass(ChatRoomStyle.backlogPanelPadding)
                val backlogScrollPane = VirtualizedScrollPane(backlogGuiList).apply {
                    addClass(ChatRoomStyle.backlogPanel)
                    hboxConstraints {
                        hGrow = Priority.ALWAYS
                    }
                    content.setOnScroll {
                        Platform.runLater {
                            runAfterScrollActions()
                        }
                    }

                    // Scrolling by keyboard
                    setOnKeyPressed {
                        val scrollPageHeight = (this.height - this.padding.top - this.padding.bottom) * 0.95
                        val scrollLineHeight = 15.0
                        when {
                            it.code == KeyCode.PAGE_UP -> {
                                content.scrollYBy(-scrollPageHeight)
                                runAfterScrollActions()
                            }
                            it.code == KeyCode.PAGE_DOWN -> {
                                content.scrollYBy(scrollPageHeight)
                                runAfterScrollActions()
                            }
                            it.code == KeyCode.UP -> {
                                content.scrollYBy(-scrollLineHeight)
                                runAfterScrollActions()
                            }
                            it.code == KeyCode.DOWN -> {
                                content.scrollYBy(scrollLineHeight)
                                runAfterScrollActions()
                            }
                            it.isControlDown && it.code == KeyCode.C -> {
                                selectionManager.copyCurrentSelectionToClipboard()
                            }
                        }
                        it.consume()
                    }

                    content.layoutBoundsProperty().onChange {
                        if (scrolledToBottom) {
                            scrollToBottom()
                        }
                    }
                }

                add(backlogScrollPane)
            }

            notificationArea.setOnMouseClicked {
                inputBox.requestFocus()
            }
            add(notificationArea)
            add(inputBox)
        }
        add(userPanel)
    }

    private fun runAfterScrollActions() {
        loadBacklogIfNecessary()
        val visibleIndex = backlogGuiList.lastVisibleIndex
        scrolledToBottom = visibleIndex == -1 ||
            (backlogGuiList.visibleCells().last() as BacklogCell).getRow() == room.backlogManager.rows.last()
    }

    init {
        runAsync(daemon = true) {
            logger.debug("${room.id}: Event loop of room started")

            val newEvents = mutableListOf<Event>()
            while (true) {
                val newEvent = eventProcessor.processNewRoomEvent(room.id, room.unprocessedBacklogEvents.take())
                if (newEvent != null) {
                    newEvents.add(newEvent)
                }
                if (room.unprocessedBacklogEvents.isEmpty() && newEvents.isNotEmpty()) {
                    addEvents(ArrayList(newEvents))
                    newEvents.clear()
                }
            }
        }

        Timer("BacklogViewTimer", true).schedule(timerTask {
            Platform.runLater {
                room.backlogManager.updateTodayStringIfNecessary()
            }
        }, 4000, 4000)

        backlogGuiList.setOnMousePressed {
            backlogGuiList.requestFocus()
            if (it.isShiftDown) {
                updateEndOfSelectionMarker(it)
            } else {
                selectionManager.setSelectionEnd(null)

                val hit = backlogGuiList.hit(it.x, it.y)
                val cell = hit.cell as BacklogCell
                val cellIndex = hit.cellIndex
                val nodeIndex = cell.nodeHit(it.screenX, it.screenY)
                val charIndex = cell.characterHit(it.screenX, it.screenY)
                if (nodeIndex != null && charIndex != null) {
                    val selectionStart = SelectionIndex(cellIndex, nodeIndex, charIndex)
                    selectionManager.setSelectionStart(selectionStart)
                } else {
                    selectionManager.setSelectionStart(null)
                }
            }
        }

        backlogGuiList.setOnMouseDragged {
            updateEndOfSelectionMarker(it)
        }

        inputListener.registerListener {
            // Send read marker, if view is visible
            if (root.parent != null) {
                updateReadMarker()
            }
        }
    }

    private fun updateReadMarker() {
        val currentUser = client.user

        val lastVisibleIndex = backlogGuiList.lastVisibleIndex
        val firstVisibleIndex = backlogGuiList.firstVisibleIndex
        if (lastVisibleIndex >= 0 && firstVisibleIndex >= 0) {
            val visibleRange = IntRange(firstVisibleIndex, lastVisibleIndex)

            if (room.readMarkerManager.isReadMarkerInRange(currentUser, visibleRange)) {
                val lastVisibleCell = backlogGuiList.getCell(lastVisibleIndex) as BacklogCell?
                val lastVisibleRow = lastVisibleCell?.getRow()
                val lastVisibleEvent = if (lastVisibleRow is EventRow) {
                    lastVisibleRow.event.id
                } else null
                if (lastVisibleEvent != null &&
                    room.readMarkerManager.addLocalReadMarker(lastVisibleEvent, currentUser)) {

                    client.sendReadReceipt(room, lastVisibleEvent)
                }
            }
        }
    }

    private val selectionUpdateSemaphore = Semaphore(1)

    private fun updateEndOfSelectionMarker(it: MouseEvent) {
        if (selectionUpdateSemaphore.tryAcquire()) {
            val hit = backlogGuiList.hit(it.x, it.y)
            if (hit.isCellHit) {
                val cell = hit.cell as BacklogCell
                val cellIndex = hit.cellIndex
                val nodeIndex = cell.nodeHit(it.screenX, it.screenY)
                val charIndex = cell.characterHit(it.screenX, it.screenY)
                if (nodeIndex != null && charIndex != null) {
                    val selectionEnd = SelectionIndex(cellIndex, nodeIndex, charIndex)
                    selectionManager.setSelectionEnd(selectionEnd)
                }
            }
            selectionUpdateSemaphore.release()
        }
    }

    private fun renderCell(row: BacklogRow?): Cell<BacklogRow, *>? {
        if (row != null) {

            // Get an existing readMarkerOverlay read marker window and pass it to the newly created cell
            val overlayPair = overlayWindow
            val overlay = if (overlayPair != null && row == overlayPair.first) {
                overlayPair.second.first
            } else null

            val cellFragment = find<BacklogCellFragment>(
                mapOf(BacklogCellFragment::row to row,
                    BacklogCellFragment::parentList to backlogGuiList,
                    BacklogCellFragment::readMarkerManager to room.readMarkerManager,
                    BacklogCellFragment::readMarkerOverlayParam to overlay,
                    BacklogCellFragment::showReadMarkerOverlay to { overlayWithPointer: OverlayWithPointer, index: Int ->
                        if (row is EventRow) {
                            overlayWindow = Pair(row, Pair(overlayWithPointer, index))
                        }
                    })
            )

            val cell = BacklogCell(cellFragment)
            selectionManager.updateSelection(room.backlogManager.rows.indexOf(row), cell)

            // Reconnect existing readMarkerOverlay window with appropriate item of the cell
            if (overlayPair != null && row == overlayPair.first) {
                val newTarget = cell.getOverlayTarget(overlayPair.second.second)
                if (newTarget != null && newTarget is Region) {
                    overlay?.pointerTarget?.value = newTarget
                } else {
                    // Could not reconnect, becauso no target item was found -> dispose
                    overlay?.dispose()
                }
            }

            return cell
        }

        return null
    }

    fun startProcessing() {
        if (!processing) {
            processing = true
            room.fetchUserMetaData.value = true

            if (room.unprocessedBacklogEvents.isEmpty()) {
                loadBacklog()
            }
            fetchingBacklogPanel.fadeIn()
        }
    }

    private fun loadBacklogIfNecessary() {
        if (isScrolledToTop()) {
            loadBacklog()
        }
    }

    private fun loadBacklog() {
        if (fetchingBacklog.tryAcquire()) {
            fetchingBacklogPanel.fadeIn()
            logger.debug("Fetching backlog for room ${room.name.value}")
            loadBacklogAsynchronously()
        }
    }

    private fun loadBacklogAsynchronously() {
        runAsync(daemon = true) {
            try {
                if (!client.fetchBacklog(room)) {
                    fetchingBacklogPanel.fadeOut()
                    fetchingBacklog.release()
                }
            } catch (e: MatrixClientRequestException) {
                // When there are problems with the connection, retry fetching
                loadBacklogAsynchronously()
            }
        }
    }

    private fun addEvents(events: ArrayList<Event>) {
        Platform.runLater {
            selectionManager.clearSelection()

            events.forEach {
                room.backlogManager.addEvent(it, messages["today"])
            }
            /*
             * Update the read markers, as their internal saved positions can change after loading new events.
             * Reason: No read marker events send for the client's own events, so the client has to calculate the
             * correct read marker positions all by itself.
             */
            room.readMarkerManager.updateAllReadMarkers()

            when {
                scrolledToBottom -> scrollToBottom()
            }

            fetchingBacklogPanel.fadeOut()
            fetchingBacklog.release()
            loadBacklogIfNecessary()
        }
    }

    private fun scrollToBottom() {
        backlogGuiList.showAsLast(room.backlogManager.rows.size - 1)
    }

    private fun isScrolledToTop(): Boolean {
        return backlogGuiList.firstVisibleIndex <= 0
    }
}
