/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys.chatroom

import de.localtoast.palaver.sys.StringMatcher
import javafx.beans.property.SimpleStringProperty
import javafx.collections.transformation.FilteredList
import javafx.collections.transformation.SortedList
import tornadofx.ItemViewModel
import tornadofx.observable
import tornadofx.onChange
import java.util.function.Predicate

class ChatRoomModel : ItemViewModel<ChatRoom>() {
    enum class RoomType {
        Person, Group
    }

    val filter = SimpleStringProperty("")

    private val persons = mutableListOf<ChatRoom>().observable()
    private val groups = mutableListOf<ChatRoom>().observable()

    private val personsFiltered = FilteredList(persons)
    private val groupsFiltered = FilteredList(groups)

    val personsSortedAndFiltered = SortedList(personsFiltered)
    val groupsSortedAndFiltered = SortedList(groupsFiltered)

    init {
        personsSortedAndFiltered.setComparator { item1, item2 -> item1.name.value.compareTo(item2.name.value, ignoreCase = true) }
        groupsSortedAndFiltered.setComparator { item1, item2 -> item1.name.value.compareTo(item2.name.value, ignoreCase = true) }

        filter.onChange {
            personsFiltered.predicate = Predicate {
                StringMatcher.getMatchPositions(it.name.value, filter.value).size == filter.value.length
            }
            groupsFiltered.predicate = Predicate {
                StringMatcher.getMatchPositions(it.name.value, filter.value).size == filter.value.length
            }
        }
    }

    fun addRoomIfNew(room: ChatRoom) {
        if (!persons.contains(room) && !groups.contains(room)) {
            when (room.type.value) {
                RoomType.Person -> persons.add(room)
                RoomType.Group -> groups.add(room)
                null -> {
                }
            }
        }
    }
}
