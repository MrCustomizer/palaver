/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.text

import de.localtoast.palaver.gui.stylesheet.TextStyles
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import tornadofx.CssRule
import tornadofx.add
import tornadofx.addClass
import tornadofx.removeClass

/**
 * Displays formatted text (with support for links)
 */
class TextNode(text: String, style: CssRule? = null) : VBox() {

    init {
        val stackPane = StackPane()
        add(stackPane)
        val innerVBox = VBox()
        TextFlowHelper.createTextFlows(text).forEach {
            innerVBox.add(it)
            if (style != null) {
                it.children.forEach {
                    it.removeClass(TextStyles.message)
                    it.addClass(style)
                }
            }
        }
        stackPane.add(innerVBox)
    }
}