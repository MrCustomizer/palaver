/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.fragments.validatedinputbox

import de.localtoast.palaver.gui.UiController
import de.localtoast.palaver.gui.stylesheet.FontStyle
import de.localtoast.palaver.gui.stylesheet.InputBoxStyle
import de.localtoast.palaver.gui.stylesheet.ValidationStyle
import javafx.animation.Timeline
import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.TextField
import javafx.scene.image.Image
import javafx.scene.image.ImageView
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.StackPane
import javafx.scene.layout.VBox
import javafx.util.Duration
import tornadofx.add
import tornadofx.addClass
import tornadofx.box
import tornadofx.hbox
import tornadofx.hboxConstraints
import tornadofx.imageview
import tornadofx.keyframe
import tornadofx.label
import tornadofx.onChange
import tornadofx.px
import tornadofx.style
import tornadofx.timeline

class ValidatedInputBox(
    textField1: TextField,
    textField2: TextField? = null,
    label1: String? = null,
    label2: String? = null,
    readOnlyField: Boolean = false
) : VBox() {

    var decorateAndValidate = false

    var hasBeenEdited = false

    private enum class State { Nothing, Clean, Warning, Error }

    private var currentState = State.Nothing

    private val readOnlyProperty = SimpleBooleanProperty(readOnlyField)

    private val imageViewSuccess = ImageView().apply {
        image = Image("/de/localtoast/palaver/icons/status/dialog-clean.png")
    }
    private val labelClean = Label().apply {
        // Add style to prevent layout quirks
        addClass(ValidationStyle.validationLabelWarning)
    }

    private val stackPaneImageWritable = StackPane().apply {
        minWidth = 20.0
    }
    private val stackPaneImageReadOnly = StackPane().apply {
        minWidth = 20.0
        style {
            padding = box(0.px, 10.px, 0.px, 0.px)
        }
    }
    private val stackPaneLabel = StackPane().apply {
        add(labelClean)
        alignment = Pos.CENTER_LEFT
    }

    private var cleanTimeline: Timeline? = null

    private val labelWarning = Label().apply {
        addClass(ValidationStyle.validationLabelWarning)
        opacity = 0.0
    }
    private val imageViewWarning = ImageView().apply {
        image = Image("/de/localtoast/palaver/icons/status/dialog-warning.png")
        opacity = 0.0
    }

    private var warningTimeline: Timeline? = null

    private val labelError = Label().apply {
        addClass(ValidationStyle.validationLabelError)
        opacity = 0.0
    }
    private val imageViewError = ImageView().apply {
        image = Image("/de/localtoast/palaver/icons/status/dialog-error-4.png")
        opacity = 0.0
    }

    private var errorTimeline: Timeline? = null

    init {
        hboxConstraints {
            hGrow = Priority.ALWAYS
        }

        val writableFields = HBox().apply {
            alignment = Pos.CENTER
            addClass(InputBoxStyle.validatedInputBoxBorder)

            if (label1 != null) {
                label(label1) {
                    addClass(FontStyle.regular)
                }
            }

            add(textField1)
            with(textField1) {
                hboxConstraints {
                    hGrow = Priority.ALWAYS
                }
                addClass(InputBoxStyle.validatedInputBox)
            }

            if (label2 != null) {
                label(label2) {
                    addClass(FontStyle.regular)
                }
            }

            if (textField2 != null) {
                add(textField2)
                with(textField2) {
                    hboxConstraints {
                        hGrow = Priority.ALWAYS
                    }
                    addClass(InputBoxStyle.validatedInputBox)
                }
            }

            add(stackPaneImageWritable)
        }

        val readOnlyFields = HBox(5.0).apply {
            style {
                padding = box(12.px, 0.px, 11.px, 0.px)
            }
            alignment = Pos.CENTER_LEFT

            imageview {
                image = Image("/de/localtoast/palaver/icons/actions/openIconLibrary/document-edit.png")
            }

            if (label1 != null) {
                label(label1) {
                    addClass(FontStyle.regular)
                }
            }

            hbox {
                hboxConstraints {
                    hGrow = Priority.ALWAYS
                }
                label {
                    addClass(FontStyle.regular)
                    text = textField1.text
                    textField1.textProperty().onChange {
                        text = it
                    }
                }
            }

            if (label2 != null) {
                label(label2) {
                    addClass(FontStyle.regular)
                }
            }

            if (textField2 != null) {
                hbox {
                    hboxConstraints {
                        hGrow = Priority.ALWAYS
                    }
                    label {
                        addClass(FontStyle.regular)
                        text = textField2.text
                        textField2.textProperty().onChange {
                            text = it
                        }
                    }
                }
            }

            add(stackPaneImageReadOnly)

            setOnMouseClicked {
                readOnlyProperty.value = false
                hasBeenEdited = true
                Platform.runLater {
                    textField1.requestFocus()
                }
            }
        }

        if (readOnlyProperty.value) {
            add(readOnlyFields)
        } else {
            add(writableFields)
        }

        add(stackPaneLabel)

        readOnlyProperty.onChange {
            children.clear()
            if (it) {
                add(readOnlyFields)
            } else {
                add(writableFields)
            }

            add(stackPaneLabel)
        }

        if (readOnlyField) {
            textField1.focusedProperty().onChange {
                if (!it && !(textField2 != null && textField2.isFocused)) {
                    readOnlyProperty.value = true
                }
            }

            textField2?.focusedProperty()?.onChange {
                if (!it && !textField1.isFocused) {
                    readOnlyProperty.value = true
                }
            }
        }
    }

    fun setSuccess() {
        changeState(State.Clean)
    }

    fun setWarning(text: String) {
        labelWarning.text = text
        changeState(State.Warning)
    }

    fun setError(text: String) {
        labelError.text = text
        changeState(State.Error)
    }

    private fun changeState(target: State) {
        if (decorateAndValidate) {
            if (currentState != target) {
                when (currentState) {
                    State.Nothing -> {
                    }
                    State.Clean -> {
                        cleanTimeline?.stop()
                        cleanTimeline = fadeOut(labelClean, imageViewSuccess)
                    }
                    State.Warning -> {
                        warningTimeline?.stop()
                        warningTimeline = fadeOut(labelWarning, imageViewWarning)
                    }
                    State.Error -> {
                        errorTimeline?.stop()
                        errorTimeline = fadeOut(labelError, imageViewError)
                    }
                }
                fadeIn(target)
                currentState = target
            }
        }
    }

    private fun fadeOut(label: Label, imageView: ImageView): Timeline {
        val timeline = timeline {
            keyframe(Duration.millis(UiController.animationDurationOut.toMillis())) {
                keyvalue(label.opacityProperty(), 0.0)
                keyvalue(imageView.opacityProperty(), 0.0)
            }
        }

        timeline.setOnFinished {
            stackPaneImageWritable.children.remove(imageView)
            stackPaneImageReadOnly.children.remove(imageView)
            stackPaneLabel.children.remove(label)
        }

        return timeline
    }

    private fun fadeIn(target: State) {
        when (target) {
            State.Nothing -> {
            }
            State.Clean -> {
                cleanTimeline?.stop()
                cleanTimeline = fadeIn(labelClean, imageViewSuccess)
            }
            State.Warning -> {
                warningTimeline?.stop()
                warningTimeline = fadeIn(labelWarning, imageViewWarning)
            }
            State.Error -> {
                errorTimeline?.stop()
                errorTimeline = fadeIn(labelError, imageViewError)
            }
        }
    }

    private fun fadeIn(label: Label, imageView: ImageView): Timeline {
        stackPaneLabel.add(label)
        if (readOnlyProperty.value) {
            stackPaneImageReadOnly.add(imageView)
        } else {
            stackPaneImageWritable.add(imageView)
        }

        return timeline {
            keyframe(Duration.millis(UiController.animationDurationIn.toMillis())) {
                keyvalue(label.opacityProperty(), 1.0)
                keyvalue(imageView.opacityProperty(), 1.0)
            }
        }
    }
}
