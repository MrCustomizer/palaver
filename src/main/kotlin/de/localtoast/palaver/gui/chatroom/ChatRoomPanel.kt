/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom

import de.localtoast.palaver.Settings
import de.localtoast.palaver.gui.stylesheet.ChatRoomStyle
import de.localtoast.palaver.sys.chatroom.ChatRoom
import de.localtoast.palaver.sys.chatroom.ChatRoomModel
import javafx.scene.layout.Priority
import tornadofx.View
import tornadofx.addClass
import tornadofx.hboxConstraints
import tornadofx.onChange
import tornadofx.stackpane
import tornadofx.vbox
import tornadofx.vboxConstraints

class ChatRoomPanel : View() {
    private val model: ChatRoomModel by inject()

    private val backlogStackPane = stackpane {
        vboxConstraints {
            vGrow = Priority.ALWAYS
        }
    }
    private val backlogViews = mutableMapOf<String, BacklogView>()

    override val root = vbox {
        hboxConstraints {
            hGrow = Priority.ALWAYS
        }
        addClass(ChatRoomStyle.chatRoomPanel)

        add(backlogStackPane)
    }

    init {
        model.itemProperty.onChange {
            if (it != null) {
                val view = getBacklogView(it)
                backlogStackPane.children.clear()
                backlogStackPane.add(view.root)
                view.startProcessing()
                Settings.lastRoom = it.id
            }
        }
    }

    private fun getBacklogView(it: ChatRoom): BacklogView {
        val roomId = it.id
        val viewFromMap = backlogViews[roomId]
        return if (viewFromMap != null) viewFromMap else {
            val view = find<BacklogView>(mapOf(BacklogView.paramId to roomId))
            backlogViews[roomId] = view
            view
        }
    }
}