/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.fragments.validatedinputbox

import de.localtoast.palaver.gui.stylesheet.FontStyle
import javafx.scene.Cursor
import javafx.scene.text.Text
import org.slf4j.LoggerFactory
import tornadofx.addClass
import java.awt.Desktop
import java.net.URI
import javax.swing.SwingUtilities

class HyperlinkText(text: String, link: String) : Text(text) {
    private val logger = LoggerFactory.getLogger(this.javaClass.name)

    init {
        addClass(FontStyle.hyperlinkFont)
        setOnMousePressed {
            SwingUtilities.invokeLater {
                if (Desktop.isDesktopSupported()) {
                    if (!link.startsWith("http")) {
                        Desktop.getDesktop().browse(URI("https://$link"))
                    } else {
                        Desktop.getDesktop().browse(URI(link))
                    }
                } else {
                    logger.error("Desktop not supported by Java, cannot open link!")
                }
            }
        }

        setOnMouseEntered {
            scene.cursor = Cursor.HAND
        }

        setOnMouseExited {
            scene.cursor = Cursor.DEFAULT
        }
    }
}