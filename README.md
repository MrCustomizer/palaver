# Palaver

**This project is currently unmaintained Feel free to fork it, it's Open Source!**

## Purpose

Palaver is a chat client for [Matrix](https://matrix.org/blog/home/) written in [Kotlin](https://kotlinlang.org/).

## Status
The project is currently in early Alpha status. Basic chatting is possible, but a lot of important features are missing.

### Features
For a complete list of features, take a look at the [changelog](https://gitlab.com/MrCustomizer/palaver/blob/master/CHANGELOG.md).

There is a [short demo video on youtube](https://youtu.be/AuGuxAPuRIM), which shows some of the features of v0.1.0.

You can get a rough idea of the next planned features in the [issue tracker](https://gitlab.com/MrCustomizer/palaver/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=enhancement)
and on the [milestones page](https://gitlab.com/MrCustomizer/palaver/milestones).
Feel free to add your wishes for new features as issues over there. I will try to to respect them during development.

## Compile
Be sure to compile Palaver with Java 8 as the project is heavily based on 
[TornadoFX](https://github.com/edvin/tornadofx), which doesn't support newer Java SDKs yet.
If you are using OpenJDK, you probably have to install OpenJFX too, as most 
packages of the JDK don't include JavaFX any more.

If you want to compile the project, clone the repository and run 
`./gradlew build`. 

If you want to create a fat jar with all the dependencies included, run 
`./gradlew shadowJar` instead.

## Run
If you have built a fat jar, you should be able to just run the the program 
by executing `java -jar <nameOfFatJar.java>`.

## Contribute
If you want to contribute to the project feel free to open pull 
requests against my repository. Any help is appreciated!

I recommend using [IntelliJ IDEA](https://www.jetbrains.com/idea/). You can 
import the source code as Gradle-project. In Linux it's possible that you get 
problems importing JavaFX's classes at first. If that's the case, check if there
is an OpenJFX-package for your distribution. If OpenJFX is 
installed and the problem persists, take a look at the class path of the SDK
that is used in the project. If the file `jfxrt.jar` is missing, this is 
most likely the cause for your issues. Add this file to the class path
and everything should be fine. Further information on this topic can be found
on [stackoverflow](https://stackoverflow.com/questions/27178895/cannot-resolve-symbol-javafx-application-in-intellij-idea-ide).

Please run automatic code formatting with `./gradlew spotlessApply` 
before opening a pull request. If you are getting in trouble with wildcard 
imports (Spotless/Ktlint don't like them), disable wildcard imports in Intellij 
Idea: Go to `File -> Settings -> Editor -> Code Style -> Kotlin` and open 
the tab `Imports`. Activate `Use single name import`for `Top-level Symbols` 
and `Java Statics and Enum Members`.

## Contact
If you find any bugs, please file an issue in the [bug tracker of this repository](https://gitlab.com/MrCustomizer/palaver/issues).

If you want to discuss anything related to Palaver, feel free to join Palaver's channel in Matrix:
[#palaver:matrix.localtoast.de](https://matrix.to/#/#palaver:matrix.localtoast.de)