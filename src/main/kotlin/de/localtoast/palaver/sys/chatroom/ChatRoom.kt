/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys.chatroom

import de.localtoast.palaver.Settings
import de.localtoast.palaver.gui.chatroom.backlog.BacklogManager
import de.localtoast.palaver.sys.GraphicUtilities
import de.localtoast.palaver.sys.PalaverHttpClient
import de.localtoast.palaver.sys.events.EventRepository
import de.localtoast.palaver.sys.users.User
import de.localtoast.palaver.sys.users.UserRepository
import io.kamax.matrix._MatrixID
import io.kamax.matrix.client.MatrixClientRequestException
import io.kamax.matrix.client.MatrixHttpRoom
import io.kamax.matrix.event._MatrixEvent
import javafx.application.Platform
import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleLongProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.SetChangeListener
import javafx.scene.image.Image
import javafx.scene.input.KeyCode
import org.slf4j.LoggerFactory
import tornadofx.onChange
import tornadofx.runAsync
import java.awt.image.BufferedImage
import java.net.URI
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.Semaphore

// TODO think about the models. Separate model for table view??

class ChatRoom(
    val id: String,
    roomName: ChatRoomName?,
    topic: ChatRoomTopic?,
    userIds: Set<_MatrixID>,
    private val userRepo: UserRepository,
    eventRepo: EventRepository,
    private val client: PalaverHttpClient,
    roomAliases: Set<String>
) {

    private val logger = LoggerFactory.getLogger(this.javaClass.name)

    var type = SimpleObjectProperty<ChatRoomModel.RoomType>(if (userIds.size <= 2) {
        ChatRoomModel.RoomType.Person
    } else {
        ChatRoomModel.RoomType.Group
    })

    /*
     * The real room roomName can be empty. If this is the case and it is a personal chat, we take the other user's roomName
     * as visible room roomName. That is why there are two different values for the room names.
     */
    val realRoomName = SimpleObjectProperty<ChatRoomName>(roomName)
    val name = SimpleStringProperty(realRoomName.value?.name ?: "")

    val topic = SimpleObjectProperty<ChatRoomTopic>(topic)

    val aliases = FXCollections.observableSet(roomAliases)!!

    val users = FXCollections.observableArrayList<_MatrixID>()!!

    var fromToken: String? = null
    val unprocessedBacklogEvents = LinkedBlockingQueue<_MatrixEvent>()

    var fetchUserMetaData = SimpleBooleanProperty(false)
    val userMetadataFetchQueue = LinkedBlockingQueue<User>()

    val shortcut = SimpleObjectProperty<KeyCode>(Settings.getShortcutForRoom(id))
    val unreadCount = SimpleLongProperty(0)
    val highlightCount = SimpleLongProperty(0)

    var avatarFetched = false
    private val fetchAvatarSemaphore = Semaphore(1)
    private val avatar = SimpleObjectProperty<BufferedImage>(null)
    var scaledAndCutOutAvatar = SimpleObjectProperty<Image>(when (type.value) {
        ChatRoomModel.RoomType.Person -> Image("/de/localtoast/palaver/icons/avatars/avatar-default.png")
        else -> Image("/de/localtoast/palaver/icons/avatars/system-users.png")
    })

    val backlogManager = BacklogManager(eventRepo)
    val readMarkerManager = ReadMarkerManager(backlogManager, eventRepo, client.user)

    init {
        userIds.forEach {
            if (!users.contains(it)) {
                users.add(it)
            }
        }

        updateRoomName(realRoomName.value?.name, aliases.firstOrNull())

        users.onChange {
            type.value = if (it.list.size <= 2) ChatRoomModel.RoomType.Person else ChatRoomModel.RoomType.Group
            updateRoomName(realRoomName.value?.name, aliases.firstOrNull())
        }

        realRoomName.onChange {
            if (it != null) {
                updateRoomName(it.name, aliases.firstOrNull())
            }
        }

        aliases.addListener(SetChangeListener<String> { change ->
            if (change != null && change.set.isNotEmpty()) {
                updateRoomName(realRoomName.value?.name, change.set.firstOrNull())
            }
        })
    }

    fun fetchMetadata(urlParam: URI? = null) {
        if (!avatarFetched) {
            if (fetchAvatarSemaphore.tryAcquire()) {
                try {
                    val avatarUrl = if (urlParam != null) urlParam else {
                        val url = MatrixHttpRoom(client.context, id).avatarUrl
                        when {
                            url.isPresent -> URI(url.get())
                            type.value == ChatRoomModel.RoomType.Person -> {
                                val otherUser = getOtherUser()
                                when {
                                    otherUser != null -> {
                                        URI(otherUser.avatarUrl)
                                    }
                                    else -> {
                                        logger.debug("$id: No avatar found")
                                        null
                                    }
                                }
                            }
                            else -> {
                                logger.debug("$id: No avatar found")
                                null
                            }
                        }
                    }

                    if (avatarUrl != null) {
                        logger.debug("$id: Start fetching room avatar: $avatarUrl")
                        val image = client.loadImage(avatarUrl)
                        if (image != null) {
                            setAvatarWithScaling(image)
                        }
                        logger.debug("$id: Finished fetching room avatar")
                        avatarFetched = true
                    }
                } catch (e: MatrixClientRequestException) {
                    logger.debug("Could not load room avatar.", e)
                }
                fetchAvatarSemaphore.release()
            }
        }
    }

    private fun setAvatarWithScaling(bufferedImage: BufferedImage) {
        avatar.value = bufferedImage

        val scaledImage = GraphicUtilities.scaleAvatar(bufferedImage, GraphicUtilities.avatarSize)

        if (scaledImage != null) {
            val roundImage = GraphicUtilities.cutOutCircle(scaledImage)
            scaledAndCutOutAvatar.value = roundImage
        } else {
            logger.error("$id: Room's avatar could not be scaled")
        }
    }

    // TODO use the suggestions in the spec to get correct name (also for users)
    private fun updateRoomName(realRoomNameParam: String?, alias: String?) {
        val realRoomName = realRoomNameParam ?: ""
        if (realRoomName.isNotBlank()) {
            if (type.value == ChatRoomModel.RoomType.Group ||
                (type.value == ChatRoomModel.RoomType.Person && realRoomName.isNotBlank())) {
                name.value = realRoomName
            }
        } else if ((type.value == ChatRoomModel.RoomType.Group) && alias != null && alias.isNotBlank()) {
            name.value = alias
        } else if (type.value == ChatRoomModel.RoomType.Person) {
            // Use name of the other user as fallback
            val otherUser = getOtherUser()
            if (otherUser != null) {
                otherUser.name.onChange {
                    if (type.value == ChatRoomModel.RoomType.Person) {
                        Platform.runLater {
                            name.value = it
                        }
                    }
                }
                name.value = otherUser.name.value
                runAsync {
                    otherUser.fetchDisplayName()
                }
            } else {
                logger.debug("$id: The user list is empty. Is no one else in the room?")
            }
        }
    }

    private fun getOtherUser(): User? {
        val userList = mutableListOf<_MatrixID>()
        userList.addAll(users)
        val iterator = userList.iterator()
        while (iterator.hasNext()) {
            val nextUser = iterator.next()
            if (nextUser == client.user) {
                iterator.remove()
            }
        }
        return if (userList.isNotEmpty()) userRepo.getUser(userList[0]) else null
    }
}
