/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.login

import de.localtoast.palaver.gui.UiController
import de.localtoast.palaver.gui.fragments.validatedinputbox.ValidatedInputBox
import de.localtoast.palaver.gui.fragments.validatedinputbox.ValidationDecorator
import de.localtoast.palaver.gui.stylesheet.FontStyle
import de.localtoast.palaver.gui.stylesheet.LoginDialogStyle
import de.localtoast.palaver.gui.stylesheet.PalaverStyle
import de.localtoast.palaver.gui.text.TextNode
import de.localtoast.palaver.sys.login.LoginCredentialsModel
import de.localtoast.palaver.sys.login.LoginValidators
import de.localtoast.palaver.sys.login.MatrixConnector
import io.kamax.matrix.MatrixID
import javafx.application.Platform
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.PasswordField
import javafx.scene.control.TextField
import javafx.scene.input.Clipboard
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyEvent
import javafx.scene.layout.ColumnConstraints
import javafx.scene.layout.Priority
import javafx.scene.layout.VBox
import javafx.scene.text.TextAlignment
import tornadofx.ValidationTrigger
import tornadofx.View
import tornadofx.action
import tornadofx.addClass
import tornadofx.addValidator
import tornadofx.bind
import tornadofx.button
import tornadofx.checkbox
import tornadofx.enableWhen
import tornadofx.get
import tornadofx.gridpane
import tornadofx.gridpaneConstraints
import tornadofx.hbox
import tornadofx.label
import tornadofx.onChange
import tornadofx.stackpane
import tornadofx.style
import tornadofx.vbox
import tornadofx.vboxConstraints

class LoginCredentialsDialog : View() {

    private val uiController: UiController by inject()
    private val matrixConnector: MatrixConnector by inject()
    private val model: LoginCredentialsModel by inject()
    private val validators: LoginValidators by inject()

    private val introductionLabelBox = VBox()

    private val passwordField = PasswordField().apply {
        promptText = messages["password"]

        bind(model.password)
    }

    private val homeserverInputField = TextField().apply {
        promptText = "https://matrix.org"
        bind(model.homeserver)
    }
    private val homeserverInputBox = ValidatedInputBox(homeserverInputField, readOnlyField = true)

    private val vBox = vbox {
        gridpaneConstraints {
            columnRowIndex(1, 0)
            vhGrow = Priority.ALWAYS
            alignment = Pos.CENTER
        }

        vbox(10.0) {
            val columnWidth = 450.0
            prefWidth = columnWidth
            minWidth = columnWidth
            maxWidth = columnWidth
            alignment = Pos.CENTER_LEFT

            val title = Label(messages["login"]).apply {
                addClass(FontStyle.paragraphTitle)
            }

            add(title)
            title.prefWidthProperty().bind(prefWidthProperty())

            add(introductionLabelBox)
            introductionLabelBox.add(TextNode(messages["loginText"], LoginDialogStyle.font))

            label(messages["matrixUsername"]) {
                addClass(FontStyle.title)
            }

            hbox(5.0) {
                alignment = Pos.CENTER
                vboxConstraints {
                    vGrow = Priority.ALWAYS
                }
                add(createMatrixIdField())
            }

            label(messages["homeserver"]) {
                addClass(FontStyle.title)
            }

            add(configureHomeserverField())

            label(messages["password"]) {
                addClass(FontStyle.title)
            }

            add(createPasswordField())

            checkbox {
                addClass(FontStyle.regular)
                text = messages["stayLoggedIn"]
                bind(model.stayLoggedIn)
                isSelected = true
                addEventFilter(KeyEvent.KEY_PRESSED) {
                    if (it.code == KeyCode.ENTER) {
                        startLoginFromGui()
                    }
                }
            }

            button {
                minWidth = columnWidth
                alignment = Pos.CENTER
                addClass(PalaverStyle.button)
                text = messages["login"]
                enableWhen { model.valid }
                action {
                    startLoginFromGui()
                }
            }

            model.validationContext.decorationProvider =
                { ValidationDecorator(it.message, it.severity) }
            model.validate(decorateErrors = false)
        }
    }

    override val root = stackpane {
        style {
            backgroundColor += PalaverStyle.defaultBackground
        }
        gridpane {
            val column1 = ColumnConstraints()
            column1.percentWidth = 50.0
            val column2 = ColumnConstraints()
            column2.percentWidth = 50.0
            columnConstraints.addAll(column1, column2)

            vbox(20.0) {
                gridpaneConstraints {
                    columnRowIndex(0, 0)
                }
                addClass(LoginDialogStyle.backgroundImage)
                alignment = Pos.TOP_CENTER

                label(messages["welcomeToMatrix"]) {
                    addClass(LoginDialogStyle.imageTitle)
                }

                label(messages["anOpenNetworkForSecureCommunication"]) {
                    addClass(LoginDialogStyle.imageText)
                    prefWidth = 350.0
                    isWrapText = true
                    textAlignment = TextAlignment.JUSTIFY
                }
            }

            add(vBox)
        }
    }

    private fun createMatrixIdField(): ValidatedInputBox {

        val domainTextField = object : TextField() {
            override fun paste() {
                if (!pasteMatrixId()) {
                    super.paste()
                }
            }
        }.apply {
            promptText = "matrix.org"
            bind(model.domain)

            textProperty().onChange {
                if (!homeserverInputBox.hasBeenEdited) {
                    model.homeserver.value = "https://$it"
                }
            }
        }

        val localPartTextField = object : TextField() {
            override fun paste() {
                if (!pasteMatrixId()) {
                    super.paste()
                }
            }
        }.apply {
            promptText = "Bob"
            bind(model.localPart)

            textProperty().addListener { _, oldValue, newValue ->
                run {
                    if ((oldValue == null || !oldValue.endsWith(":")) && newValue.endsWith(":")) {
                        domainTextField.requestFocus()
                        Platform.runLater {
                            text = text.substring(0, text.length - 1)
                        }
                    } else if ((oldValue == null || !oldValue.startsWith("@")) && newValue.startsWith("@")) {
                        Platform.runLater {
                            text = text.substring(1, text.length)
                        }
                    }
                }
            }

            Platform.runLater {
                requestFocus()
            }
        }

        val matrixIdInput = ValidatedInputBox(localPartTextField, domainTextField, "@", ":")

        with(localPartTextField) {
            setOnAction {
                matrixIdInput.decorateAndValidate = true
                model.validate(focusFirstError = false)
                startLoginFromGui()
            }
            textProperty().onChange {
                model.matrixId.value = "@${it ?: ""}:${domainTextField.text ?: ""}"
                if (matrixIdInput.decorateAndValidate) {
                    model.validate(focusFirstError = false)
                }
            }
        }

        with(domainTextField) {
            setOnAction {
                matrixIdInput.decorateAndValidate = true
                model.validate(focusFirstError = false)
                startLoginFromGui()
            }
            focusedProperty().onChange {
                if (!it) {
                    matrixIdInput.decorateAndValidate = true
                    model.validate(focusFirstError = false)
                }
            }
            textProperty().onChange {
                model.matrixId.value = "@${localPartTextField.text ?: ""}:${it ?: ""}"
                matrixIdInput.decorateAndValidate = true
                model.validate(focusFirstError = false)
            }
        }

        model.matrixId.addValidator(matrixIdInput, trigger = ValidationTrigger.None,
            validator = validators.matrixID)
        return matrixIdInput
    }

    private fun pasteMatrixId(): Boolean {
        val localPart = model.localPart.value
        val domain = model.domain.value
        return when {
            (localPart == null || localPart.isBlank())
                && (domain == null || domain.isBlank()) -> {
                val clipboard = Clipboard.getSystemClipboard()
                when {
                    clipboard != null && clipboard.hasString() -> {
                        var text = clipboard.string
                        when {
                            text != null -> try {
                                val idBuilder = MatrixID.Builder(text)
                                val id = idBuilder.acceptable()
                                model.localPart.value = id.localPart
                                model.domain.value = id.domain
                                Platform.runLater {
                                    passwordField.requestFocus()
                                }
                                true
                            } catch (e: IllegalArgumentException) {
                                false
                            }
                            else -> false
                        }
                    }
                    else -> false
                }
            }
            else -> false
        }
    }

    private fun configureHomeserverField(): ValidatedInputBox {
        model.homeserver.addValidator(homeserverInputBox, trigger = ValidationTrigger.None, validator = validators.homeserver)
        model.homeserver.onChange {
            if (homeserverInputBox.decorateAndValidate) {
                model.validate(focusFirstError = false)
            }
        }
        homeserverInputField.focusedProperty().onChange {
            homeserverInputBox.decorateAndValidate = true
            model.validate(focusFirstError = false)
        }
        return homeserverInputBox
    }

    private fun createPasswordField(): ValidatedInputBox {
        val passwordInput = ValidatedInputBox(passwordField)

        with(passwordField) {
            setOnAction {
                passwordInput.decorateAndValidate = true
                model.validate(focusFirstError = false)
                startLoginFromGui()
            }
            focusedProperty().onChange {
                homeserverInputBox.decorateAndValidate = true
                if (!it) {
                    passwordInput.decorateAndValidate = true
                    model.validate(focusFirstError = false)
                }
            }
            textProperty().onChange {
                passwordInput.decorateAndValidate = true
                model.validate(focusFirstError = false)
            }
        }
        model.password.addValidator(passwordInput, trigger = ValidationTrigger.None,
            validator = validators.password)
        return passwordInput
    }

    private fun startLoginFromGui() {
        if (model.isValid) {
            startLogin()
        }
    }

    private fun startLogin() {
        Platform.runLater {
            val progressIndicator = uiController.createProgressIndicator(messages["loggingIn"])
            uiController.showProgressCircle(root, progressIndicator)

            runAsync(daemon = true) {
                matrixConnector.login()
            } ui {
                when (it) {
                    MatrixConnector.LoginResult.Success -> {
                        uiController.hideDialog(this)
                        clear()
                    }
                    MatrixConnector.LoginResult.ServerNotFound -> {
                        uiController.hideProgressCircle(root, progressIndicator)
                        loginFailed(messages["loginAttemptFailed"])
                    }
                    MatrixConnector.LoginResult.WrongPasswordOrUserNotFound -> {
                        uiController.hideProgressCircle(root, progressIndicator)
                        loginFailed(messages["loginAttemptFailedPasswordOrUserWrong"])
                    }
                    MatrixConnector.LoginResult.UnknownError -> {
                        uiController.hideProgressCircle(root, progressIndicator)
                        loginFailed(messages["loginAttemptFailed"])
                    }
                }
            }
        }
    }

    private fun loginFailed(error: String) {
        val newTextNode = TextNode(error, LoginDialogStyle.errorFont)

        introductionLabelBox.children.clear()
        introductionLabelBox.add(newTextNode)
    }

    private fun clear() {
        model.localPart.value = ""
        model.domain.value = ""
        model.password.value = ""
        model.homeserver.value = ""
    }
}