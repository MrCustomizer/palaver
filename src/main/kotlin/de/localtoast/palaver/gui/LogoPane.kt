/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui

import de.localtoast.palaver.gui.stylesheet.FontStyle
import de.localtoast.palaver.gui.stylesheet.PalaverStyle
import javafx.geometry.Pos
import javafx.scene.image.Image
import javafx.scene.layout.HBox
import tornadofx.addClass
import tornadofx.hboxConstraints
import tornadofx.imageview
import tornadofx.label

class LogoPane : HBox(8.0) {
    init {
        hboxConstraints {
            alignment = Pos.CENTER_LEFT
        }

        val height = 45.0

        minHeight = height
        maxHeight = height
        prefHeight = height

        addClass(PalaverStyle.logoPane)

        imageview(Image("/de/localtoast/palaver/logo.png"))

        label {
            text = "Palaver"
            addClass(FontStyle.logo)
        }
    }
}