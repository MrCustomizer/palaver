/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.stylesheet

import de.localtoast.palaver.sys.GraphicUtilities
import javafx.geometry.Pos
import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.c
import tornadofx.cssclass
import tornadofx.multi
import tornadofx.pt
import tornadofx.px

class ChatRoomStyle : Stylesheet() {
    companion object {
        val topicBox by cssclass()
        val topicBoxRoomName by cssclass()
        val chatRoomPanel by cssclass()

        val backlogPanel by cssclass()
        val backlogPanelPadding by cssclass()
        val backlogAvatarBig by cssclass()
        val backlogAvatarMini by cssclass()
        val backlogUser by cssclass()

        val backlogMessagePadding by cssclass()
        val backlogHeaderPadding by cssclass()
        val backlogTimestamp by cssclass()
        val backlogDayRow by cssclass()
        val backlogStateRowPadding by cssclass()
        val backlogStateRowText by cssclass()

        val readMarkerBox by cssclass()

        val userPanelBox by cssclass()
        val userPanelTitle by cssclass()
        val userPanelText by cssclass()
        val userPanelListBorder by cssclass()
        val userPanelListScrollBar by cssclass()
        val userPanelListCell by cssclass()
        val tabCompletionListView by cssclass()
        val backlogFetchingPanel by cssclass()
        val selectionMarker by cssclass()
    }

    init {
        topicBox {
            backgroundColor += PalaverStyle.boxBackground
            borderWidth += box(0.px, 0.px, 1.px, 0.px)
            borderColor += box(PalaverStyle.borderColor)

            padding = box(10.px)
        }

        topicBoxRoomName {
            +FontStyle.regularMixin
            wrapText = true
            fontSize = 15.pt
            textFill = Color.BLACK
            padding = box(0.px, 0.px, 5.px, 0.px)
        }

        chatRoomPanel {
            backgroundColor += PalaverStyle.defaultBackground
        }

        backlogPanel {
            +PalaverStyle.scrollBarStyleMixin
            +PalaverStyle.scrollBarDefaultBackgroundMixin
            scrollBar {
                backgroundColor += PalaverStyle.defaultBackground
            }
            scrollPane {
                backgroundColor += PalaverStyle.defaultBackground
            }
        }

        backlogPanelPadding {
            padding = box(10.px)
        }

        backlogAvatarBig {
            val widthIntern = GraphicUtilities.avatarSize.px
            minWidth = widthIntern
            maxWidth = widthIntern
            prefWidth = widthIntern

            alignment = Pos.TOP_CENTER
        }

        backlogAvatarMini {
            padding = box(4.px, 0.px, 0.px, 0.px)
            val widthIntern = GraphicUtilities.avatarSize.px
            minWidth = widthIntern
            maxWidth = widthIntern
            prefWidth = widthIntern

            alignment = Pos.TOP_CENTER
        }

        backlogUser {
            +FontStyle.regularMixin
            textFill = c("#4a97cb")
            padding = box(0.px, 0.px, 0.px, 10.px)
        }

        backlogMessagePadding {
            padding = box(4.px, 0.px, 0.px, 10.px)
        }

        backlogHeaderPadding {
            padding = box(15.px, 0.px, 0.px, 0.px)
        }

        backlogTimestamp {
            +FontStyle.regularMixin
            fontSize = 10.px
            padding = box(5.px, 5.px, 0.px, 10.px)
        }

        backlogDayRow {
            +FontStyle.regularMixin
            textFill = PalaverStyle.textColorGrey
            padding = box(12.px, 0.px, 0.px, 10.px)
        }

        backlogStateRowPadding {
            padding = box(10.px, 0.px, 0.px, 0.px)
        }

        backlogStateRowText {
            +FontStyle.regularMixin
            fill = PalaverStyle.textColorGrey
        }

        readMarkerBox {
            backgroundInsets += box(0.px, 5.px, 0.px, 5.px)
            backgroundColor += PalaverStyle.lightBlue
            minHeight = 1.px
            prefHeight = minHeight
            maxHeight = minHeight
        }

        userPanelBox {
            borderWidth += box(0.px, 0.px, 0.px, 1.px)
            borderColor += box(PalaverStyle.borderColor)
        }

        userPanelTitle {
            +FontStyle.regularMixin
            fontSize = 9.pt
            backgroundColor += PalaverStyle.boxBackground
            padding = box(10.px, 0.px, 10.px, 15.px)
        }

        userPanelText {
            +FontStyle.regularMixin
        }

        userPanelListBorder {
            padding = box(0.px, 10.px, 0.px, 10.px)
        }

        userPanelListScrollBar {
            +PalaverStyle.scrollBarStyleMixin
            +PalaverStyle.scrollBarDefaultBackgroundMixin
        }

        userPanelListCell {
            and(hover) {
                backgroundColor += c("#f8f8f8")
            }
        }

        tabCompletionListView {
            // Remove list view'monoRegular border
            backgroundInsets += box(0.px)
            padding = box(0.px, 8.px, 0.px, 8.px)
            fontSize = 11.px

            +PalaverStyle.scrollBarStyleMixin
            +PalaverStyle.scrollBarDefaultBackgroundMixin

            cell {
                selectionMarker {
                    prefHeight = 2.px
                    backgroundColor += Color.TRANSPARENT
                }
                backgroundColor += PalaverStyle.defaultBackground

                and(selected) {
                    selectionMarker {
                        backgroundColor = multi(PalaverStyle.lightBlue)
                    }
                }
            }
        }

        backlogFetchingPanel {
            backgroundInsets += box(4.px)
            val height = 30.px
            prefHeight = height
            minHeight = height
            maxHeight = height
        }

        backlogFetchingPanel {
            +FontStyle.regularMixin
        }
    }
}