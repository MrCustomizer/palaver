/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.safety.Whitelist

object HtmlOperations {
    fun sanitize(unsafe: String): String {
        // Only allow the tags which Riot allows, too
        // https://docs.google.com/document/d/1QPncBmMkKOo6_B2jyBuy5FFSZJrRsq7WU5wgRSzOMho/edit
        return Jsoup.clean(unsafe, "https://matrix.to", Whitelist()
            .addTags(
                "a", "strong", "b", "em", "i", "u", "code", "strike", "del", "font",
                "h1", "h2", "h3", "h4", "h5", "h6", "blockquote", "ul", "ol", "nl", "li", "hr",
                "br", "p", "div", "table", "thead", "tbody", "tr", "th", "td", "pre", "span")

            .addAttributes("a", "href")
            .addAttributes("font", "data-mx-color")
            .addAttributes("font", "data-mx-bg-color")
            .addAttributes("span", "data-mx-color")
            .addAttributes("span", "data-mx-bg-color")

            .addProtocols("a", "href", "http", "https", "#")

            .addEnforcedAttribute("a", "rel", "nofollow")
            .preserveRelativeLinks(false),
            Document.OutputSettings().prettyPrint(false)

        )
    }

    fun sanitizeNoHtml(unsafe: String): String {
        return Jsoup.clean(unsafe, Whitelist.none())
    }

    /**
     * Replaces every link in the cellText with a corresponding html link tag
     */
    fun makeTextToLinks(text: String?): String {
        if (text != null) {
            val pattern = """((http://)|(https://)|(www.))[^\s]+"""

            return text.replace(pattern.toRegex()) {
                val index = it.range.first

                if (index == 0 || (index > 0 && text.substring(index - 1, index) != "\"")) {
                    """<a href="${it.value}">${it.value}</a>"""
                } else {
                    it.value
                }
            }
        }
        return ""
    }
}