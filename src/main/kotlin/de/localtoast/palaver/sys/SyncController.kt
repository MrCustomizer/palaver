/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys

import de.localtoast.palaver.sys.events.MatrixEventProcessor
import io.kamax.matrix.client.MatrixClientRequestException
import io.kamax.matrix.client._SyncData
import io.kamax.matrix.client.regular.SyncOptions
import org.slf4j.LoggerFactory
import tornadofx.Controller

class SyncController : Controller() {
    private val client: PalaverHttpClient by inject()
    private val eventProcessor: MatrixEventProcessor by inject()
    private val logger = LoggerFactory.getLogger(this.javaClass.name)

    fun startSyncing(): Boolean {
        var retries = 10
        while (retries > 0) {
            logger.debug("Starting sync, retries left: $retries")
            retries--
            val syncData = try {
                sync(0)
            } catch (e: MatrixClientRequestException) {
                null
            }
            if (syncData != null) {
                eventProcessor.processSyncResult(syncData)
                syncInBackground(syncData.nextBatchToken())
                return true
            }
        }
        return false
    }

    private fun syncInBackground(nextBatchToken: String) {
        logger.debug("Starting background sync")
        runAsync(daemon = true) {
            try {
                val syncData = sync(3000, nextBatchToken)
                eventProcessor.processSyncResult(syncData)
                syncInBackground(syncData.nextBatchToken())
            } catch (e: MatrixClientRequestException) {
                logger.debug("Background sync: trouble with the connection. Retrying.", e)
                // Try again on connection failure
                syncInBackground(nextBatchToken)
            }
        }
    }

    private fun sync(timeout: Long, nextBatchToken: String? = null): _SyncData {
        with(SyncOptions.build()) {
            setSince(nextBatchToken)
            setTimeout(timeout)
            return client.sync(get())
        }
    }
}