/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom

import de.localtoast.palaver.gui.stylesheet.ChatRoomStyle
import de.localtoast.palaver.gui.stylesheet.PalaverStyle
import de.localtoast.palaver.sys.StringMatcher
import de.localtoast.palaver.sys.users.UserRepository
import io.kamax.matrix._MatrixID
import javafx.application.Platform
import javafx.beans.property.SimpleStringProperty
import javafx.collections.transformation.FilteredList
import javafx.geometry.Orientation
import javafx.scene.control.ListView
import javafx.scene.paint.Color
import javafx.scene.text.Text
import tornadofx.add
import tornadofx.addClass
import tornadofx.cellFormat
import tornadofx.onChange
import tornadofx.pane
import tornadofx.textflow
import tornadofx.vbox
import java.util.function.Predicate

class TabCompletionBox(
    private val filteredList: FilteredList<_MatrixID>,
    private val searchString: SimpleStringProperty,
    private val userRepo: UserRepository
) : ListView<_MatrixID>(filteredList) {

    init {
        addClass(ChatRoomStyle.tabCompletionListView)
        orientation = Orientation.HORIZONTAL
        val height = BacklogView.notificationAreaHeight
        prefHeight = height
        maxHeight = height
        minHeight = height

        isVisible = false

        val listItemHeight = 12.0

        cellFormat {
            graphic = vbox(2.0) {
                prefHeight = listItemHeight + 5
                maxHeight = listItemHeight + 5
                minHeight = listItemHeight + 5
                textflow {
                    var usernameChanged = true
                    val user = userRepo.getUser(it)
                    val username = user.name.value
                    val letterList = username.map { Text(it.toString()) }.toMutableList()
                    if (this@TabCompletionBox.visibleProperty().value) {
                        letterList.forEach { add(it) }
                        setCharacterColors(username, searchString.value, letterList)
                    }

                    prefHeight = listItemHeight
                    maxHeight = listItemHeight
                    minHeight = listItemHeight

                    user.name.onChange {
                        if (this@TabCompletionBox.visibleProperty().value) {
                            Platform.runLater {
                                if (it != null) {
                                    this@textflow.children.clear()
                                    letterList.clear()
                                    letterList.addAll(it.map { Text(it.toString()) })
                                    letterList.forEach { this@textflow.add(it) }
                                    setCharacterColors(it, searchString.value, letterList)
                                }
                            }
                        } else {
                            usernameChanged = true
                        }
                    }
                    searchString.onChange {
                        if (this@TabCompletionBox.visibleProperty().value) {
                            setCharacterColors(user.name.value, it, letterList)
                        }
                    }
                    this@TabCompletionBox.visibleProperty().onChange {
                        if (it) {
                            if (usernameChanged) {
                                usernameChanged = false
                                this@textflow.children.clear()
                                letterList.clear()
                                letterList.addAll(user.name.value.map { Text(it.toString()) })
                                letterList.forEach { this@textflow.add(it) }
                                setCharacterColors(user.name.value, searchString.value, letterList)
                            }
                        }
                    }
                }
                pane {
                    addClass(ChatRoomStyle.selectionMarker)
                }
            }
        }

        searchString.onChange { matchString ->
            if (matchString != null) {
                filteredList.predicate = Predicate {
                    StringMatcher.getMatchPositions(userRepo.getUser(it).name.value, matchString).size == matchString.length
                }
            }
        }
    }

    private fun setCharacterColors(username: String, searchString: String?, letterList: List<Text>) {
        val positions = StringMatcher.getMatchPositions(username, searchString)

        if (letterList.size == username.length) {
            for (i in 0 until username.length) {
                when {
                    positions.contains(i) -> letterList[i].fill = PalaverStyle.lightBlue
                    else -> letterList[i].fill = Color.BLACK
                }
            }
        }
    }
}