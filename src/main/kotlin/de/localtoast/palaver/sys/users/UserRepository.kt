/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys.users

import de.localtoast.palaver.sys.PalaverHttpClient
import io.kamax.matrix._MatrixID
import tornadofx.Controller

class UserRepository : Controller() {
    private val users = mutableMapOf<String, User>()

    private val client: PalaverHttpClient by inject()

    fun getUser(id: _MatrixID): User {
        return users[id.id] ?: addUser(id)
    }

    fun addUser(user: User) {
        if (!users.containsKey(user.id.id)) {
            users[user.id.id] = user
        }
    }

    fun containsUser(id: _MatrixID): Boolean {
        return users.containsKey(id.id)
    }

    private fun addUser(id: _MatrixID): User {
        val user = User(id = id, client = client)
        users[id.id] = user
        return user
    }
}