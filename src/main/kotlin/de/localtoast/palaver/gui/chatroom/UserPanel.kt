/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom

import de.localtoast.palaver.gui.stylesheet.ChatRoomStyle
import de.localtoast.palaver.gui.stylesheet.PalaverStyle
import de.localtoast.palaver.sys.GraphicUtilities
import de.localtoast.palaver.sys.StringMatcher
import de.localtoast.palaver.sys.chatroom.RoomRepository
import de.localtoast.palaver.sys.users.UserRepository
import io.kamax.matrix._MatrixID
import javafx.application.Platform
import javafx.collections.transformation.FilteredList
import javafx.collections.transformation.SortedList
import javafx.geometry.Pos
import javafx.scene.image.Image
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import org.fxmisc.flowless.Cell
import org.fxmisc.flowless.VirtualFlow
import org.fxmisc.flowless.VirtualizedScrollPane
import tornadofx.Fragment
import tornadofx.addClass
import tornadofx.box
import tornadofx.get
import tornadofx.hbox
import tornadofx.hboxConstraints
import tornadofx.imageview
import tornadofx.label
import tornadofx.onChange
import tornadofx.px
import tornadofx.style
import tornadofx.textfield
import tornadofx.vbox
import tornadofx.vboxConstraints
import java.util.function.Predicate

class UserPanel : Fragment() {
    companion object {
        const val paramRoomId = "roomId"
    }

    private val userRepo: UserRepository by inject()

    private val roomId = if (params[paramRoomId] != null) params[paramRoomId] as String else ""
    private val room = RoomRepository.rooms.getValue(roomId)
    private val filteredUsers = FilteredList(room.users)
    private val sortedAndFilteredUsers = SortedList(filteredUsers).apply {
        setComparator { item1, item2 ->
            userRepo.getUser(item1).name.value.compareTo(userRepo.getUser(item2).name.value, ignoreCase = true)
        }
    }

    override val root = vbox {
        minWidth = 200.0
        maxWidth = 200.0
        maxWidth = 200.0

        addClass(ChatRoomStyle.userPanelBox)

        label(messages["roomMembers"]) {
            addClass(ChatRoomStyle.userPanelTitle)
            minWidthProperty().bind(this@vbox.minWidthProperty())
            prefWidthProperty().bind(this@vbox.prefWidthProperty())
        }

        hbox {
            style {
                padding = box(10.px)
            }
            hbox {
                hboxConstraints {
                    hGrow = Priority.ALWAYS
                }
                addClass(PalaverStyle.filterBoxLight)
                hbox {
                    hboxConstraints {
                        hGrow = Priority.ALWAYS
                    }
                    addClass(PalaverStyle.filterBoxPaddingLight)

                    alignment = Pos.CENTER_LEFT

                    imageview(Image("/de/localtoast/palaver/icons/actions/material.io/outline_search_black_20px.png")) {
                        opacity = 0.4
                    }

                    textfield {
                        hboxConstraints {
                            hGrow = Priority.ALWAYS
                        }
                        addClass(PalaverStyle.filterBoxInputFieldLight)
                        promptText = messages["filter"]
                        textProperty().onChange {
                            filteredUsers.predicate = Predicate {
                                StringMatcher.getMatchPositions(userRepo.getUser(it).name.value, text).size == text.length
                            }
                        }
                    }
                }
            }
        }

        val list = VirtualFlow.createVertical(sortedAndFilteredUsers, { user -> renderCell(user) })

        hbox {
            vboxConstraints {
                vGrow = Priority.ALWAYS
            }

            addClass(ChatRoomStyle.userPanelListBorder)
            add(VirtualizedScrollPane(list).apply {
                hboxConstraints {
                    hGrow = Priority.ALWAYS
                }
                addClass(ChatRoomStyle.userPanelListScrollBar)
            })
        }
    }

    private fun renderCell(userId: _MatrixID): Cell<_MatrixID, HBox> {
        val user = userRepo.getUser(userId)
        val hBox = HBox(4.0).apply {
            val cellHeight = GraphicUtilities.avatarSize.toDouble() + 4
            minHeight = cellHeight
            maxHeight = cellHeight
            prefHeight = cellHeight
            alignment = Pos.CENTER_LEFT

            addClass(ChatRoomStyle.userPanelListCell)

            hbox {
                val width = GraphicUtilities.avatarSize.toDouble() + 4
                minWidth = width
                maxWidth = width
                prefWidth = width
                alignment = Pos.CENTER
                imageview {
                    image = user.scaledAndCutOutAvatar.value

                    user.scaledAndCutOutAvatar.onChange {
                        Platform.runLater {
                            image = it
                        }
                    }
                }
            }
            label {
                addClass(ChatRoomStyle.userPanelText)
                text = user.name.value
                user.name.onChange {
                    Platform.runLater {
                        text = it
                    }
                }
            }
            RoomRepository.rooms[roomId]?.userMetadataFetchQueue?.add(user)
        }

        return Cell.wrapNode<_MatrixID, HBox>(hBox)
    }
}