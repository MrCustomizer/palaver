/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.stylesheet

import tornadofx.Stylesheet
import tornadofx.c
import tornadofx.cssclass
import tornadofx.pt

class ValidationStyle : Stylesheet() {
    companion object {
        val inputBoxInvalid by cssclass()
        val validationLabelError by cssclass()
        val validationLabelWarning by cssclass()
    }

    init {
        inputBoxInvalid {
            unsafe("-fx-control-inner-background", c("#ffc2c2"))
        }

        validationLabelError {
            +FontStyle.regularMixin
            textFill = c("#b50000")
            fontSize = 9.pt
        }

        validationLabelWarning {
            +FontStyle.regularMixin
            textFill = c("#b37e00")
            fontSize = 9.pt
        }
    }
}