/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.text

import de.localtoast.palaver.gui.fragments.validatedinputbox.HyperlinkText
import de.localtoast.palaver.gui.stylesheet.TextStyles
import de.localtoast.palaver.sys.HtmlOperations
import javafx.scene.text.Text
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.nodes.Node
import org.jsoup.nodes.TextNode
import org.jsoup.select.NodeVisitor
import tornadofx.addClass

/**
 * Helper class for generating formatted TextFlow objects
 */
object TextFlowHelper {
    fun createTextFlows(completeText: String): List<TextFlowWithSelection> {
        val sanitizedContent = HtmlOperations.sanitize(HtmlOperations.makeTextToLinks(completeText))
        val body = Jsoup.parseBodyFragment(sanitizedContent).apply { outputSettings().prettyPrint(false) }.body()

        val textFlows = mutableListOf<TextFlowWithSelection>()

        var currentTextFlow = TextFlowWithSelection()
        textFlows.add(currentTextFlow)
        var textCounter = 0
        var newLine = false

        body.traverse(object : NodeVisitor {
            override fun head(htmlNode: Node?, depth: Int) {
                if (htmlNode != null) {
                    when (htmlNode) {
                        is TextNode -> {
                            val parentTags = getParentTagnames(htmlNode)

                            val text = htmlNode.wholeText
                            textCounter++
                            if (newLine) {
                                newLine = false
                                currentTextFlow = TextFlowWithSelection()
                                textFlows.add(currentTextFlow)
                            }

                            val newNode = when {
                                parentTags.contains("a") -> {
                                    val href = htmlNode.parent().attributes()["href"]
                                    HyperlinkText(text, href)
                                }
                                else -> {
                                    Text(text)
                                }
                            }

                            when {
                                parentTags.contains("h1") -> {
                                    newNode.addClass(TextStyles.messageH1)
                                    newLine = true
                                }
                                parentTags.contains("h2") -> {
                                    newNode.addClass(TextStyles.messageH2)
                                    newLine = true
                                }
                                parentTags.contains("h3") -> {
                                    newNode.addClass(TextStyles.messageH3)
                                    newLine = true
                                }
                                parentTags.contains("h4") -> {
                                    newNode.addClass(TextStyles.messageH4)
                                    newLine = true
                                }
                                parentTags.contains("h5") -> {
                                    newNode.addClass(TextStyles.messageH5)
                                    newLine = true
                                }
                                parentTags.contains("h6") -> {
                                    newNode.addClass(TextStyles.messageH6)
                                    newLine = true
                                }
                                parentTags.contains("strong") && parentTags.contains("em") -> {
                                    when {
                                        parentTags.contains("code") -> newNode.addClass(TextStyles.codeBoldItalic)
                                        else -> newNode.addClass(TextStyles.messageBoldItalic)
                                    }
                                }
                                parentTags.contains("em") -> {
                                    when {
                                        parentTags.contains("code") -> newNode.addClass(TextStyles.codeItalic)
                                        else -> newNode.addClass(TextStyles.messageItalic)
                                    }
                                }
                                parentTags.contains("strong") -> {
                                    when {
                                        parentTags.contains("code") -> newNode.addClass(TextStyles.codeBold)
                                        else -> newNode.addClass(TextStyles.messageBold)
                                    }
                                }
                                else -> {
                                    when {
                                        parentTags.contains("code") -> newNode.addClass(TextStyles.code)
                                        else -> newNode.addClass(TextStyles.message)
                                    }
                                }
                            }

                            if (
                                parentTags.contains("li")) {
                                newLine = true
                                when {
                                    parentTags.contains("ul") -> {
                                        newNode.text = "• ${newNode.text}"
                                    }
                                    parentTags.contains("ol") -> {
                                        newNode.text = "$textCounter. ${newNode.text}"
                                    }
                                }
                            }

                            if (parentTags.contains("del")) {
                                newNode.addClass(TextStyles.messageStrikethrough)
                            }
                            if (parentTags.contains("u")) {
                                newNode.addClass(TextStyles.messageUnderline)
                            }
                            if (parentTags.contains("blockquote")) {
                                currentTextFlow.addClass(TextStyles.textFlowCitation)
                                newNode.addClass(TextStyles.messageCitation)
                                newLine = true
                            }
                            if (parentTags.contains("p")) {
                                newLine = true
                            }

                            currentTextFlow.children.add(newNode)
                        }
                    }
                }
            }

            override fun tail(node: Node?, depth: Int) {
                // nothing to do here
            }
        })

        return textFlows
    }

    fun getParentTagnames(htmlNode: Node?): List<String> {
        val tags = mutableListOf<String>()
        var traverseNode = htmlNode
        while (traverseNode?.parent() != null) {
            val parent = traverseNode.parent() as Element
            val tag = parent.tagName()
            if (tag != null &&
                tag != "body" &&
                tag != "html" &&
                tag != "#root") {
                tags.add(tag)
            }

            traverseNode = parent
        }
        return tags
    }
}