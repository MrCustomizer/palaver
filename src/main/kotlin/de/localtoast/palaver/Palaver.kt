/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver

import de.localtoast.palaver.gui.MainWindow
import de.localtoast.palaver.gui.stylesheet.ChatRoomStyle
import de.localtoast.palaver.gui.stylesheet.FontStyle
import de.localtoast.palaver.gui.stylesheet.InputBoxStyle
import de.localtoast.palaver.gui.stylesheet.LoginDialogStyle
import de.localtoast.palaver.gui.stylesheet.PalaverStyle
import de.localtoast.palaver.gui.stylesheet.ReadMarkerListPanelStyle
import de.localtoast.palaver.gui.stylesheet.SettingsStyle
import de.localtoast.palaver.gui.stylesheet.SideBarStyle
import de.localtoast.palaver.gui.stylesheet.TextStyles
import de.localtoast.palaver.gui.stylesheet.ValidationStyle
import de.localtoast.palaver.sys.HotkeyManager
import javafx.application.Application
import javafx.scene.image.Image
import javafx.scene.input.KeyEvent
import javafx.stage.Screen
import javafx.stage.Stage
import org.slf4j.LoggerFactory
import tornadofx.App
import tornadofx.FX
import tornadofx.onChange
import tornadofx.reloadStylesheetsOnFocus

class Palaver : App(MainWindow::class, PalaverStyle::class, FontStyle::class, SideBarStyle::class,
    ChatRoomStyle::class, ValidationStyle::class, InputBoxStyle::class, LoginDialogStyle::class,
    TextStyles::class, ReadMarkerListPanelStyle::class, SettingsStyle::class) {
    private val mainWindow: MainWindow by inject()
    private val hotkeyManager: HotkeyManager by inject()
    private val logger = LoggerFactory.getLogger(this.javaClass.name)

    init {
        reloadStylesheetsOnFocus()
    }

    override fun start(stage: Stage) {
        val profile = parameters.named["profile"]
        Settings.initSettings(profile)

        FX.locale = Settings.localeDisplay
        logger.debug("Locale display: ${Settings.localeDisplay}")
        logger.debug("Locale format: ${Settings.localeFormat}")
        stage.icons.add(Image("/de/localtoast/palaver/logoIcon.png"))

        var windowCanBeRestored = false
        Screen.getScreens().forEach {
            val visualBounds = it.visualBounds
            if (visualBounds.minX <= Settings.windowPosX &&
                visualBounds.maxX > Settings.windowPosX + Settings.windowWidth &&
                visualBounds.minY <= Settings.windowPosY &&
                visualBounds.maxY > Settings.windowPosY + Settings.windowHeight) {
                windowCanBeRestored = true
            }
        }

        if ((Settings.windowHeight != 0 && Settings.windowWidth != 0 && windowCanBeRestored) || Settings.windowMaximized) {
            restoreWindow(stage)
        } else {
            Settings.windowPosX = 0
            Settings.windowPosY = 0
            Settings.windowWidth = 0
            Settings.windowHeight = 0
            showOnPrimaryScreen(stage)
        }

        bindWindowPosAndSizeToSettingsFile(stage)

        super.start(stage)

        stage.scene.addEventFilter(KeyEvent.KEY_PRESSED) { event -> hotkeyManager.handle(event) }

        mainWindow.initConnection()
    }

    private fun restoreWindow(stage: Stage) {
        if (Settings.windowMaximized) {
            stage.isMaximized = Settings.windowMaximized
            stage.x = Settings.windowPosX.toDouble()
            stage.y = Settings.windowPosY.toDouble()

            // Saved size when maximized is always a little too big
            stage.width = Settings.windowWidth.toDouble() - 100
            stage.height = Settings.windowHeight.toDouble() - 100
            stage.centerOnScreen()
        } else {
            stage.x = Settings.windowPosX.toDouble()
            stage.y = Settings.windowPosY.toDouble()
            stage.width = Settings.windowWidth.toDouble()
            stage.height = Settings.windowHeight.toDouble()
        }
    }

    private fun showOnPrimaryScreen(stage: Stage) {
        val visualBounds = Screen.getPrimary().visualBounds

        stage.width = visualBounds.width - 200
        stage.height = visualBounds.height - 200
        stage.x = (visualBounds.width - stage.width) / 2
        stage.y = (visualBounds.height - stage.height) / 2
    }

    private fun bindWindowPosAndSizeToSettingsFile(stage: Stage) {
        stage.xProperty().onChange { Settings.windowPosX = it.toInt() }
        stage.yProperty().onChange { Settings.windowPosY = it.toInt() }
        stage.widthProperty().onChange { Settings.windowWidth = it.toInt() }
        stage.heightProperty().onChange { Settings.windowHeight = it.toInt() }
        stage.maximizedProperty().onChange { Settings.windowMaximized = it }
    }
}

fun main(args: Array<String>) {
    Application.launch(Palaver::class.java, *args)
}