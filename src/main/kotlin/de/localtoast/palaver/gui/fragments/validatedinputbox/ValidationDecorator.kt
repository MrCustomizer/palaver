/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.fragments.validatedinputbox

import de.localtoast.palaver.gui.stylesheet.ValidationStyle
import javafx.application.Platform
import javafx.scene.Node
import tornadofx.Decorator
import tornadofx.ValidationSeverity
import tornadofx.addClass
import tornadofx.removeClass

class ValidationDecorator(
    private val message: String?,
    private val severity: ValidationSeverity
) : Decorator {

    override fun decorate(node: Node) {
        Platform.runLater({
            if (severity == ValidationSeverity.Error) {
                node.addClass(ValidationStyle.inputBoxInvalid)
            }

            if (node is ValidatedInputBox) {
                when (severity) {
                    ValidationSeverity.Warning -> node.setWarning(message ?: "")
                    ValidationSeverity.Error -> node.setError(message ?: "")
                    else -> node.setSuccess()
                }
            }
        })
    }

    override fun undecorate(node: Node) {
        Platform.runLater({
            node.removeClass(ValidationStyle.inputBoxInvalid)
            if (node is ValidatedInputBox) {
                node.setSuccess()
            }
        })
    }
}
