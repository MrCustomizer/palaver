/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.text

import de.localtoast.palaver.gui.chatroom.backlog.SelectionIndex
import de.localtoast.palaver.gui.chatroom.backlog.SelectionIndexInCell
import de.localtoast.palaver.gui.stylesheet.ChatRoomStyle
import de.localtoast.palaver.sys.DateTimeHelper
import de.localtoast.palaver.sys.events.Event
import de.localtoast.palaver.sys.events.HistoryVisibilityEvent
import de.localtoast.palaver.sys.events.MembershipEvent
import de.localtoast.palaver.sys.events.MessageEvent
import de.localtoast.palaver.sys.events.NameEvent
import de.localtoast.palaver.sys.events.PowerLevelEvent
import de.localtoast.palaver.sys.events.TopicEvent
import de.localtoast.palaver.sys.users.UserRepository
import javafx.scene.image.ImageView
import javafx.scene.layout.VBox
import javafx.scene.text.TextFlow
import tornadofx.Fragment
import tornadofx.addClass
import tornadofx.get
import tornadofx.text

/**
 * Displays formatted text and images of events (with support for links)
 */
class EventNode : Fragment() {
    val eventParam: Event by param()

    private val userRepo: UserRepository by inject()

    private val textFlows = mutableListOf<TextFlowWithSelection>()

    override val root = VBox()

    init {
        val event = eventParam
        when (event) {
            is MessageEvent -> {
                if (event.image != null) {
                    val imageView = ImageView().apply {
                        image = event.image
                        fitWidthProperty().bind(root.widthProperty())
                        isPreserveRatio = true
                    }
                    // Wrap image view in text flow, so it gets scaled properly
                    val textFlow = TextFlow()
                    textFlow.children.add(imageView)
                    root.add(textFlow)
                } else {
                    textFlows.addAll(TextFlowHelper.createTextFlows(event.content))
                }
            }
            is TopicEvent -> {
                textFlows.add(TextFlowWithSelection().apply {
                    text(createTopicString(event)).apply {
                        addClass(ChatRoomStyle.backlogStateRowText)
                    }
                })
            }

            is NameEvent -> {
                textFlows.add(TextFlowWithSelection().apply {
                    text(createRoomNameString(event)).apply {
                        addClass(ChatRoomStyle.backlogStateRowText)
                    }
                })
            }

            is MembershipEvent -> {
                textFlows.add(TextFlowWithSelection().apply {
                    text(createMembershipString(event)).apply {
                        addClass(ChatRoomStyle.backlogStateRowText)
                    }
                })
            }

            is PowerLevelEvent -> {
                textFlows.add(TextFlowWithSelection().apply {
                    text(createPowerLevelString(event)).apply {
                        addClass(ChatRoomStyle.backlogStateRowText)
                    }
                })
            }

            is HistoryVisibilityEvent -> {
                textFlows.add(TextFlowWithSelection().apply {
                    text(createHistoryVisibilityString(event)).apply {
                        addClass(ChatRoomStyle.backlogStateRowText)
                    }
                })
            }
        }

        val textContainer = VBox()

        textFlows.forEach {
            textContainer.add(it)
        }
        root.add(textContainer)
    }

    private fun createPowerLevelString(event: PowerLevelEvent): String {
        val sender = userRepo.getUser(event.sender)

        val content = messages["hasChangedPowerLevel"].replace("#powerLevel", event.newLevel.toString())
            .replace("#user", event.users)

        return content.replace("#name", sender.name.value ?: "")
    }

    private fun createHistoryVisibilityString(event: HistoryVisibilityEvent): String {
        val sender = userRepo.getUser(event.sender)
        val visibility = event.visibility

        val content = when (visibility) {
            "invited" -> messages["hasChangedRoomVisibilityToInvited"]
            "joined" -> messages["hasChangedRoomVisibilityToJoined"]
            "shared" -> messages["hasChangedRoomVisibilityToShared"]
            "world_readable" -> messages["hasChangedRoomVisibilityToWorldReadable"]
            else -> ""
        }

        return content.replace("#name", sender.name.value ?: "")
    }

    private fun createMembershipString(event: MembershipEvent): String {
        val sender = userRepo.getUser(event.sender)
        val name = event.displayName
        val avatarUrl = event.avatarUrl
        val membership = event.membership

        val oldName = event.prevDisplayName
        val oldAvatarUrl = event.prevAvatarUrl
        val oldMembership = event.prevMembership

        val content = when {
            oldMembership != membership -> {
                when (membership) {
                    "invite" -> messages["hasInvited"]
                    "join" -> messages["hasJoined"]
                    "leave" -> messages["hasLeft"]
                    "ban" -> messages["hasBeenBanned"]
                    "kick" -> messages["hasBeenKicked"]
                    else -> ""
                }
            }

            oldName.isNullOrBlank() && name != oldName -> messages["hasSetProfileName"]
            !oldName.isNullOrBlank() && name.isNullOrBlank() -> messages["hasRemovedProfileName"]
            name != oldName -> messages["hasChangedProfileName"]

            oldAvatarUrl.isNullOrBlank() && avatarUrl != oldAvatarUrl -> messages["hasSetAvatar"]
            !oldAvatarUrl.isNullOrBlank() && avatarUrl.isNullOrBlank() -> messages["hasRemovedAvatar"]
            avatarUrl != oldAvatarUrl -> messages["hasChangedAvatar"]

            else -> ""
        }

        val invitee = userRepo.getUser(event.invitee).name.value ?: ""

        return content.replace("#name", name ?: "")
            .replace("#oldName", oldName ?: "")
            .replace("#newName", name ?: "")
            .replace("#leaver", sender.name.value)
            .replace("#inviter", sender.name.value)
            .replace("#invitee", invitee)
            .replace("#kicker", sender.name.value ?: "")
            .replace("#kickee", oldName ?: "")
            .replace("#banner", sender.name.value ?: "")
            .replace("#bannee", oldName ?: "")
    }

    private fun createRoomNameString(event: NameEvent): String {
        val sender = userRepo.getUser(event.sender)
        val name = event.name
        val content = if (name != null) {
            messages["hasChangedRoomNameTo"].replace("#newName", name)
        } else ""

        return content.replace("#name", sender.name.value)
    }

    private fun createTopicString(event: TopicEvent): String {
        val sender = userRepo.getUser(event.sender)
        val topic = event.topic
        val content = if (topic != null) {
            messages["hasChangedTopicTo"].replace("#topic", topic)
        } else ""

        return content.replace("#name", sender.name.value)
    }

    fun nodeHit(screenX: Double, screenY: Double): Int? {
        textFlows.forEach {
            val point = root.screenToLocal(screenX, screenY)
            if (it.boundsInParent.contains(point)) {
                return textFlows.indexOf(it)
            }
        }
        return null
    }

    fun characterHit(screenX: Double, screenY: Double): Int? {
        val nodeHitIndex = nodeHit(screenX, screenY)
        return if (nodeHitIndex != null) {
            textFlows[nodeHitIndex].characterHit(screenX, screenY)
        } else null
    }

    fun setSelection(start: SelectionIndexInCell, end: SelectionIndexInCell) {
        val indices = sortedSetOf(start, end)
        val range = when {
            start < end -> IntRange(start.nodeIndex, end.nodeIndex)
            else -> IntRange(end.nodeIndex, start.nodeIndex)
        }

        for (i in range) {
            val node = textFlows[i]

            val startIndex = when (i) {
                range.first() -> indices.first().charIndex
                else -> 0
            }

            val endIndex = when (i) {
                range.last() -> indices.last().charIndex
                else -> node.charLength() - 1
            }

            node.selection = IntRange(startIndex, endIndex)
        }
    }

    fun lastTextFlowIndex(): Int {
        return textFlows.size - 1
    }

    fun lastTextLength(): Int? {
        return if (textFlows.size > 0) {
            textFlows.last().charLength()
        } else null
    }

    fun clearSelection() {
        textFlows.forEach { it.selection = IntRange.EMPTY }
    }

    fun getText(start: SelectionIndex?, end: SelectionIndex?): String {
        val startNodeIndex = start?.indexInCell?.nodeIndex ?: 0
        val startAtBeginning = if (start?.indexInCell != null) start.indexInCell.charIndex == 0 else startNodeIndex == 0
        var text = if (startAtBeginning) {
            "${DateTimeHelper.formatTimestampTime(eventParam.originServerTimestamp)}: "
        } else ""

        val endNodeIndex = end?.indexInCell?.nodeIndex ?: textFlows.size - 1

        for (i in startNodeIndex..endNodeIndex) {
            text += textFlows[i].text(start?.indexInCell?.charIndex, end?.indexInCell?.charIndex)
        }
        return text
    }
}