/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys

import javafx.embed.swing.SwingFXUtils
import javafx.scene.image.WritableImage
import net.coobird.thumbnailator.Thumbnails
import net.coobird.thumbnailator.geometry.Positions
import java.awt.Color
import java.awt.RenderingHints
import java.awt.image.BufferedImage
import java.awt.image.BufferedImage.TYPE_INT_ARGB

object GraphicUtilities {
    const val avatarSize = 28
    const val avatarSizeMini = 16

    /**
     * Cuts out an anti-antialased circle of the image. The background is transparent.
     */
    fun cutOutCircle(image: BufferedImage): WritableImage {
        val size = image.width
        val mask = BufferedImage(size, size, TYPE_INT_ARGB)
        val gMask = mask.createGraphics()
        gMask.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        gMask.paint = Color.black
        gMask.fillRect(0, 0, size, size)
        gMask.color = Color.white
        gMask.fillOval(0, 0, size, size)
        gMask.dispose()

        return applyGrayscaleMaskToAlpha(SwingFXUtils.toFXImage(image, null), SwingFXUtils.toFXImage(mask, null))
    }

    // https://stackoverflow.com/a/8058442/261769
    private fun applyGrayscaleMaskToAlpha(image: WritableImage, mask: WritableImage): WritableImage {
        val writer = image.pixelWriter
        val reader = image.pixelReader
        val maskReader = mask.pixelReader

        for (x in 0 until image.width.toInt()) {
            for (y in 0 until image.height.toInt()) {
                val oldColor = reader.getColor(x, y)
                val brightness = maskReader.getColor(x, y).brightness
                val opacity = brightness * oldColor.opacity
                val c = javafx.scene.paint.Color(oldColor.red, oldColor.green, oldColor.blue, opacity)
                writer.setColor(x, y, c)
            }
        }
        return image
    }

    fun scaleAvatar(bufferedImage: BufferedImage, size: Int): BufferedImage? {
        return Thumbnails.of(bufferedImage)
                .size(size, size)
                .crop(Positions.CENTER)
                .outputFormat("jpg")
                .asBufferedImage()
    }
}