/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.stylesheet

import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

class InputBoxStyle : Stylesheet() {
    companion object {
        val sendMessageInputBoxBorder by cssclass()
        val sendMessageInputBox by cssclass()
        val validatedInputBoxBorder by cssclass()
        val validatedInputBox by cssclass()
    }

    init {
        sendMessageInputBoxBorder {
            backgroundColor += PalaverStyle.boxBackground
            borderColor += box(PalaverStyle.borderColor)
            borderWidth += box(1.px, 0.px, 0.px, 0.px)
            padding = box(6.px)
        }

        sendMessageInputBox {
            +FontStyle.regularMixin

            +PalaverStyle.scrollBarStyleMixin
            +PalaverStyle.scrollBarBoxBackgroundMixin

            focusColor = Color.TRANSPARENT
            textBoxBorder = Color.TRANSPARENT
            content {
                backgroundColor += PalaverStyle.boxBackground
                backgroundRadius += box(0.px)
            }
            and(focused) {
                faintFocusColor = Color.TRANSPARENT
            }
        }

        validatedInputBoxBorder {
            backgroundColor += PalaverStyle.boxBackground
            borderColor += box(PalaverStyle.borderColor)
            borderWidth += box(1.px, 1.px, 1.px, 1.px)
            padding = box(6.px)
        }

        validatedInputBox {
            +FontStyle.regularMixin

            +PalaverStyle.scrollBarStyleMixin
            +PalaverStyle.scrollBarBoxBackgroundMixin

            focusColor = Color.TRANSPARENT
            textBoxBorder = Color.TRANSPARENT
            backgroundColor += PalaverStyle.boxBackground
        }
    }
}