/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom

import de.localtoast.palaver.gui.stylesheet.FontStyle
import de.localtoast.palaver.gui.stylesheet.InputBoxStyle
import de.localtoast.palaver.sys.PalaverHttpClient
import de.localtoast.palaver.sys.chatroom.RoomRepository
import de.localtoast.palaver.sys.users.UserRepository
import io.kamax.matrix._MatrixID
import javafx.beans.property.SimpleStringProperty
import javafx.collections.FXCollections
import javafx.collections.transformation.FilteredList
import javafx.scene.control.TextArea
import javafx.scene.input.KeyCode
import javafx.scene.layout.Priority
import javafx.scene.layout.StackPane
import javafx.scene.text.Text
import tornadofx.Fragment
import tornadofx.addClass
import tornadofx.get
import tornadofx.hboxConstraints
import tornadofx.onChange
import tornadofx.selectedItem
import tornadofx.textarea
import tornadofx.vbox

class SendMessageInputBox : Fragment() {
    companion object {
        val paramRoomId = "roomId"
        val paramNotificationArea = "notificationArea"
    }

    private val client: PalaverHttpClient by inject()
    private val roomId = if (params[paramRoomId] != null) params[paramRoomId] as String else ""
    private val notificationArea = if (params[paramNotificationArea] != null) {
        params[paramNotificationArea] as StackPane
    } else null
    private val userRepo: UserRepository by inject()

    private val searchString = SimpleStringProperty()

    private val filteredList = FilteredList(RoomRepository.rooms[roomId]?.users ?: FXCollections.observableArrayList())

    private val tabCompletionListView = TabCompletionBox(filteredList, searchString, userRepo)

    private val inputBox: TextArea = textarea {
        hboxConstraints {
            hGrow = Priority.ALWAYS
        }
        addClass(InputBoxStyle.sendMessageInputBox)
        promptText = messages["sendUnencryptedMessage"]

        isWrapText = true

        val holder = Text()
        widthProperty().onChange {
            holder.wrappingWidth = it
        }
        var oldHeight = 0.0
        prefHeight = 10.0
        maxHeight = 300.0
        holder.textProperty().bind(textProperty())
        holder.addClass(InputBoxStyle.validatedInputBox)
        holder.layoutBoundsProperty().addListener { _, _, newValue ->
            if (oldHeight != newValue.height) {
                val rows = newValue.height / FontStyle.defaultFontSize.value
                val newHeight = rows * FontStyle.defaultFontSize.value + rows + 10
                if (newHeight <= maxHeight) {
                    minHeight = newHeight
                }
            }
        }

        setOnKeyPressed {
            when {
                it.code == KeyCode.ENTER -> {
                    if (tabCompletionListView.isVisible) {
                        executeTabCompletion()
                    } else {
                        var sendText = text
                        text = ""

                        client.sendMessageToCurrentRoom(sendText)
                    }

                    it.consume()
                }
                it.code == KeyCode.TAB && it.isShiftDown -> {
                    val itemCount = tabCompletionListView.items.size
                    if (itemCount > 1) {
                        if (tabCompletionListView.isVisible) {
                            if (!tabCompletionListView.selectionModel.isSelected(0)) {
                                tabCompletionListView.selectionModel.selectPrevious()
                                tabCompletionListView.scrollTo(tabCompletionListView.selectedItem)
                            }
                        }
                    }
                    it.consume()
                }
                it.code == KeyCode.TAB -> {
                    if (searchString.value != null && searchString.value.isNotEmpty()) {
                        val itemCount = tabCompletionListView.items.size
                        if (itemCount > 1) {
                            if (tabCompletionListView.isVisible) {
                                if (tabCompletionListView.selectionModel.isSelected(itemCount - 1)) {
                                    tabCompletionListView.selectionModel.selectFirst()
                                    tabCompletionListView.scrollTo(tabCompletionListView.selectedItem)
                                } else {
                                    tabCompletionListView.selectionModel.selectNext()
                                    tabCompletionListView.scrollTo(tabCompletionListView.selectedItem)
                                }
                            }
                            if (!tabCompletionListView.isVisible) {
                                tabCompletionListView.isVisible = true
                                tabCompletionListView.selectionModel.selectFirst()
                                tabCompletionListView.scrollTo(tabCompletionListView.selectedItem)
                            }
                        } else if (itemCount == 1) {
                            executeTabCompletion()
                        }
                        it.consume()
                    }
                }
                it.code == KeyCode.ESCAPE -> {
                    hideTabCompletionView()
                    it.consume()
                }
                it.code == KeyCode.SPACE -> {
                    hideTabCompletionView()
                }
            }
        }

        setOnKeyReleased {
            val cursorPos = caretPosition
            searchString.value = getRelevantPartForTabCompletion(text.substring(0, cursorPos))
        }
    }

    private fun hideTabCompletionView() {
        tabCompletionListView.isVisible = false
    }

    init {
        notificationArea?.children?.add(tabCompletionListView)

        // Maintain selection, when the items get filtered
        var lastSelection: _MatrixID? = null

        val selectionModel = tabCompletionListView.selectionModel

        selectionModel.selectedItemProperty().onChange {
            val selection = selectionModel.selectedItem
            if (selection != null) {
                lastSelection = selection
            }
        }

        tabCompletionListView.items.onChange {
            val selectedItem = tabCompletionListView.selectedItem
            if (selectedItem == null && tabCompletionListView.isVisible) {
                if (lastSelection != null && tabCompletionListView.items.contains(lastSelection)) {
                    selectionModel.select(lastSelection)
                } else {
                    selectionModel.selectFirst()
                }
            }
        }
    }

    private fun TextArea.executeTabCompletion() {
        val hit = if (tabCompletionListView.items.size == 1) {
            tabCompletionListView.items[0]
        } else tabCompletionListView.selectedItem

        if (hit != null) {
            val length = text.length
            val insertionIndex = length - searchString.value.length
            val user = userRepo.getUser(hit)
            val name = user.name.value
            text = text.substring(0, insertionIndex) + name
            positionCaret(insertionIndex + name.length)
        }
        hideTabCompletionView()
    }

    override val root = vbox {
        addClass(InputBoxStyle.sendMessageInputBoxBorder)
        add(inputBox)
    }

    private fun getRelevantPartForTabCompletion(text: String): String {
        return text.substringAfterLast(" ")
    }

    fun requestFocus() {
        inputBox.requestFocus()
    }

    fun sendCharacterToInputBox(it: String) {
        inputBox.insertText(inputBox.caretPosition, it)
    }

    fun hasFocus(): Boolean {
        return inputBox.isFocused
    }
}
