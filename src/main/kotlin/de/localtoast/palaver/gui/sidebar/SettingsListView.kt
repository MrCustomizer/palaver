/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.sidebar

import de.localtoast.palaver.gui.settings.GeneralSettingsPanel
import de.localtoast.palaver.gui.settings.UserProfileSettingsPanel
import de.localtoast.palaver.gui.stylesheet.SideBarStyle
import javafx.geometry.Pos
import javafx.scene.control.ListView
import javafx.scene.layout.Pane
import tornadofx.View
import tornadofx.addClass
import tornadofx.box
import tornadofx.get
import tornadofx.hbox
import tornadofx.label
import tornadofx.observable
import tornadofx.onChange
import tornadofx.pane
import tornadofx.px
import tornadofx.style

class SettingsListView : View() {

    val settingsMainArea: Pane by param()

    private val userProfileSettings: UserProfileSettingsPanel by inject()
    private val generalSettingsPanel: GeneralSettingsPanel by inject()

    private val settingsGroups = mutableListOf<String>().observable()

    override val root = ListView<String>()

    init {
        root.apply {
            items = settingsGroups
            style {
                padding = box(10.px, 0.px, 0.px, 0.px)
            }

            val listCellHeight = 34.0

            addClass(SideBarStyle.view)
            cellFormat {
                graphic = hbox(10.0) {
                    maxWidth = SideBarStyle.cellPaneWidth.value - 5 * 4.0
                    pane {
                        addClass(SideBarStyle.selectionMarker)
                    }

                    hbox(4.0) {
                        alignment = Pos.CENTER_LEFT
                        prefHeight = listCellHeight

                        hbox {
                            alignment = Pos.CENTER_LEFT
                            label {
                                text = it
                                addClass(SideBarStyle.cellText)
                            }
                        }
                    }
                }
            }
        }

        settingsGroups.add(messages["userProfile"])
        settingsGroups.add(messages["general"])

        root.selectionModel.selectedItems.onChange { change ->
            change.next()
            val newItem = change.list.firstOrNull()
            settingsMainArea.children.clear()
            when (newItem) {
                messages["userProfile"] -> settingsMainArea.children.add(userProfileSettings.root)
                messages["general"] -> settingsMainArea.children.add(generalSettingsPanel.root)
            }
        }

        root.selectionModel.select(0)
    }
}