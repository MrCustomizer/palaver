# Changelog
All notable changes to this project will be documented in this file.

The format is based on [keep a changelog](https://keepachangelog.com/en/1.0.0/) and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).


<!--- next entry here -->

## 0.2.1
2018-08-13

### Fixes

- ignore malformed events (82fd47c178a469bbc28365a0752e75c536bcbac0)

## 0.2.0
2018-08-12

### Features

- all usages of web views have been replaced by native JavaFX nodes ([#52](https://github.com/MrCustomizer/palaver/issues/52))
- show state events in backlog
- support for read markers

### Fixes

- don't execute topic change on ui thread
- calculate text of day rows correctly
- reduced log level for timeouts and other errors of other servers during user's metadata fetching from other servers ([#58](https://github.com/MrCustomizer/palaver/issues/58))
- fix for stack overflow when fetching backlog on interrupted connection ([#61](https://github.com/MrCustomizer/palaver/issues/61))

## 0.1.0

2018-06-07

### Added
- Automatically load backlog when scrolling up (asynchronously)
- Asynchronous loading of user names and avatars
- Show counter for unread notifications and highlights
- Tab completion of user names
- Display rooms with avatar
- Display users in a room with avatars
- Filter rooms in GUI
- Filter users in GUI
- Assign a number to a room with Ctrl-[0-9] and quick access that room with Alt-[0-9]
- Show images in backlog
- Input box resizes dynamically when entering large amounts of text
- Display room topic and update room topic, when it changes
- Display room name and update room name, when it changes
- Clickable links in topic view and chat backlog
- Experimental implementation of the .well-known-proposal
- Restore last window size and position on startup
