/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys.login

import io.kamax.matrix.MatrixID
import javafx.application.Platform
import org.apache.commons.validator.routines.UrlValidator
import tornadofx.Controller
import tornadofx.ValidationContext
import tornadofx.ValidationMessage
import tornadofx.get

class LoginValidators : Controller() {
    private val model: LoginCredentialsModel by inject()
    private val client: MatrixConnector by inject()

    val matrixID: ValidationContext.(String?) -> ValidationMessage? = {
        when {
            it == null || it.isEmpty() -> error(messages["mandatoryUser"])
            else -> {
                val idBuilder = try {
                    MatrixID.Builder(it)
                } catch (e: IllegalArgumentException) {
                    null
                }

                when (idBuilder) {
                    null -> error(messages["invalidMatrixId"])
                    else -> {
                        val validId = try {
                            idBuilder.valid()
                        } catch (e: IllegalArgumentException) {
                            null
                        }
                        val acceptableId = try {
                            idBuilder.acceptable()
                        } catch (e: IllegalArgumentException) {
                            null
                        }

                        when {
                            validId != null -> {
                                success()
                            }
                            acceptableId != null -> {
                                warning(messages["acceptableMatrixId"])
                            }
                            else -> error(messages["invalidMatrixId"])
                        }
                    }
                }
            }
        }
    }

    val homeserver: ValidationContext.(String?) -> ValidationMessage? = {
        when {
            it == null || it.isBlank() -> success(null)
            else -> {
                val validator = UrlValidator(arrayOf("https", "http"))
                when {
                    validator.isValid(it) -> when {
                        it.startsWith("http:") -> {
                            determineServerUrl(it)
                            warning(messages["unsafeHttp"])
                        }
                        else -> {
                            determineServerUrl(it)
                            success()
                        }
                    }
                    else -> error(messages["invalidHomeserver"])
                }
            }
        }
    }

    val password: ValidationContext.(String?) -> ValidationMessage? = {
        when {
            it.isNullOrEmpty() -> error(messages["mandatoryPassword"])
            else -> success()
        }
    }

    private var lastDomain = ""

    private fun determineServerUrl(url: String) {
        runAsync(daemon = true) {
            val domain = when {
                url.startsWith("https://") -> url.substring(8, url.length)
                url.startsWith("http://") -> url.substring(7, url.length)
                else -> url
            }

            if (lastDomain != domain) {
                lastDomain = domain
                val result = try {
                    val opt = client.discoverSettings(domain)
                    when {
                        opt.isPresent && opt.get().hsBaseUrls.isNotEmpty() -> opt.get().hsBaseUrls[0].toExternalForm()
                        else -> null
                    }
                } catch (e: Exception) {
                    null
                }

                if (result != null) {
                    Platform.runLater {
                        model.homeserver.value = result
                    }
                }
            }
        }
    }
}