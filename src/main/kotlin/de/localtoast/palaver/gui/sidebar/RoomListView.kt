/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.sidebar

import de.localtoast.palaver.gui.TextUtils
import de.localtoast.palaver.gui.stylesheet.SideBarStyle
import de.localtoast.palaver.sys.GraphicUtilities
import de.localtoast.palaver.sys.chatroom.ChatRoom
import de.localtoast.palaver.sys.chatroom.ChatRoomModel
import javafx.application.Platform
import javafx.beans.binding.Bindings
import javafx.geometry.Pos
import javafx.scene.control.Label
import javafx.scene.control.ListView
import javafx.scene.input.KeyCode
import tornadofx.add
import tornadofx.addClass
import tornadofx.cellFormat
import tornadofx.hbox
import tornadofx.imageview
import tornadofx.label
import tornadofx.onChange
import tornadofx.pane
import tornadofx.px
import tornadofx.rebindOnChange
import tornadofx.runAsync
import tornadofx.style
import tornadofx.vbox

class RoomListView(contactType: ChatRoomModel.RoomType, model: ChatRoomModel) : ListView<ChatRoom>() {

    init {
        items = when (contactType) {
            ChatRoomModel.RoomType.Person -> model.personsSortedAndFiltered
            ChatRoomModel.RoomType.Group -> model.groupsSortedAndFiltered
        }
        val listCellHeight = 34.0

        model.rebindOnChange(this) { selectedRoom ->
            if (selectedRoom != null) {
                item = selectedRoom
            }
        }

        addClass(SideBarStyle.view)
        cellFormat {
            graphic = hbox(4.0) {
                maxWidth = SideBarStyle.cellPaneWidth.value - 5 * 4.0
                pane {
                    addClass(SideBarStyle.selectionMarker)
                }

                hbox(4.0) {
                    alignment = Pos.CENTER_LEFT
                    prefHeight = listCellHeight

                    hbox {
                        alignment = Pos.CENTER
                        style {
                            minWidth = (GraphicUtilities.avatarSize).px
                        }
                        imageview {

                            it.scaledAndCutOutAvatar.onChange {
                                image = it
                            }
                            image = it.scaledAndCutOutAvatar.value
                            if (!it.avatarFetched) {
                                runAsync(daemon = true) {
                                    it.fetchMetadata()
                                }
                            }
                        }
                    }

                    val shortcutPane = vbox {
                        alignment = Pos.CENTER
                        val shortcut = it.shortcut.value
                        if (shortcut != null) {
                            add(createShortcutIcon(shortcut))
                        }
                    }
                    it.shortcut.onChange {
                        shortcutPane.children.clear()
                        if (it != null) {
                            val icon = createShortcutIcon(it)
                            shortcutPane.add(icon)
                        }
                    }

                    hbox {
                        alignment = Pos.CENTER_LEFT
                        label {
                            text = it.name.value
                            it.name.onChange {
                                Platform.runLater {
                                    if (it != null) {
                                        text = it
                                    }
                                }
                            }
                            addClass(SideBarStyle.cellText)
                        }
                    }

                    vbox {
                        alignment = Pos.CENTER

                        if (it.unreadCount.value != 0.toLong()) {
                            label {
                                alignment = Pos.CENTER
                                text = it.unreadCount.value.toString()
                                addClass(SideBarStyle.counterUnread)

                                minWidth = TextUtils.computeTextWidth(font, text)
                                textProperty().onChange {
                                    minWidth = TextUtils.computeTextWidth(font, text)
                                }
                            }
                        }

                        it.unreadCount.onChange {
                            Platform.runLater {
                                this@vbox.children.clear()
                                if (it != 0.toLong()) {
                                    this@vbox.add(Label(it.toString()).apply {
                                        alignment = Pos.CENTER
                                        addClass(SideBarStyle.counterUnread)
                                        minWidth = TextUtils.computeTextWidth(font, text)
                                        textProperty().onChange {
                                            minWidth = TextUtils.computeTextWidth(font, text)
                                        }
                                    })
                                }
                            }
                        }
                    }

                    vbox {
                        alignment = Pos.CENTER

                        if (it.highlightCount.value != 0.toLong()) {
                            label {
                                alignment = Pos.CENTER
                                text = it.highlightCount.value.toString()
                                addClass(SideBarStyle.counterHighlight)
                                minWidth = TextUtils.computeTextWidth(font, text)
                                textProperty().onChange {
                                    minWidth = TextUtils.computeTextWidth(font, text)
                                }
                            }
                        }

                        it.highlightCount.onChange {
                            Platform.runLater {
                                this@vbox.children.clear()
                                if (it != 0.toLong()) {
                                    this@vbox.add(Label(it.toString()).apply {
                                        alignment = Pos.CENTER
                                        addClass(SideBarStyle.counterHighlight)
                                        minWidth = TextUtils.computeTextWidth(font, text)
                                        textProperty().onChange {
                                            minWidth = TextUtils.computeTextWidth(font, text)
                                        }
                                    })
                                }
                            }
                        }
                    }
                }
            }
        }
        // Height of list has to reflect the size of the list elements and shouldn't be bigger
        prefHeightProperty().bind(Bindings.size(items).multiply(listCellHeight + 2))
        items.onChange {
            if (items.size > 0) {
                minHeight = Math.min(3, items.size) * listCellHeight + 2
            }
        }

        val size = if (items.size > 0) items.size else 1
        minHeight = Math.min(3, size) * listCellHeight + 2
    }

    private fun createShortcutIcon(shortcut: KeyCode): Label {
        return Label(shortcut.getName()).apply {
            addClass(SideBarStyle.shortcut)
        }
    }
}