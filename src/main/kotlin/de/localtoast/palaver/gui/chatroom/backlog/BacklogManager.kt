/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom.backlog

import de.localtoast.palaver.Settings
import de.localtoast.palaver.sys.events.Event
import de.localtoast.palaver.sys.events.EventRepository
import de.localtoast.palaver.sys.events.HistoryVisibilityEvent
import de.localtoast.palaver.sys.events.MembershipEvent
import de.localtoast.palaver.sys.events.MessageEvent
import de.localtoast.palaver.sys.events.NameEvent
import de.localtoast.palaver.sys.events.PowerLevelEvent
import de.localtoast.palaver.sys.events.TopicEvent
import io.kamax.matrix._MatrixID
import javafx.beans.property.SimpleBooleanProperty
import javafx.collections.FXCollections
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.time.temporal.ChronoUnit
import java.util.concurrent.Semaphore

class BacklogManager(private val eventRepo: EventRepository) {
    val rows = FXCollections.observableArrayList<BacklogRow>()!!

    private val eventIds = FXCollections.observableArrayList<String>()
    private val sortedEventIds = eventIds
        .sorted { id1, id2 ->
            eventRepo.getEvent(id1)?.originServerTimestamp
                ?.compareTo(eventRepo.getEvent(id2)?.originServerTimestamp) ?: 0
        }

    private val eventRows = mutableMapOf<String, EventRow>()

    private var todayRow: DayRow? = null

    fun addEvent(event: Event, todayString: String) {
        val eventId = event.id
        if (!eventRows.contains(eventId)) {

            eventIds.add(eventId)

            val previousEventRow = findPreviousEventRow(eventId)
            val nextEventRow = findNextEventRow(eventId)

            val newRow = when {
                event is MessageEvent ->
                    when {
                        previousEventRow != null &&
                            previousEventRow is MessageRow
                            && sameBlock(previousEventRow.event, event) -> {
                            MessageRow(event, SimpleBooleanProperty(false))
                        }

                        else -> MessageRow(event, SimpleBooleanProperty(true))
                    }

                event is TopicEvent -> TopicRow(event)
                event is NameEvent -> NameRow(event)
                event is MembershipEvent -> MembershipRow(event)
                event is PowerLevelEvent -> PowerLevelRow(event)
                event is HistoryVisibilityEvent -> HistoryVisibilityRow(event)

                else -> null
            }

            if (newRow != null) {
                eventRows[event.id] = newRow

                when {
                    previousEventRow != null -> rows.add(rows.indexOf(previousEventRow) + 1, newRow)
                    nextEventRow != null -> rows.add(rows.indexOf(nextEventRow), newRow)
                    else -> rows.add(newRow)
                }

                updateDayRows(newRow, nextEventRow, event, todayString, previousEventRow)

                if (newRow is MessageRow &&
                    nextEventRow != null &&
                    nextEventRow is MessageRow &&
                    sameBlock(nextEventRow.event, event)) {
                    nextEventRow.header.value = false
                }
            }
        }
    }

    private fun updateDayRows(newRow: EventRow, nextEventRow: EventRow?, event: Event, todayString: String, previousEventRow: EventRow?) {
        if (rows.indexOf(newRow) > 0) {
            val previousRow = rows[rows.indexOf(newRow) - 1]
            if (previousRow is DayRow) {
                rows.remove(previousRow)
            }
        }
        if (rows.indexOf(newRow) < rows.size - 1) {
            val nextRow = rows[rows.indexOf(newRow) + 1]
            if (nextRow is DayRow) {
                rows.remove(nextRow)
            }
        }

        addDayRowIfNecessary(nextEventRow, event, todayString, rows.indexOf(newRow) + 1, true)
        addDayRowIfNecessary(previousEventRow, event, todayString, rows.indexOf(newRow), false)
    }

    private fun addDayRowIfNecessary(
        otherRow: EventRow?,
        event: Event,
        todayString: String,
        rowIndex: Int,
        after: Boolean
    ) {
        if (otherRow != null) {
            val timestamp = if (after) {
                otherRow.event.originServerTimestamp
            } else {
                event.originServerTimestamp
            }
            val dayString = getDay(timestamp)

            if (!sameDay(event, otherRow.event)) {

                val relevantTimestamp = if (after) {
                    otherRow.event.originServerTimestamp
                } else {
                    event.originServerTimestamp
                }

                val dateIsToday = isToday(relevantTimestamp)
                val text = if (dateIsToday) todayString else dayString
                val dayRow = DayRow(text, relevantTimestamp)
                if (dateIsToday) {
                    updateTodayStringIfNecessary()
                    updateTodayStringSemaphore.acquire()
                    todayRow = dayRow
                }

                rows.add(rowIndex, dayRow)
                if (dateIsToday) {
                    updateTodayStringSemaphore.release()
                }
            }
        }
    }

    fun getLastConsecutiveEventOfUser(eventId: String, user: _MatrixID?): String? {
        val startIndex = sortedEventIds.indexOf(eventId)
        if (startIndex >= 0) {
            for (i in startIndex + 1 until sortedEventIds.size) {
                val event = eventRepo.getEvent(sortedEventIds[i])
                if (event != null && event.sender != user) {
                    return sortedEventIds[i - 1]
                }
            }

            // All of the following events are from the current user
            return sortedEventIds.lastOrNull()
        }
        return null
    }

    fun isLastKnownEvent(eventId: String): Boolean {
        return sortedEventIds.lastOrNull() == eventId
    }

    fun getRowIndex(eventId: String): Int {
        return rows.indexOf(eventRows[eventId])
    }

    private fun isToday(eventTime: Instant): Boolean {
        val today = Instant.now().atZone(ZoneId.systemDefault())
        return sameDay(today, eventTime.atZone(ZoneId.systemDefault()))
    }

    private val updateTodayStringSemaphore = Semaphore(1)

    fun updateTodayStringIfNecessary() {
        updateTodayStringSemaphore.acquire()
        val today = todayRow
        if (today != null && !isToday(today.timestamp)) {
            val index = rows.indexOf(today)
            val dayString = getDay(today.timestamp)
            val newDayRow = DayRow(dayString, today.timestamp)
            rows.remove(today)
            rows.add(index, newDayRow)
            todayRow = newDayRow
        }
        updateTodayStringSemaphore.release()
    }

    private fun findPreviousEventRow(eventId: String): EventRow? {
        val index = sortedEventIds.indexOf(eventId)

        for (i in index - 1 downTo 0) {
            val prevEventId = sortedEventIds[i]
            if (eventRows.containsKey(prevEventId)) {
                return eventRows[prevEventId]
            }
        }

        return null
    }

    private fun findNextEventRow(eventId: String): EventRow? {
        val index = sortedEventIds.indexOf(eventId)

        for (i in index + 1 until sortedEventIds.size) {
            val nextEventId = sortedEventIds[i]
            if (eventRows.containsKey(nextEventId)) {
                return eventRows[nextEventId]
            }
        }

        return null
    }

    private fun sameBlock(event: Event, otherEvent: Event) =
        event.sender == otherEvent.sender && sameDay(event, otherEvent)

    private fun sameDay(event: Event, otherEvent: Event): Boolean {
        val eventZonedTime = event.originServerTimestamp.atZone(ZoneId.systemDefault())
        val otherEventZonedTime = otherEvent.originServerTimestamp.atZone(ZoneId.systemDefault())

        return sameDay(eventZonedTime, otherEventZonedTime)
    }

    private fun sameDay(eventZonedTime: ZonedDateTime, otherEventZonedTime: ZonedDateTime): Boolean {
        val eventDay = eventZonedTime.truncatedTo(ChronoUnit.DAYS)
        val otherEventDay = otherEventZonedTime.truncatedTo(ChronoUnit.DAYS)

        return otherEventDay == eventDay
    }

    private fun getDay(timestamp: Instant): String {
        val formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)
            .withLocale(Settings.localeFormat)
            .withZone(ZoneId.systemDefault())
        return formatter.format(timestamp)
    }
}