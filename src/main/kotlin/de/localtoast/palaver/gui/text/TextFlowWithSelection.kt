/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.text

import com.sun.javafx.scene.text.TextLayout
import de.localtoast.palaver.gui.stylesheet.PalaverStyle
import javafx.beans.property.SimpleObjectProperty
import javafx.scene.Node
import javafx.scene.shape.LineTo
import javafx.scene.shape.MoveTo
import javafx.scene.shape.PathElement
import javafx.scene.shape.Rectangle
import javafx.scene.shape.Shape
import javafx.scene.text.Text
import javafx.scene.text.TextFlow
import tornadofx.onChange
import java.lang.reflect.Method

class TextFlowWithSelection : TextFlow() {
    private val selectionProperty = SimpleObjectProperty(IntRange.EMPTY)
    private val contentChildren = mutableListOf<Node>()

    var selection: IntRange
        set(value) {
            selectionProperty.value = value
        }
        get() {
            return selectionProperty.value
        }

    fun characterHit(screenX: Double, screenY: Double): Int {
        val point = screenToLocal(screenX - insets.left, screenY)
        val hitInfo = getTextLayout().getHitInfo(point.x.toFloat(), point.y.toFloat())
        return hitInfo.insertionIndex
    }

    fun charLength(): Int {
        return children.sumBy {
            when (it) {
                is Text -> it.text.length
                else -> 0
            }
        }
    }

    fun text(start: Int?, end: Int?): String {
        val text = contentChildren.joinToString(separator = "", transform = {
            when (it) {
                is Text -> it.text
                else -> ""
            }
        })

        val endChar = if (end != null ) Math.min(end, text.length) else text.length
        val startChar = start ?: 0

        return text.substring(startChar, endChar)
    }

    /*
     * As long as JavaFX does not expose this method as at least protected, we have to get the layout by reflection…
     * RichTextFX currently, meaning in July 2018, does it in the same ugly way.
     */
    private val getTextLayoutMethod: Method = TextFlow::class.java.getDeclaredMethod("getTextLayout").apply {
        isAccessible = true
    } ?: throw Exception("Method 'getTextLayoutMethod' in class 'TextFlow' not found!")

    private val backgroundShapes = mutableListOf<Shape>()

    init {
        selectionProperty.onChange {
            layoutChildren()
        }
    }

    override fun layoutChildren() {
        super.layoutChildren()
        updateSelectionShapes()
    }

    private fun getTextLayout(): TextLayout {
        return getTextLayoutMethod.invoke(this) as TextLayout
    }

    private fun updateSelectionShapes() {
        backgroundShapes.forEach { children.remove(it) }
        backgroundShapes.clear()

        contentChildren.clear()
        contentChildren.addAll(children)

        if (!selectionProperty.value.isEmpty()) {
            val layout = getTextLayout()

            val ranges = divideRangeByLines(layout, selectionProperty.value)

            ranges.forEach {

                // TODO in Java 9 it will be possible to directly call the rangeShape-method of TextFlow
                val rangePath = layout.getRange(it.start, it.last,
                    TextLayout.TYPE_TEXT, 0f, 0f)
                /*
                 * getRange delivers a path, containing of four lines, forming a rectangle. We are converting this path to
                 * a filled rectangle shape.
                 */
                val shape = createRectangleFromRangePath(rangePath)

                if (shape != null) {
                    backgroundShapes.add(shape)
                    children.add(0, shape)
                }
            }
        }
    }

    /**
     * Create one range per line
     */
    private fun divideRangeByLines(layout: TextLayout, range: IntRange): List<IntRange> {
        return layout.lines
            .mapNotNull {
                if (it != null) {
                    val selStart = Math.max(it.start, range.first)
                    val selEnd = Math.min(it.start + it.length, range.last)
                    IntRange(selStart, selEnd)
                } else null
            }
    }

    private fun createRectangleFromRangePath(rangePath: Array<PathElement>): Rectangle? {
        if (rangePath.size == 5) {
            val topLeft = rangePath[0] as MoveTo
            val bottomRight = rangePath[2] as LineTo

            var xMin = insets.left + boundsInParent.minX + topLeft.x
            var yMin = topLeft.y
            var xMax = insets.left + boundsInParent.minX + bottomRight.x
            var yMax = bottomRight.y

            return Rectangle(xMin, yMin, xMax - xMin, yMax - yMin).apply {
                isManaged = false
                fill = PalaverStyle.selectionColor
                strokeWidth = 0.0
            }
        }

        return null
    }
}