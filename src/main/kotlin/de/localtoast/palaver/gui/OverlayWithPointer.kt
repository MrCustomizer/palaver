/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui

import de.localtoast.palaver.gui.stylesheet.PalaverStyle
import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.effect.DropShadow
import javafx.scene.input.KeyCode
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import javafx.scene.layout.VBox
import javafx.scene.shape.Polyline
import javafx.scene.shape.StrokeLineCap
import javafx.scene.shape.StrokeLineJoin
import javafx.scene.shape.StrokeType
import tornadofx.Fragment
import tornadofx.box
import tornadofx.onChange
import tornadofx.px
import tornadofx.style
import tornadofx.vboxConstraints
import kotlin.math.roundToInt

class OverlayWithPointer : Fragment() {
    val pointerTarget: SimpleObjectProperty<Region?> by param()
    val prefWidth: Double by param()
    val prefHeight: Double by param()
    val content: Node by param()

    override val root = VBox().apply {
        style {
            effect = DropShadow().apply {
                radius = 5.0
                color = PalaverStyle.borderColor
            }
        }
    }

    private val mainWindow: MainWindow by inject()
    private val distanceToTarget = 3.0

    private val pointerHeight = 8.0

    private val pointerTopBox = HBox().apply {
        maxHeight = pointerHeight
        minHeight = pointerHeight
        children.add(HBox())
    }
    private val pointerBottomBox = HBox().apply {
        maxHeight = pointerHeight
        minHeight = pointerHeight
        children.add(HBox())
    }

    private val contentBox = VBox().apply {
        vboxConstraints { vGrow = Priority.ALWAYS }
        style {
            backgroundColor += PalaverStyle.defaultBackground
        }
        content.apply {
            vboxConstraints { vGrow = Priority.ALWAYS }

            style {
                padding = box(10.px)
            }
        }
        add(content)
    }

    init {
        // First, remove other readMarkerOverlay windows, if present
        dispose()

        // Then add the new one
        mainWindow.overlayPane.children.add(root)
        root.prefWidth = if (prefWidth > 25) prefWidth else 25.0
        root.prefHeight = prefHeight
        root.alignment = Pos.CENTER

        root.add(pointerTopBox)
        root.add(contentBox)
        root.add(pointerBottomBox)

        pointerTarget.value?.needsLayoutProperty()?.onChange { updatePosition() }
        pointerTarget.value?.localToSceneTransformProperty()?.onChange { updatePosition() }

        pointerTarget.onChange {
            it?.needsLayoutProperty()?.onChange { updatePosition() }
            it?.localToSceneTransformProperty()?.onChange { updatePosition() }
        }

        root.needsLayoutProperty().onChange { updatePosition() }
        root.localToSceneTransformProperty().onChange { updatePosition() }

        updatePosition()

        root.setOnKeyPressed {
            if (it.code == KeyCode.ESCAPE) {
                dispose()
            }
        }
    }

    fun dispose() {
        mainWindow.overlayPane.children.clear()
    }

    private fun updatePosition() {
        val pointerTargetValue = pointerTarget.value
        if (pointerTargetValue != null && pointerTargetValue.parent != null) {

            val layoutBounds = pointerTargetValue.localToScene(pointerTargetValue.layoutBounds)
            /*
             * There are situation in the flow layout, where the node isn't visible. The layoutBounds have a width
             * and height of 0 in this case and/or a height of zero. We don't want to update the layout in this case.
             */
            if (layoutBounds != null && layoutBounds.width != 0.0 && layoutBounds.height != 0.0) {

                if (layoutBounds.minX != 0.0 && layoutBounds.minY != 0.0) {
                    val targetX = (layoutBounds.minX + layoutBounds.maxX) / 2

                    // We are rounding to Int as real Double values can create undesired visual effects
                    val newX = (targetX - root.width * 0.2).roundToInt()

                    root.layoutX = when {
                        newX >= 0 && newX + root.width > mainWindow.root.scene.width &&
                            root.width < mainWindow.root.scene.width -> mainWindow.root.scene.width - root.width
                        newX >= 0 -> newX.toDouble()
                        else -> 0.0
                    }

                    val y = (layoutBounds.minY + layoutBounds.maxY) / 2

                    val localTargetBounds = root.sceneToLocal(layoutBounds)
                    val localTargetX = ((localTargetBounds.minX + localTargetBounds.maxX) / 2).roundToInt().toDouble()

                    when {
                    // pointer on bottom
                        y > mainWindow.root.scene.height / 2 -> {
                            contentBox.style {
                                borderWidth += box(1.px, 1.px, 0.px, 1.px)
                                borderColor += box(PalaverStyle.borderColor)
                                backgroundColor += PalaverStyle.defaultBackground
                            }
                            root.layoutY = (layoutBounds.minY - root.height - distanceToTarget).roundToInt().toDouble()
                            root.maxHeight = layoutBounds.minY - distanceToTarget

                            pointerTopBox.children[0] = HBox()
                            pointerTopBox.minHeight = 0.0

                            pointerBottomBox.minHeight = pointerHeight
                            pointerBottomBox.children[0] = createBorderWithPointer(arrayOf(0.0, 0.0,
                                localTargetX - 10, 0.0,
                                localTargetX, pointerHeight,
                                localTargetX + 10, 0.0,
                                root.width - 2.0, 0.0))
                        }
                    // pointer on top
                        else -> {
                            contentBox.style {
                                borderWidth += box(0.px, 1.px, 1.px, 1.px)
                                borderColor += box(PalaverStyle.borderColor)
                                backgroundColor += PalaverStyle.defaultBackground
                            }
                            root.layoutY = (layoutBounds.maxY + distanceToTarget).roundToInt().toDouble()
                            root.maxHeight = mainWindow.root.scene.height - root.layoutY - distanceToTarget

                            pointerTopBox.minHeight = pointerHeight
                            pointerTopBox.children[0] = createBorderWithPointer(arrayOf(0.0, pointerHeight,
                                localTargetX - 10, pointerHeight,
                                localTargetX, 0.0,
                                localTargetX + 10, pointerHeight,
                                root.width - 2.0, pointerHeight))
                            pointerBottomBox.children[0] = HBox()
                            pointerBottomBox.minHeight = 0.0
                        }
                    }
                }
            }
        }
    }

    private fun createBorderWithPointer(coordinates: Array<Double>): Polyline {
        return Polyline().apply {
            points.addAll(coordinates)

            fill = PalaverStyle.defaultBackground
            strokeWidth = 1.0
            strokeType = StrokeType.CENTERED
            strokeLineJoin = StrokeLineJoin.ROUND
            strokeLineCap = StrokeLineCap.ROUND
            stroke = PalaverStyle.borderColor
        }
    }
}