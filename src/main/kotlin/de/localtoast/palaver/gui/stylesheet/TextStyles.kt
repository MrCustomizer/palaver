/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.stylesheet

import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

class TextStyles : Stylesheet() {
    companion object {
        val message by cssclass()
        val code by cssclass()
        val textFlowCitation by cssclass()
        val messageCitation by cssclass()
        val messageItalic by cssclass()
        val codeItalic by cssclass()
        val messageBold by cssclass()
        val codeBold by cssclass()
        val messageBoldItalic by cssclass()
        val codeBoldItalic by cssclass()
        val messageH1 by cssclass()
        val messageH2 by cssclass()
        val messageH3 by cssclass()
        val messageH4 by cssclass()
        val messageH5 by cssclass()
        val messageH6 by cssclass()
        val messageStrikethrough by cssclass()
        val messageUnderline by cssclass()
    }

    init {
        message {
            +FontStyle.regularMixin
        }

        code {
            +FontStyle.regularMonoMixin
        }

        textFlowCitation {
            padding = box(0.px, 0.px, 0.px, 10.px)
            borderWidth += box(0.px, 0.px, 0.px, 4.px)
            borderColor += box(PalaverStyle.lightBlue)
        }

        messageCitation {
            fill = Color.GRAY
        }

        messageItalic {
            +FontStyle.regularItalicMixin
        }

        codeItalic {
            +FontStyle.regularMonoItalicMixin
        }

        messageBold {
            +FontStyle.boldMixin
        }

        codeBold {
            +FontStyle.boldMonoMixin
        }

        messageBoldItalic {
            +FontStyle.boldItalicMixin
        }

        codeBoldItalic {
            +FontStyle.boldMonoItalicMixin
        }

        val headerFactor = 2

        messageH1 {
            +FontStyle.boldMixin
            fontSize = FontStyle.defaultFontSize + 6 * headerFactor
        }

        messageH2 {
            +FontStyle.boldMixin
            fontSize = FontStyle.defaultFontSize + 5 * headerFactor
        }

        messageH3 {
            +FontStyle.boldMixin
            fontSize = FontStyle.defaultFontSize + 4 * headerFactor
        }

        messageH4 {
            +FontStyle.boldMixin
            fontSize = FontStyle.defaultFontSize + 3 * headerFactor
        }

        messageH5 {
            +FontStyle.boldMixin
            fontSize = FontStyle.defaultFontSize + 2 * headerFactor
        }

        messageH6 {
            +FontStyle.boldMixin
            fontSize = FontStyle.defaultFontSize + headerFactor
        }

        messageStrikethrough {
            strikethrough = true
        }

        messageUnderline {
            underline = true
        }
    }
}