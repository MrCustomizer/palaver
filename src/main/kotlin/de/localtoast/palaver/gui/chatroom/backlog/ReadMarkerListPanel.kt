/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom.backlog

import de.localtoast.palaver.gui.stylesheet.FontStyle
import de.localtoast.palaver.gui.stylesheet.PalaverStyle
import de.localtoast.palaver.gui.stylesheet.ReadMarkerListPanelStyle
import de.localtoast.palaver.sys.DateTimeHelper
import de.localtoast.palaver.sys.GraphicUtilities
import de.localtoast.palaver.sys.StringMatcher
import de.localtoast.palaver.sys.users.User
import javafx.application.Platform
import javafx.collections.transformation.FilteredList
import javafx.collections.transformation.SortedList
import javafx.geometry.Pos
import javafx.scene.image.Image
import javafx.scene.layout.HBox
import javafx.scene.layout.Priority
import org.fxmisc.flowless.Cell
import org.fxmisc.flowless.VirtualFlow
import org.fxmisc.flowless.VirtualizedScrollPane
import tornadofx.Fragment
import tornadofx.addClass
import tornadofx.box
import tornadofx.get
import tornadofx.hbox
import tornadofx.hboxConstraints
import tornadofx.imageview
import tornadofx.label
import tornadofx.observable
import tornadofx.onChange
import tornadofx.px
import tornadofx.style
import tornadofx.textfield
import tornadofx.vbox
import tornadofx.vboxConstraints
import java.time.Instant
import java.util.function.Predicate

class ReadMarkerListPanel : Fragment() {
    companion object {
        const val cellHeight = GraphicUtilities.avatarSize.toDouble() + 4
    }

    val users: List<Pair<User, Instant>> by param()

    private val filteredUsers = FilteredList(users.observable())
    private val sortedAndFilteredUsers = SortedList(filteredUsers).apply {
        setComparator { item1, item2 ->
            item1.first.name.value.compareTo(item2.first.name.value, ignoreCase = true)
        }
    }

    override val root = vbox {
        label(messages["seenBy"]) { addClass(FontStyle.title) }

        hbox {
            hboxConstraints {
                hGrow = Priority.ALWAYS
            }
            style {
                padding = box(10.px, 0.px, 0.px, 0.px)
            }
            hbox {
                val filterBoxWidth = 200.0
                prefWidth = filterBoxWidth
                maxWidth = filterBoxWidth

                addClass(PalaverStyle.filterBoxLight)
                hbox {
                    hboxConstraints {
                        hGrow = Priority.ALWAYS
                    }
                    addClass(PalaverStyle.filterBoxPaddingLight)

                    alignment = Pos.CENTER_LEFT

                    imageview(Image("/de/localtoast/palaver/icons/actions/material.io/outline_search_black_20px.png")) {
                        opacity = 0.4
                    }

                    textfield {
                        hboxConstraints {
                            hGrow = Priority.ALWAYS
                        }
                        addClass(PalaverStyle.filterBoxInputFieldLight)
                        promptText = messages["filter"]
                        textProperty().onChange {
                            filteredUsers.predicate = Predicate {
                                StringMatcher.getMatchPositions(it.first.name.value, text).size == text.length
                            }
                        }
                        Platform.runLater {
                            this.requestFocus()
                        }
                    }
                }
            }
        }

        val list = VirtualFlow.createVertical(sortedAndFilteredUsers) { user -> renderCell(user.first, user.second) }

        hbox {
            vboxConstraints {
                vGrow = Priority.ALWAYS
            }
            addClass(ReadMarkerListPanelStyle.listBorder)

            add(VirtualizedScrollPane(list).apply {
                hboxConstraints {
                    hGrow = Priority.ALWAYS
                }
                addClass(ReadMarkerListPanelStyle.listScrollBar)
            })
        }
    }

    private fun renderCell(user: User, timestamp: Instant): Cell<Pair<User, Instant>, HBox> {
        val hBox = HBox(4.0).apply {

            minHeight = cellHeight
            maxHeight = cellHeight
            prefHeight = cellHeight
            alignment = Pos.CENTER_LEFT

            addClass(ReadMarkerListPanelStyle.listCell)

            hbox {
                val width = GraphicUtilities.avatarSize.toDouble() + 4
                minWidth = width
                maxWidth = width
                prefWidth = width
                alignment = Pos.CENTER

                imageview {
                    image = user.scaledAndCutOutAvatar.value

                    user.scaledAndCutOutAvatar.onChange {
                        Platform.runLater {
                            image = it
                        }
                    }
                }
            }

            label {
                addClass(FontStyle.regular)
                text = user.name.value
                maxWidth = 150.0

                user.name.onChange {
                    Platform.runLater {
                        text = it
                    }
                }
            }

            hbox {
                hboxConstraints {
                    hGrow = Priority.ALWAYS
                }
                alignment = Pos.CENTER_RIGHT
                label(DateTimeHelper.formatTimestampDateTime(timestamp)) {
                    addClass(ReadMarkerListPanelStyle.timestampFont)
                }
            }
        }

        return Cell.wrapNode<Pair<User, Instant>, HBox>(hBox)
    }
}