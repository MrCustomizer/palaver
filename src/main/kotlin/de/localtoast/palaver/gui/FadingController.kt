/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui

import javafx.animation.Animation
import javafx.animation.Timeline
import javafx.scene.Node
import javafx.util.Duration
import tornadofx.keyframe
import tornadofx.timeline

class FadingController(private val node: Node) {
    private var fadeOutTimeline: Timeline? = null
    private var fadeInTimeline: Timeline? = null

    fun fadeIn(duration: Duration) {
        if (fadeInTimeline?.status != Animation.Status.RUNNING) {
            fadeOutTimeline?.stop()
            fadeInTimeline?.stop()
            fadeInTimeline = timeline {
                keyframe(duration) {
                    keyvalue(node.opacityProperty(), 1.0)
                }
            }
        }
    }

    fun fadeOut(duration: Duration) {
        if (fadeOutTimeline?.status != Animation.Status.RUNNING) {
            fadeInTimeline?.stop()
            fadeOutTimeline?.stop()
            fadeOutTimeline = timeline {
                keyframe(duration) {
                    keyvalue(node.opacityProperty(), 0.0)
                }
            }
        }
    }
}