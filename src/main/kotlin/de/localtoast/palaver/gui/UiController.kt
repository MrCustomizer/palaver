/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui

import de.localtoast.palaver.gui.stylesheet.FontStyle
import javafx.geometry.Pos
import javafx.scene.layout.StackPane
import javafx.scene.paint.Color
import javafx.util.Duration
import tornadofx.Controller
import tornadofx.View
import tornadofx.add
import tornadofx.addClass
import tornadofx.fade
import tornadofx.label
import tornadofx.pane
import tornadofx.progressindicator
import tornadofx.style
import tornadofx.vbox

class UiController : Controller() {
    companion object {
        val animationDurationIn = Duration.millis(300.0)!!
        val animationDurationOut = Duration.millis(100.0)!!
    }

    private val mainWindow: MainWindow by inject()

    fun hideDialog(dialog: View) {
        dialog.fade(UiController.animationDurationOut, 0, op = {
            setOnFinished { mainWindow.mainView.children.remove(dialog.root) }
        })
    }

    fun createProgressIndicator(message: String): StackPane {
        return StackPane().apply {
            pane {
                style {
                    backgroundColor += Color.WHITE
                }
            }
            vbox(15.0) {
                alignment = Pos.CENTER
                progressindicator {
                    progress = -1.0
                }
                label {
                    addClass(FontStyle.progressIndicatorFont)
                    text = message
                }
            }
        }
    }

    fun showProgressCircle(stackPane: StackPane, progressIndicator: StackPane) {
        progressIndicator.opacity = 0.0
        stackPane.add(progressIndicator)
        progressIndicator.fade(UiController.animationDurationIn, 1)
    }

    fun hideProgressCircle(stackPane: StackPane, progressIndicator: StackPane, action: (() -> Unit)? = null) {
        progressIndicator.fade(UiController.animationDurationOut, 0, op = {
            setOnFinished {
                stackPane.children.remove(progressIndicator)
                if (action != null) {
                    action()
                }
            }
        })
    }
}