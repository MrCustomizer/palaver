/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.sidebar

import de.localtoast.palaver.Settings
import de.localtoast.palaver.gui.stylesheet.SideBarStyle
import de.localtoast.palaver.sys.chatroom.ChatRoomModel
import javafx.geometry.Pos
import javafx.scene.image.Image
import javafx.scene.layout.Priority
import tornadofx.View
import tornadofx.addClass
import tornadofx.get
import tornadofx.hbox
import tornadofx.hboxConstraints
import tornadofx.imageview
import tornadofx.label
import tornadofx.onChange
import tornadofx.textfield
import tornadofx.vbox
import tornadofx.vboxConstraints

class ContactPane : View() {
    private val model: ChatRoomModel by inject()

    private val groupListView = RoomListView(ChatRoomModel.RoomType.Group, model)
    private val personListView = RoomListView(ChatRoomModel.RoomType.Person, model)

    private var initialSelect = false

    override val root = vbox {
        addClass(SideBarStyle.paneBackground)
        alignment = Pos.TOP_LEFT

        vboxConstraints {
            vGrow = Priority.ALWAYS
        }

        hbox {
            addClass(SideBarStyle.filterBox)
            hbox {
                hboxConstraints {
                    hGrow = Priority.ALWAYS
                }
                addClass(SideBarStyle.filterBoxPadding)

                alignment = Pos.CENTER_LEFT

                imageview(Image("/de/localtoast/palaver/icons/actions/material.io/outline_search_white_20px.png")) {
                    opacity = 0.7
                }

                textfield {
                    hboxConstraints {
                        hGrow = Priority.ALWAYS
                    }
                    addClass(SideBarStyle.filterBoxInputField)
                    promptText = messages["filter"]
                    textProperty().onChange { model.filter.value = it }
                }
            }
        }

        label(messages["people"]) {
            prefHeight = 23.0
            addClass(SideBarStyle.contactCategoryTitleFont)
        }

        add(personListView)

        label(messages["groups"]) {
            prefHeight = 23.0
            addClass(SideBarStyle.contactCategoryTitleFont)
        }

        add(groupListView)

        ensureOnlyOneItemIsSelected(personListView, groupListView)
    }

    fun selectInitialItem() {
        if (!initialSelect) {
            val lastRoomId = Settings.lastRoom
            when {
                lastRoomId.isNotBlank() -> selectRoom(lastRoomId)
                personListView.items.isNotEmpty() -> {
                    personListView.selectionModel.selectFirst()
                    initialSelect = true
                }
                groupListView.items.isNotEmpty() -> {
                    groupListView.selectionModel.selectFirst()
                    initialSelect = true
                }
            }
        }
    }

    fun selectRoom(roomId: String) {
        if (roomId.isNotBlank()) {
            personListView.items.filter { it.id == roomId }.forEach {
                personListView.selectionModel.select(it)
                initialSelect = true
            }
            groupListView.items.filter { it.id == roomId }.forEach {
                groupListView.selectionModel.select(it)
                initialSelect = true
            }
        }
    }

    private fun ensureOnlyOneItemIsSelected(personListView: RoomListView, groupListView: RoomListView) {
        personListView.selectionModel.selectedItemProperty().onChange {
            if (!personListView.selectionModel.isEmpty) {
                groupListView.selectionModel.clearSelection()
            }
        }
        groupListView.selectionModel.selectedItemProperty().onChange {
            if (!groupListView.selectionModel.isEmpty) {
                personListView.selectionModel.clearSelection()
            }
        }
    }
}