/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys

object StringMatcher {

    fun getMatchPositions(text: String?, matchString: String?): List<Int> {
        val positions = mutableListOf<Int>()
        if (text != null && matchString != null) {
            if (matchString.isNotEmpty() && text.isNotEmpty()) {
                var offset = 0
                (0 until matchString.length).forEach { i ->
                    if (offset < text.length) {
                        val matchPosWithOffset = text.substring(offset).indexOf(matchString[i], ignoreCase = true)
                        if (matchPosWithOffset >= 0) {
                            val matchPos = matchPosWithOffset + offset
                            positions.add(matchPos)
                            offset = matchPos + 1
                        }
                    }
                }
            }
        }
        return positions
    }
}