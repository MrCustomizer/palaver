/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui

import javafx.scene.text.Font
import javafx.scene.text.Text

// Based on https://stackoverflow.com/a/18608568/261769
object TextUtils {
    private val helper = Text()

    fun computeTextWidth(font: Font, text: String): Double {
        val helper = Text()
        helper.text = text
        helper.font = font

        helper.wrappingWidth = 0.0
        helper.lineSpacing = 0.0

        return Math.ceil(helper.layoutBounds.width) + 10
    }
}