/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys

import de.localtoast.palaver.Settings
import de.localtoast.palaver.gui.sidebar.ContactPane
import de.localtoast.palaver.sys.chatroom.ChatRoomModel
import de.localtoast.palaver.sys.chatroom.RoomRepository
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import javafx.scene.input.KeyEvent
import tornadofx.Controller

class HotkeyManager : Controller() {
    companion object {
        val allowedRoomShortcuts = listOf(KeyCode.DIGIT0,
                KeyCode.DIGIT1,
                KeyCode.DIGIT2,
                KeyCode.DIGIT3,
                KeyCode.DIGIT4,
                KeyCode.DIGIT5,
                KeyCode.DIGIT6,
                KeyCode.DIGIT7,
                KeyCode.DIGIT8,
                KeyCode.DIGIT9,
                KeyCode.DIGIT0)
    }

    private val roomModel: ChatRoomModel by inject()

    private val contactPane: ContactPane by inject()

    fun handle(event: KeyEvent) {

        val ctrlMatch = allowedRoomShortcuts.firstOrNull {
            KeyCodeCombination(it, KeyCombination.CONTROL_DOWN).match(event)
        }

        val altMatch = allowedRoomShortcuts.firstOrNull {
            KeyCodeCombination(it, KeyCombination.ALT_DOWN).match(event)
        }

        when {
            ctrlMatch != null -> {
                val currentRoomId = roomModel.item?.id
                if (currentRoomId != null) {
                    val oldRoomId = Settings.getRoomFromShortcut(ctrlMatch)

                    if (oldRoomId.isNotBlank()) {
                        val oldRoom = RoomRepository.rooms.getValue(oldRoomId)
                        val oldShortcut = oldRoom.shortcut.value
                        if (oldShortcut != null) {
                            oldRoom.shortcut.value = null
                            Settings.setShortcutForRoom(oldShortcut, "")
                        }
                    }

                    if (oldRoomId != currentRoomId) {
                        val currentRoom = RoomRepository.rooms.getValue(currentRoomId)
                        val oldShortcut = currentRoom.shortcut.value
                        currentRoom.shortcut.value = ctrlMatch
                        Settings.setShortcutForRoom(ctrlMatch, currentRoomId)
                        if (oldShortcut != null) {
                            Settings.setShortcutForRoom(oldShortcut, "")
                        }
                    }
                }
            }
            altMatch != null -> contactPane.selectRoom(Settings.getRoomFromShortcut(altMatch))
        }
    }
}