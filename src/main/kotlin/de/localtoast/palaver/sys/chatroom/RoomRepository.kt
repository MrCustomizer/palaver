/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys.chatroom

import de.localtoast.palaver.sys.PalaverHttpClient
import de.localtoast.palaver.sys.events.EventRepository
import de.localtoast.palaver.sys.users.User
import de.localtoast.palaver.sys.users.UserRepository
import io.kamax.matrix._MatrixID
import tornadofx.Controller
import tornadofx.onChangeOnce
import kotlin.collections.set

class RoomRepository : Controller() {
    companion object {
        // TODO do something about the fact, that after accessing the map you always have to deal with nullable values
        val rooms = hashMapOf<String, ChatRoom>()
    }

    private val userRepo: UserRepository by inject()
    private val client: PalaverHttpClient by inject()
    private val eventRepo: EventRepository by inject()

    fun createRoom(
        id: String,
        topic: ChatRoomTopic?,
        name: ChatRoomName?,
        userIds: Set<_MatrixID>,
        roomAliases: Set<String>
    ): ChatRoom {
        if (rooms.containsKey(id)) {
            throw IllegalArgumentException("Room already created!")
        }

        val mutableUserIdSet = mutableSetOf<_MatrixID>()
        mutableUserIdSet.addAll(userIds)

        val room = ChatRoom(id, name, topic, mutableUserIdSet, userRepo, eventRepo, client, roomAliases)
        rooms[id] = room

        room.fetchUserMetaData.onChangeOnce {
            runAsync(daemon = true) {
                val newUsers = mutableListOf<User>()
                while (true) {
                    val newUser = room.userMetadataFetchQueue.take()
                    if (newUser != null) {
                        newUsers.add(newUser)
                    }
                    if (room.userMetadataFetchQueue.isEmpty()) {
                        val copy = ArrayList<User>(newUsers)
                        copy.forEach {
                            runAsync(daemon = true) {
                                it.fetchMetadata()
                            }
                        }
                        newUsers.clear()
                    }
                }
            }
        }

        return room
    }
}