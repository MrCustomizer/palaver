/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys

import de.localtoast.palaver.sys.chatroom.ChatRoom
import de.localtoast.palaver.sys.chatroom.ChatRoomModel
import de.localtoast.palaver.sys.chatroom.RoomRepository
import de.localtoast.palaver.sys.users.User
import de.localtoast.palaver.sys.users.UserRepository
import io.kamax.matrix._MatrixID
import io.kamax.matrix.client.MatrixClientContext
import io.kamax.matrix.client.MatrixClientRequestException
import io.kamax.matrix.client.MatrixHttpRoom
import io.kamax.matrix.client._SyncData
import io.kamax.matrix.client._SyncOptions
import io.kamax.matrix.client.regular.MatrixHttpClient
import io.kamax.matrix.json.InvalidJsonException
import io.kamax.matrix.json.MatrixJsonEventFactory
import io.kamax.matrix.json.event.MatrixJsonRoomEvent
import io.kamax.matrix.room.MatrixRoomMessageChunkOptions
import io.kamax.matrix.room._MatrixRoomMessageChunkOptions
import org.slf4j.LoggerFactory
import tornadofx.Controller
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.net.URI
import javax.imageio.ImageIO

class PalaverHttpClient : Controller() {
    private val logger = LoggerFactory.getLogger(this.javaClass.name)

    val context: MatrixClientContext
        get() = httpClient.context

    val user: _MatrixID
        get() = httpClient.user
            .orElseThrow { throw IllegalStateException("Client user is not allowed to be null, here") }

    private var httpClientIntern: MatrixHttpClient? = null

    private val httpClient: MatrixHttpClient
        get() {
            val client = httpClientIntern
            return if (client != null) client else {
                logger.error("Http client accessed, but no connection has been initialized.")
                throw Exception("Http client is not initialized.")
            }
        }

    private val userRepo: UserRepository by inject()

    private val roomModel: ChatRoomModel by inject()

    fun setHttpClient(client: MatrixHttpClient?) {
        httpClientIntern = client
    }

    fun sendMessageToCurrentRoom(message: String) {
        if (message.isNotEmpty()) {
            val roomId = roomModel.item?.id
            runAsync(daemon = true) {
                val ranges = mutableMapOf<IntRange, User>()
                RoomRepository.rooms[roomId]?.users
                    ?.sortedByDescending { userRepo.getUser(it).name.value.length }
                    ?.forEach {
                        val user = userRepo.getUser(it)
                        Regex(user.name.value).findAll(message).forEach { match ->
                            var alreadyMatched = false
                            ranges.keys.forEach loop@{ prevRange ->
                                if (prevRange.intersect(match.range).isNotEmpty()) {
                                    alreadyMatched = true
                                    return@loop
                                }
                            }
                            if (!alreadyMatched) {
                                ranges[match.range] = user
                            }
                        }
                    }

                var sendText = message
                ranges.toSortedMap(compareByDescending { it.start })
                    .forEach { element ->
                        sendText = sendText.substring(0, element.key.first) + createUserLink(element.value) +
                            sendText.substring(element.key.endInclusive + 1)
                    }

                if (roomId != null) {
                    val room = MatrixHttpRoom(context, roomId)
                    try {
                        room.sendFormattedText(HtmlOperations.sanitize(sendText), HtmlOperations.sanitizeNoHtml(message))
                    } catch (e: MatrixClientRequestException) {
                        // On connection errors try again, until the message has been successfully sent
                        // TODO add to some kind of queue, so that the messages get resent in the correct order
                        sendMessageToCurrentRoom(message)
                    }
                }
            }
        }
    }

    fun sendReadReceipt(room: ChatRoom, eventId: String) {
        runAsync(daemon = true) {
            val matrixRoom = MatrixHttpRoom(context, room.id)
            try {
                matrixRoom.sendReadReceipt(eventId)
            } catch (e: MatrixClientRequestException) {
                // On connection errors try again, until the message has been successfully sent
                sendReadReceipt(room, eventId)
            }
        }
    }

    private fun createUserLink(user: User): String {
        user.fetchDisplayName()
        return """<a href="https://matrix.to/#/@${user.id.localPart}:${user.id.domain}">${user.name.value}</a>"""
    }

    /**
     * Returns true, if older message events have been found, false else
     */
    fun fetchBacklog(chatRoom: ChatRoom): Boolean {
        val matrixRoom = MatrixHttpRoom(context, chatRoom.id)
        val options = MatrixRoomMessageChunkOptions.Builder()
            .setFromToken(chatRoom.fromToken)
            .setDirection(_MatrixRoomMessageChunkOptions.Direction.Backward)

        logger.debug("Start loading messages for room ${chatRoom.name.value}")
        val chunk = matrixRoom.getMessages(options.get())

        val persistentEvents = chunk.events.mapNotNull {
            try {
                MatrixJsonEventFactory.get(it.json)
            } catch (e: InvalidJsonException) {
                logger.debug("Malformed event: ${it.json}", e)
                null
            }
        }
            .filterIsInstance<MatrixJsonRoomEvent>()

        if (persistentEvents.isNotEmpty()) {
            logger.debug("Processing new events for room ${chatRoom.name.value}")
        }

        chatRoom.unprocessedBacklogEvents.addAll(persistentEvents)

        if (chatRoom.fromToken != chunk.endToken) {
            chatRoom.fromToken = chunk.endToken

            // If no message events were fetched, fetch more
            if (persistentEvents.isEmpty()) {
                return fetchBacklog(chatRoom)
            }
        } else {
            logger.debug("There are no more message events in room ${chatRoom.name.value}")
            return false
        }

        logger.debug("Finished processing new events for room ${chatRoom.name.value}")
        return true
    }

    fun sync(options: _SyncOptions): _SyncData {
        return httpClient.sync(options)
    }

    fun loadImage(url: String): BufferedImage? {
        return loadImage(URI(url))
    }

    fun loadImage(url: URI): BufferedImage? {
        return try {
            val content = httpClient.getMedia(url)
            if (content != null && content.isValid) {
                val data = content.data
                if (data != null) {
                    ImageIO.read(ByteArrayInputStream(data))
                } else {
                    logger.error("Image could not be loaded: byte array is empty")
                    null
                }
            } else {
                if (content == null) {
                    logger.trace("Image could not be loaded, content is null")
                } else {
                    logger.trace("Image could not be loaded, content: ${content.isValid}")
                }
                null
            }
        } catch (e: MatrixClientRequestException) {
            // TODO retry on specific errors?
            logger.error("Request exception while loading image", e)
            null
        }
    }
}