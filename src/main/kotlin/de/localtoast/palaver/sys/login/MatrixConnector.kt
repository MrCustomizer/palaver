/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys.login

import de.localtoast.palaver.Settings
import de.localtoast.palaver.sys.PalaverHttpClient
import de.localtoast.palaver.sys.SyncController
import io.kamax.matrix.MatrixID
import io.kamax.matrix.client.MatrixClientContext
import io.kamax.matrix.client.MatrixClientRequestException
import io.kamax.matrix.client.MatrixPasswordCredentials
import io.kamax.matrix.client._AutoDiscoverySettings
import io.kamax.matrix.client.regular.MatrixHttpClient
import io.kamax.matrix.hs.MatrixHomeserver
import org.slf4j.LoggerFactory
import tornadofx.Controller
import java.net.UnknownHostException
import java.util.Optional

/**
 * This class encapsulates the initialization process of the network
 * communication classes. The result is an http
 * client class which is subsequently used to handle the communication
 * with the server.
 */
class MatrixConnector : Controller() {
    enum class LoginResult {
        Success, UnknownError, ServerNotFound, WrongPasswordOrUserNotFound
    }

    private val httpClient: PalaverHttpClient by inject()

    private val loginModel: LoginCredentialsModel by inject()
    private val logger = LoggerFactory.getLogger(this.javaClass.name)

    fun initializeFromSettings() {
        logger.debug("Reading login data from settings file")
        val token = Settings.accessToken
        val baseUrl = Settings.baseUrl
        val domain = Settings.domain
        val idString = Settings.matrixId
        val id = MatrixID.Builder(idString).acceptable()

        logger.debug("baseUrl: $baseUrl")
        logger.debug("domain: $domain")
        logger.debug("idString: $idString")
        logger.debug("id: ${id.id}")

        val homeserver = MatrixHomeserver(domain, baseUrl)
        val context = MatrixClientContext(homeserver, id, token)
        httpClient.setHttpClient(MatrixHttpClient(context))
    }

    fun discoverSettings(domain: String): Optional<_AutoDiscoverySettings> {
        try {
            val discoverClient = MatrixHttpClient(domain)

            val settings = discoverClient.discoverSettings()
            if (settings != null && settings.isPresent) {
                return settings
            }
        } catch (e: Exception) {
            // do nothing
        }
        return Optional.empty()
    }

    fun login(): LoginResult {
        var server = if (loginModel.homeserver.value.isNullOrEmpty()) "matrix.qarte.de" else loginModel.homeserver.value
        val https = "https://"
        val http = "http://"
        var baseUrl = server
        when {
            server.startsWith(https) -> server = server.substring(https.length, server.length)
            server.startsWith(http) -> server = server.substring(http.length, server.length)
            else -> baseUrl = https + server
        }

        val domain = server
        val matrixId = loginModel.matrixId.value
        val password = loginModel.password.value

        val result = login(domain, baseUrl, matrixId, password, loginModel.stayLoggedIn.value)
        if (result == LoginResult.Success) {
            try {
                find<SyncController>().startSyncing()
            } catch (e: MatrixClientRequestException) {
                return LoginResult.UnknownError
            }
        }
        return result
    }

    private fun login(
        domain: String,
        baseUrl: String,
        matrixId: String,
        password: String,
        stayLoggedIn: Boolean = false
    ): MatrixConnector.LoginResult {

        logger.debug("Logging in")
        logger.debug("domain: $domain")
        logger.debug("matrixId: $matrixId")
        logger.debug("stayLoggedIn: $stayLoggedIn")

        val homeserver = MatrixHomeserver(domain, baseUrl)
        val context = MatrixClientContext(homeserver)
        val client = MatrixHttpClient(context)

        val localPart =
            if (matrixId.startsWith("@")) {
                MatrixID.Builder(matrixId).acceptable().localPart
            } else {
                matrixId
            }
        logger.debug("localPart: $localPart")

        val credentials = MatrixPasswordCredentials(localPart, password)
        try {
            client.context?.initialDeviceName = "Palaver"
            client.login(credentials)
        } catch (e: MatrixClientRequestException) {
            logger.debug("Login unsuccessful", e)
            var loginResult = MatrixConnector.LoginResult.UnknownError
            if (e.cause is UnknownHostException) {
                loginResult = MatrixConnector.LoginResult.ServerNotFound
            }
            val error = e.error
            if (error.isPresent &&
                error.get().errcode == "M_FORBIDDEN" &&
                error.get().error == "Invalid password") {
                loginResult = MatrixConnector.LoginResult.WrongPasswordOrUserNotFound
            }
            return loginResult
        } catch (e: IllegalStateException) {
            logger.debug("Login unsuccessful", e)
            return MatrixConnector.LoginResult.UnknownError
        }

        val token = client.accessToken
        if (stayLoggedIn && token != null && token.isPresent) {
            logger.debug("Saving session data")
            Settings.domain = domain
            Settings.baseUrl = baseUrl
            val id = client.user
            if (id != null && id.isPresent) {
                Settings.matrixId = id.get().id
            } else {
                Settings.matrixId = ""
            }
            Settings.accessToken = token.get()
        } else {
            Settings.domain = ""
            Settings.baseUrl = ""
            Settings.matrixId = ""
            Settings.accessToken = ""
        }

        httpClient.setHttpClient(client)
        return MatrixConnector.LoginResult.Success
    }
}