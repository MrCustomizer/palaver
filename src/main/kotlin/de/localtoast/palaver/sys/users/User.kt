/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys.users

import de.localtoast.palaver.sys.GraphicUtilities
import de.localtoast.palaver.sys.PalaverHttpClient
import io.kamax.matrix._MatrixID
import io.kamax.matrix.client.MatrixClientRequestException
import io.kamax.matrix.client.MatrixHttpUser
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.scene.image.Image
import org.slf4j.LoggerFactory
import java.awt.image.BufferedImage
import java.util.concurrent.Semaphore

/**
 * This class gives access to various user-related data. It caches all the accessed data, so every information
 * has only to be fetched once from the server.
 */
class User(displayName: String = "", val id: _MatrixID, val avatarUrl: String? = null, private val client: PalaverHttpClient) {
    companion object {
        private val backupAvatar = Image("/de/localtoast/palaver/icons/avatars/avatar-default.png")
        private val backupAvatarMini = Image("/de/localtoast/palaver/icons/avatars/avatar-default-mini.png")
    }

    private val logger = LoggerFactory.getLogger(this.javaClass.name)

    private var avatarFetched = false
    private val fetchAvatarSemaphore = Semaphore(1)
    private var nameFetched = displayName.isNotBlank()
    private val fetchNameSemaphore = Semaphore(1)

    val name = SimpleStringProperty(if (displayName == "") id.id else displayName)

    val scaledAndCutOutAvatar = SimpleObjectProperty<Image>(backupAvatar)
    val scaledAndCutOutAvatarMini = SimpleObjectProperty<Image>(backupAvatarMini)

    /**
     * This method forces fetching of the user's name from the server
     */
    fun fetchDisplayName() {
        fetchMetadata(fetchAvatar = false)
    }

    /**
     * This method forces fetching of all the user's metadata from the server
     */
    fun fetchMetadata(fetchAvatar: Boolean = true, fetchDisplayName: Boolean = true) {
        if (fetchAvatar && !avatarFetched) {
            if (fetchAvatarSemaphore.tryAcquire()) {
                fetchAvatarInternal()
                avatarFetched = true
                fetchAvatarSemaphore.release()
            }
        }

        if (fetchDisplayName && !nameFetched) {
            if (fetchNameSemaphore.tryAcquire()) {
                fetchDisplayNameInternal()
                nameFetched = true
                fetchNameSemaphore.release()
            }
        }
    }

    private fun fetchDisplayNameInternal() {
        try {
            val displayName = MatrixHttpUser(client.context, id).name
            if (displayName.isPresent) {
                name.value = displayName.get()
            }
        } catch (e: MatrixClientRequestException) {
            // ignore, if avatar could not be loaded
            logger.trace("${id.id}: Request exception while loading display name", e)
        }
    }

    private fun fetchAvatarInternal() {
        logger.debug("${id.id}: Start fetching user avatar: $avatarUrl")
        val url = if (avatarUrl == null || avatarUrl.toString() == "") {
            try {
                val urlOptional = MatrixHttpUser(client.context, id).avatarUrl
                if (urlOptional.isPresent) {
                    urlOptional.get()
                } else {
                    null
                }
            } catch (e: MatrixClientRequestException) {
                // TODO retry on timeout error 504
                logger.trace("${id.id}: Error while loading the avatar's URL", e)
                null
            }
        } else {
            avatarUrl
        }
        if (url != null) {
            val image = client.loadImage(url)

            if (image != null) {
                setAvatarWithScaling(image)
            } else {
                logger.trace("${id.id}: Avatar image could not be read: ImageIO result is null")
            }
        } else {
            logger.trace("${id.id}: No avatar set for this user")
        }
        logger.debug("${id.id}: Finished fetching user avatar")
    }

    private fun setAvatarWithScaling(bufferedImage: BufferedImage) {
        val scaledImage = GraphicUtilities.scaleAvatar(bufferedImage, GraphicUtilities.avatarSize)
        val scaledImageMini = GraphicUtilities.scaleAvatar(bufferedImage, GraphicUtilities.avatarSizeMini)

        if (scaledImage != null) {
            val roundImage = GraphicUtilities.cutOutCircle(scaledImage)
            scaledAndCutOutAvatar.value = roundImage
        } else {
            logger.error("${id.id}: User's avatar could not be scaled to default format")
        }

        if (scaledImageMini != null) {
            val roundImage = GraphicUtilities.cutOutCircle(scaledImageMini)
            scaledAndCutOutAvatarMini.value = roundImage
        } else {
            logger.error("${id.id}: User's avatar could not be scaled to mini format")
        }
    }
}