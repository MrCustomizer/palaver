/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom.backlog

import javafx.scene.Node
import javafx.scene.layout.Pane
import org.fxmisc.flowless.Cell

class BacklogCell(private val fragment: BacklogCellFragment) : Cell<BacklogRow, Pane> {

    override fun getNode(): Pane {
        fragment.root.requestLayout()
        return fragment.root
    }

    override fun toString(): String {
        return fragment.root.toString()
    }

    override fun updateIndex(index: Int) {
        fragment.updateIndex(index)
        super.updateIndex(index)
    }

    fun getOverlayTarget(index: Int): Node? = fragment.getOverlayTarget(index)

    fun getRow(): BacklogRow = fragment.getRowInternal()

    fun lastTextFlowIndex(): Int? = fragment.lastTextFlowIndex()

    fun lastTextLength(): Int? = fragment.lastTextLength()

    fun nodeHit(screenX: Double, screenY: Double): Int? = fragment.nodeHit(screenX, screenY)

    fun characterHit(screenX: Double, screenY: Double): Int? = fragment.characterHit(screenX, screenY)

    fun setSelection(start: SelectionIndexInCell, end: SelectionIndexInCell) = fragment.setSelection(start, end)

    fun clearSelection() = fragment.clearSelection()

    fun getText(start: SelectionIndex?, end: SelectionIndex?): String = fragment.getText(start, end)

    override fun dispose() {
        fragment.dispose()
        super.dispose()
    }
}