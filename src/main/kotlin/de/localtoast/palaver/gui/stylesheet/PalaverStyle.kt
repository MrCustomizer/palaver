/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.stylesheet

import javafx.scene.paint.Color
import javafx.scene.paint.LinearGradient
import javafx.scene.paint.Paint
import tornadofx.FXVisibility
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.c
import tornadofx.cssclass
import tornadofx.cssproperty
import tornadofx.mixin
import tornadofx.px

class PalaverStyle : Stylesheet() {

    companion object {
        val button by cssclass()

        val mainWindow by cssclass()

        val logoPane by cssclass()

        val labelWithLinks by cssclass()

        val filterBoxInputFieldLight by cssclass()
        val filterBoxLight by cssclass()
        val filterBoxPaddingLight by cssclass()

        val iconColor by cssproperty<Paint>("-fx-icon-color")

        val borderColor = c("#dce0e3")

        val lightBlue = c("#19a1f9")
        val selectionColor = c("#82ceff")

        val textColorGrey = c("#969696")

        val defaultBackground = Color.WHITE
        val darkBackground = c("#363e47")
        val boxBackground = c("#fbfbfb")

        val scrollBarStyleMixin = mixin {
            thumb {
                opacity = 0.5
                backgroundColor += c("#d2d2d2")
                backgroundRadius += box(5.px)
            }

            vertical {
                incrementButton {
                    padding = box(0.px, 12.px, 0.px, 0.px)
                }
            }
            horizontal {
                incrementButton {
                    padding = box(0.px, 0.px, 12.px, 0.px)
                }
            }
            incrementArrow {
                visibility = FXVisibility.HIDDEN
                padding = box(0.px)
            }
            decrementArrow {
                visibility = FXVisibility.HIDDEN
                padding = box(0.px)
            }
        }

        val scrollBarDefaultBackgroundMixin = mixin {
            backgroundColor += PalaverStyle.defaultBackground
            scrollBar {
                backgroundColor += PalaverStyle.defaultBackground
            }
            scrollPane {
                backgroundColor += PalaverStyle.defaultBackground
            }
            corner {
                backgroundColor += PalaverStyle.defaultBackground
            }
        }

        val scrollBarBoxBackgroundMixin = mixin {
            backgroundColor += PalaverStyle.boxBackground
            scrollBar {
                backgroundColor += PalaverStyle.boxBackground
            }
            scrollPane {
                backgroundColor += PalaverStyle.boxBackground
            }
            corner {
                backgroundColor += PalaverStyle.boxBackground
            }
        }

        val scrollBarDarkBackgroundMixin = mixin {
            backgroundColor += PalaverStyle.darkBackground
            scrollBar {
                backgroundColor += PalaverStyle.darkBackground
            }
            scrollPane {
                backgroundColor += PalaverStyle.darkBackground
            }
            corner {
                backgroundColor += PalaverStyle.darkBackground
            }
        }
    }

    init {

        button {
            backgroundColor += LinearGradient.valueOf("from 50% 0% to 50% 100%, #00aff0 0%, #006391 100%")
            +FontStyle.buttonMixin

            prefWidth = 200.px

            and(hover) {
                backgroundColor += LinearGradient.valueOf("from 50% 0% to 50% 100%, #04bbff 0%, #006391 100%")
            }
            and(pressed) {
                backgroundColor += c("#007dac")
            }
        }

        mainWindow {
            backgroundColor += c("#f3f3f3")
        }

        logoPane {
            backgroundColor += c("#272f34")
            padding = box(0.px, 0.px, 0.px, 10.px)
        }

        labelWithLinks {
            +scrollBarBoxBackgroundMixin
            +scrollBarStyleMixin
        }

        filterBoxLight {
            borderWidth += box(1.px)
            borderColor += box(c("b3b3b3"))
            borderRadius += box(15.px)
            backgroundColor += Color.TRANSPARENT
        }

        filterBoxInputFieldLight {
            +FontStyle.regularMixin

            backgroundColor += PalaverStyle.defaultBackground

            textBoxBorder = Color.TRANSPARENT

            and(focused) {
                borderWidth += box(0.px)
                focusColor = Color.TRANSPARENT
                faintFocusColor = Color.TRANSPARENT
            }
        }

        filterBoxPaddingLight {
            padding = box(1.px, 12.px, 1.px, 12.px)
        }
    }
}