/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver

import de.localtoast.palaver.sys.HotkeyManager
import javafx.scene.input.KeyCode
import net.harawata.appdirs.AppDirsFactory
import org.apache.commons.configuration2.PropertiesConfiguration
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder
import org.apache.commons.configuration2.builder.fluent.Parameters
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Locale

object Settings {
    val localeDisplay = Locale.getDefault(Locale.Category.DISPLAY) ?: Locale("en")
    val localeFormat = Locale.getDefault(Locale.Category.FORMAT) ?: localeDisplay

    private val builder = FileBasedConfigurationBuilder(PropertiesConfiguration::class.java)

    private val windowPosXKey = "windowPosX"
    private val windowPosYKey = "windowPosY"
    private val windowWidthKey = "windowWidth"
    private val windowHeightKey = "windowHeight"
    private val windowMaximizedKey = "windowMaximized"

    private val lastRoomKey = "lastRoom"

    private val matrixIdKey = "matrixId"
    private val accessTokenKey = "accessToken"
    private val domainKey = "domain"

    private val baseUrlKey = "baseUrl"

    private val roomShortcut = "roomShortcut"

    private var profile = "defaultProfile"

    var windowPosX: Int
        set(value) = setProperty(windowPosXKey, value.toString())
        get() {
            return try {
                getStringProperty(windowPosXKey).toInt()
            } catch (e: NumberFormatException) {
                0
            }
        }

    var windowPosY: Int
        set(value) = setProperty(windowPosYKey, value.toString())
        get() {
            return try {
                getStringProperty(windowPosYKey).toInt()
            } catch (e: NumberFormatException) {
                0
            }
        }

    var windowWidth: Int
        set(value) = setProperty(windowWidthKey, value.toString())
        get() {
            return try {
                getStringProperty(windowWidthKey).toInt()
            } catch (e: NumberFormatException) {
                0
            }
        }

    var windowHeight: Int
        set(value) = setProperty(windowHeightKey, value.toString())
        get() {
            return try {
                getStringProperty(windowHeightKey).toInt()
            } catch (e: NumberFormatException) {
                0
            }
        }

    var windowMaximized: Boolean
        set(value) = setProperty(windowMaximizedKey, value)
        get() = getBooleanProperty(windowMaximizedKey, false)

    var lastRoom: String
        set(value) = setProperty(lastRoomKey, value)
        get() = getStringProperty(lastRoomKey)

    var matrixId: String
        set(value) = setProperty(matrixIdKey, value)
        get() = getStringProperty(matrixIdKey)

    var accessToken: String
        set(value) = setProperty(accessTokenKey, value)
        get() = getStringProperty(accessTokenKey)

    var domain: String
        set(value) = setProperty(domainKey, value)
        get() = getStringProperty(domainKey)

    var baseUrl: String
        set(value) = setProperty(baseUrlKey, value)
        get() = getStringProperty(baseUrlKey)

    fun initSettings(profile: String?) {
        if (profile != null) {
            this.profile = profile
        }
        val configDir = getProfilePath()
        val configFile = Paths.get(configDir.toAbsolutePath().toString(), "palaver.config")

        if (!Files.exists(configDir)) {
            Files.createDirectories(configDir)
        }
        if (!Files.exists(configFile)) {
            Files.createFile(configFile)
        }

        val params = Parameters()

        builder.configure(params.properties().setFile(configFile.toFile()))
    }

    fun getProfilePath(): Path {
        val name = "palaver"
        val appDirs = AppDirsFactory.getInstance()
        return Paths.get(appDirs.getUserConfigDir(name, null, "localtoast"), profile)
    }

    fun setShortcutForRoom(code: KeyCode, roomId: String) {
        setProperty(roomShortcut + code.getName(), roomId)
    }

    fun getRoomFromShortcut(code: KeyCode): String {
        return getStringProperty(roomShortcut + code.getName())
    }

    fun getShortcutForRoom(id: String): KeyCode? {
        return HotkeyManager.allowedRoomShortcuts.firstOrNull {
            getRoomFromShortcut(it) == id
        }
    }

    private fun setProperty(key: String, value: Any) {
        builder.configuration.setProperty(key, value)
        save()
    }

    private fun getStringProperty(key: String): String {
        return builder.configuration.getString(key) ?: ""
    }

    private fun getBooleanProperty(key: String, defaultValue: Boolean): Boolean {
        return builder.configuration.getBoolean(key, defaultValue)
    }

    private fun save() {
        builder.save()
    }
}
