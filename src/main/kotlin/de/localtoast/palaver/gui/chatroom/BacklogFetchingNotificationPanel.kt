/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom

import de.localtoast.palaver.gui.UiController
import de.localtoast.palaver.gui.stylesheet.ChatRoomStyle
import javafx.animation.Timeline
import javafx.geometry.Pos
import javafx.scene.control.ProgressIndicator
import javafx.util.Duration
import tornadofx.Fragment
import tornadofx.addClass
import tornadofx.get
import tornadofx.hbox
import tornadofx.keyframe
import tornadofx.label
import tornadofx.timeline

class BacklogFetchingNotificationPanel : Fragment() {

    private var fadingInAnimation: Timeline? = null
    private var fadingOutAnimation: Timeline? = null

    override val root = hbox {
        opacity = 0.0
        alignment = Pos.CENTER

        addClass(ChatRoomStyle.backlogFetchingPanel)
    }

    fun fadeIn() {
        if (root.children.isEmpty()) {
            add(ProgressIndicator().apply {
                progress = -1.0
                maxHeight = 15.0
            })
            add(label(messages["FetchingBacklog"]) {
                addClass(ChatRoomStyle.backlogFetchingPanel)
            })
        }

        fadingOutAnimation?.stop()
        fadingInAnimation = timeline {
            keyframe(Duration.millis(UiController.animationDurationOut.toMillis())) {
                keyvalue(root.opacityProperty(), 1.0)
            }
        }
    }

    fun fadeOut() {
        fadingInAnimation?.stop()
        fadingOutAnimation = timeline {
            keyframe(Duration.millis(UiController.animationDurationOut.toMillis())) {
                keyvalue(root.opacityProperty(), 0.0)
            }
        }.apply {
            setOnFinished { root.children.clear() }
        }
    }
}