/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom.backlog

import de.localtoast.palaver.sys.events.Event
import de.localtoast.palaver.sys.events.HistoryVisibilityEvent
import de.localtoast.palaver.sys.events.MembershipEvent
import de.localtoast.palaver.sys.events.MessageEvent
import de.localtoast.palaver.sys.events.NameEvent
import de.localtoast.palaver.sys.events.PowerLevelEvent
import de.localtoast.palaver.sys.events.TopicEvent
import javafx.beans.property.BooleanProperty
import java.time.Instant

/**
 * Represents the content of one cell in the flowless layout for the backlog view.
 */
interface BacklogRow

interface EventRow : BacklogRow {
    val event: Event
}

data class DayRow(val day: String, val timestamp: Instant) : BacklogRow

data class MessageRow(override val event: MessageEvent, val header: BooleanProperty) : EventRow

data class TopicRow(override val event: TopicEvent) : EventRow

data class NameRow(override val event: NameEvent) : EventRow

data class MembershipRow(override val event: MembershipEvent) : EventRow

data class PowerLevelRow(override val event: PowerLevelEvent) : EventRow

data class HistoryVisibilityRow(override val event: HistoryVisibilityEvent) : EventRow
