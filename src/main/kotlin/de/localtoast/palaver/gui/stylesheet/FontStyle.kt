/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.stylesheet

import javafx.scene.layout.BorderStrokeStyle
import javafx.scene.paint.Color
import javafx.scene.shape.StrokeLineCap
import javafx.scene.shape.StrokeLineJoin
import javafx.scene.shape.StrokeType
import javafx.scene.text.Font
import javafx.scene.text.FontPosture
import javafx.scene.text.FontSmoothingType
import javafx.scene.text.FontWeight
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.mixin
import tornadofx.pt
import tornadofx.px

class FontStyle : Stylesheet() {
    companion object {
        private const val fontPath = "/de/localtoast/palaver/fonts/ubuntu/"
        val defaultFontSize = 13.px

        // Following this advice: https://stackoverflow.com/a/42544032/261769
        private fun getFont(name: String): Font? =
            PalaverStyle::class.java.getResourceAsStream("$fontPath$name.ttf").use { Font.loadFont(it, 10.0) }

        private val ubuntuLight = getFont("Ubuntu-L")
        private val ubuntuRegular = getFont("Ubuntu-R")
        private val ubuntuRegularItalic = getFont("Ubuntu-RI")
        private val ubuntuBold = getFont("Ubuntu-B")
        private val ubuntuBoldItalic = getFont("Ubuntu-BI")

        private val ubuntuMonoRegular = getFont("UbuntuMono-R")
        private val ubuntuMonoRegularItalic = getFont("UbuntuMono-RI")
        private val ubuntuMonoBold = getFont("UbuntuMono-B")
        private val ubuntuMonoBoldItalic = getFont("UbuntuMono-BI")

        val lightMixin = mixin {
            ubuntuLight?.let { fontFamily = it.family }
            fontWeight = FontWeight.LIGHT
            fontSize = defaultFontSize
        }

        val regularMixin = mixin {
            ubuntuRegular?.let { fontFamily = it.family }
            fontWeight = FontWeight.NORMAL
            fontSize = defaultFontSize
            fontSmoothingType = FontSmoothingType.LCD
        }

        val regularItalicMixin = mixin {
            ubuntuRegularItalic?.let { fontFamily = it.family }
            fontWeight = FontWeight.NORMAL
            fontStyle = FontPosture.ITALIC
            fontSize = defaultFontSize
            fontSmoothingType = FontSmoothingType.LCD
        }

        val boldMixin = mixin {
            ubuntuBold?.let { fontFamily = it.family }
            fontWeight = FontWeight.BOLD
            fontSize = defaultFontSize
            fontSmoothingType = FontSmoothingType.LCD
        }

        val boldItalicMixin = mixin {
            ubuntuBoldItalic?.let { fontFamily = it.family }
            fontWeight = FontWeight.BOLD
            fontStyle = FontPosture.ITALIC
            fontSize = defaultFontSize
            fontSmoothingType = FontSmoothingType.LCD
        }

        private const val monospacedSizeOffset = 1

        val regularMonoMixin = mixin {
            ubuntuMonoRegular?.let { fontFamily = it.family }
            fontWeight = FontWeight.NORMAL
            fontSize = defaultFontSize + monospacedSizeOffset
            fontSmoothingType = FontSmoothingType.LCD
        }

        val regularMonoItalicMixin = mixin {
            ubuntuMonoRegularItalic?.let { fontFamily = it.family }
            fontWeight = FontWeight.NORMAL
            fontStyle = FontPosture.ITALIC
            fontSize = defaultFontSize + monospacedSizeOffset
            fontSmoothingType = FontSmoothingType.LCD
        }

        val boldMonoMixin = mixin {
            ubuntuMonoBold?.let { fontFamily = it.family }
            fontWeight = FontWeight.BOLD
            fontSize = defaultFontSize + monospacedSizeOffset
            fontSmoothingType = FontSmoothingType.LCD
        }

        val boldMonoItalicMixin = mixin {
            ubuntuMonoBoldItalic?.let { fontFamily = it.family }
            fontWeight = FontWeight.BOLD
            fontStyle = FontPosture.ITALIC
            fontSize = defaultFontSize + monospacedSizeOffset
            fontSmoothingType = FontSmoothingType.LCD
        }

        val buttonMixin = mixin {
            +boldMixin
            fontSize = defaultFontSize
            textFill = Color.WHITE
            fontSmoothingType = FontSmoothingType.LCD
        }

        val regular by cssclass()

        val bold by cssclass()
        val title by cssclass()
        val paragraphTitle by cssclass()
        val logo by cssclass()
        val hyperlinkFont by cssclass()
        val progressIndicatorFont by cssclass()
    }

    init {

        regular {
            +regularMixin
        }

        bold {
            +boldMixin
        }

        title {
            +boldMixin
        }

        paragraphTitle {
            +regularMixin
            textFill = PalaverStyle.lightBlue
            fontSize = 11.pt
            borderWidth += box(0.px, 0.px, 1.px, 0.px)
            borderStyle += BorderStrokeStyle(
                StrokeType.INSIDE, StrokeLineJoin.MITER, StrokeLineCap.BUTT, 10.0,
                0.0, mutableListOf(1.0, 1.0))
            borderColor += box(PalaverStyle.borderColor)
            padding = box(0.px, 0.px, 3.px, 0.px)
        }

        logo {
            +regularMixin
            textFill = Color.WHITE
            fontSize = 16.px
        }

        hyperlinkFont {
            +regularMixin
            fill = PalaverStyle.lightBlue
        }

        progressIndicatorFont {
            +regularMixin
            fontSize = defaultFontSize
        }
    }
}