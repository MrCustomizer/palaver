/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys.events

import io.kamax.matrix._MatrixID
import javafx.scene.image.Image
import java.time.Instant

interface Event {
    val id: String
    val originServerTimestamp: Instant
    val sender: _MatrixID
}

data class MessageEvent(
    override val id: String,
    override val originServerTimestamp: Instant,
    override val sender: _MatrixID,
    val content: String,
    val imageUrl: String,
    val image: Image?
) : Event

data class TopicEvent(
    override val id: String,
    override val originServerTimestamp: Instant,
    override val sender: _MatrixID,
    val topic: String?
) : Event

data class NameEvent(
    override val id: String,
    override val originServerTimestamp: Instant,
    override val sender: _MatrixID,
    val name: String?
) : Event

data class MembershipEvent(
    override val id: String,
    override val originServerTimestamp: Instant,
    override val sender: _MatrixID,
    val membership: String,
    val invitee: _MatrixID,
    val displayName: String?,
    val avatarUrl: String?,
    val prevMembership: String?,
    val prevAvatarUrl: String?,
    val prevDisplayName: String?
) : Event

data class PowerLevelEvent(
    override val id: String,
    override val originServerTimestamp: Instant,
    override val sender: _MatrixID,
    val users: String,
    val newLevel: Double
) : Event

data class HistoryVisibilityEvent(
    override val id: String,
    override val originServerTimestamp: Instant,
    override val sender: _MatrixID,
    val visibility: String
) : Event
