/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys.events

import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import de.localtoast.palaver.sys.PalaverHttpClient
import de.localtoast.palaver.sys.chatroom.ChatRoom
import de.localtoast.palaver.sys.chatroom.ChatRoomModel
import de.localtoast.palaver.sys.chatroom.ChatRoomName
import de.localtoast.palaver.sys.chatroom.ChatRoomTopic
import de.localtoast.palaver.sys.chatroom.RoomRepository
import de.localtoast.palaver.sys.users.User
import de.localtoast.palaver.sys.users.UserRepository
import io.kamax.matrix._MatrixID
import io.kamax.matrix.client.MatrixHttpContent
import io.kamax.matrix.client._SyncData
import io.kamax.matrix.event._MatrixEvent
import io.kamax.matrix.json.InvalidJsonException
import io.kamax.matrix.json.MatrixJsonEventFactory
import io.kamax.matrix.json.event.MatrixJsonReadReceiptEvent
import io.kamax.matrix.json.event.MatrixJsonRoomAliasesEvent
import io.kamax.matrix.json.event.MatrixJsonRoomAvatarEvent
import io.kamax.matrix.json.event.MatrixJsonRoomHistoryVisibilityEvent
import io.kamax.matrix.json.event.MatrixJsonRoomMembershipEvent
import io.kamax.matrix.json.event.MatrixJsonRoomMessageEvent
import io.kamax.matrix.json.event.MatrixJsonRoomNameEvent
import io.kamax.matrix.json.event.MatrixJsonRoomPowerLevelsEvent
import io.kamax.matrix.json.event.MatrixJsonRoomTopicEvent
import javafx.application.Platform
import javafx.scene.image.Image
import org.slf4j.LoggerFactory
import tornadofx.Controller
import java.io.ByteArrayInputStream
import java.net.URI
import java.time.Instant
import kotlin.collections.set

class MatrixEventProcessor : Controller() {
    private val roomModel: ChatRoomModel by inject()
    private val userRepo: UserRepository by inject()
    private val roomRepo: RoomRepository by inject()
    private val eventRepo: EventRepository by inject()
    private val logger = LoggerFactory.getLogger(this.javaClass.name)
    private val client: PalaverHttpClient by inject()

    fun processSyncResult(syncData: _SyncData) {
        logger.debug("Start processing sync results")
        syncData.rooms.joined.forEach {

            val events = mutableListOf<_MatrixEvent>().plus(it.timeline.events)
                .plus(it.state.events).plus(it.ephemeral.events)

            val roomData = RoomData()
            val newUsersData = mutableMapOf<_MatrixID, UserData>()
            val users = mutableSetOf<_MatrixID>()

            events.mapNotNull { parseEvent(it.json) }
                .forEach {
                    when (it) {
                        is MatrixJsonRoomTopicEvent -> if (it.topic.isPresent) {
                            val age = (it.json["unsigned"] as JsonObject)["age"].asInt
                            val newTopic = ChatRoomTopic(age, it.topic.get())
                            val existingTopic = roomData.topic
                            if (existingTopic != null) {
                                if (existingTopic.age > age) {
                                    roomData.topic = newTopic
                                }
                            } else {
                                roomData.topic = newTopic
                            }
                        }
                    // TODO How are removals of aliases handled?
                        is MatrixJsonRoomAliasesEvent -> roomData.aliases.addAll(it.aliases)
                        is MatrixJsonRoomNameEvent -> if (it.name.isPresent) {
                            val age = (it.json["unsigned"] as JsonObject)["age"].asInt
                            val newName = ChatRoomName(age, it.name.get())
                            val existingName = roomData.name
                            if (existingName != null) {
                                if (existingName.age > age) {
                                    roomData.name = newName
                                }
                            } else {
                                roomData.name = newName
                            }
                        }
                        is MatrixJsonRoomMembershipEvent -> {
                            val id = it.invitee

                            if (it.membership == "join") {
                                if (userRepo.containsUser(id)) {
                                    users.add(id)
                                } else {
                                    /*
                                     * Users in different event types can contain different information. The temp user is used to
                                     * collect all the information present, before creating the real user further down.
                                     */
                                    val tempUser = newUsersData[id] ?: UserData(id)
                                    if (it.avatarUrl.isPresent && tempUser.avatarUrl == "") {
                                        tempUser.avatarUrl = it.avatarUrl.get()
                                    }
                                    if (it.displayName.isPresent && tempUser.name == "") {
                                        tempUser.name = it.displayName.get()
                                    }
                                    newUsersData[id] = tempUser
                                }
                            }
                        }
                        is MatrixJsonReadReceiptEvent -> {
                            it.receipts.forEach {
                                if (it.usersWithTimestamp != null) {
                                    roomData.readMarkers[it.eventId] = it.usersWithTimestamp
                                        .mapValues { Instant.ofEpochMilli(it.component2()) }
                                }
                            }
                        }
                    }
                }

            newUsersData.map {
                val user = it.value
                User(user.name, it.key, user.avatarUrl, client)
            }.forEach {
                userRepo.addUser(it)
                users.add(it.id)
            }

            // TODO Do we really need the two different ways (room present vs. room new)?
            val room = if (RoomRepository.rooms.containsKey(it.id)) {
                val roomIntern = RoomRepository.rooms.getValue(it.id)
                val topic = roomData.topic
                if (topic != null) {
                    if (roomIntern.topic.value == null || roomIntern.topic.value.age > topic.age) {
                        roomIntern.topic.value = roomData.topic
                    }
                }
                if (roomData.aliases.isNotEmpty()) {
                    roomIntern.aliases.addAll(roomData.aliases)
                }
                if (roomData.name != null) {
                    roomIntern.realRoomName.value = roomData.name
                }
                updateReadMarkers(roomData, roomIntern)
                roomIntern
            } else {
                val roomIntern = roomRepo.createRoom(it.id, roomData.topic,
                    roomData.name, users, roomData.aliases)
                updateReadMarkers(roomData, roomIntern)
                loadAvatar(roomIntern, events)
                roomIntern
            }

            room.unreadCount.value = it.unreadNotifications?.notificationCount ?: 0
            room.highlightCount.value = it.unreadNotifications?.highlightCount ?: 0
            room.fromToken = it.timeline.previousBatchToken

            // TODO put all events in this backlog queue instead of handling them here (in the preceding lines)?
            room.unprocessedBacklogEvents.addAll(it.timeline.events.map { MatrixJsonEventFactory.get(it.json) }
                .filter {
                    it is MatrixJsonRoomMessageEvent ||
                        it is MatrixJsonRoomTopicEvent ||
                        it is MatrixJsonRoomNameEvent ||
                        it is MatrixJsonRoomMembershipEvent ||
                        it is MatrixJsonRoomPowerLevelsEvent ||
                        it is MatrixJsonRoomHistoryVisibilityEvent
                })

            Platform.runLater {
                roomModel.addRoomIfNew(room)
            }
        }
    }

    private fun parseEvent(json: JsonObject): _MatrixEvent? {
        return try {
            MatrixJsonEventFactory.get(json)
        } catch (e: InvalidJsonException) {
            logger.debug("Malformed event: $json", e)
            null
        }
    }

    private fun updateReadMarkers(roomData: RoomData, roomIntern: ChatRoom) {
        roomData.readMarkers.forEach { readMarkers ->
            readMarkers.value.forEach {
                roomIntern.readMarkerManager.updateReadMarker(readMarkers.key, it.key, it.value)
            }
        }
    }

    private fun loadAvatar(room: ChatRoom, events: List<_MatrixEvent>) {
        var url = events.map { parseEvent(it.json) }
            .filterIsInstance<MatrixJsonRoomAvatarEvent>()
            .map { it.url }
            .firstOrNull()
        if (url != null) {
            runAsync(daemon = true) {
                room.fetchMetadata(URI(url))
            }
        }
    }

    // TODO Refactoring: Try to improve structure
    fun processNewRoomEvent(roomId: String?, matrixEvent: _MatrixEvent?): Event? {
        if (roomId != null && matrixEvent != null) {
            // TODO handle other matrixEvent types
            // TODO filter duplicate events
            val typedMatrixEvent = parseEvent(matrixEvent.json)
            val event = when (typedMatrixEvent) {
                is MatrixJsonRoomMessageEvent -> {
                    val format = typedMatrixEvent.format
                    val formattedBody = typedMatrixEvent.formattedBody

                    val content = typedMatrixEvent.json["content"] as JsonObject
                    val msgType = content["msgtype"]
                    val url = if (msgType != null && msgType.asString == "m.image") {
                        content["url"].asString ?: ""
                    } else ""

                    val image = if (url.isNotBlank()) {
                        val imageContent = MatrixHttpContent(client.context, URI(url))
                        if (imageContent.isValid) {
                            val data = imageContent.data
                            Image(ByteArrayInputStream(data))
                        } else null
                    } else null

                    val body = when {
                        format.isPresent && format.get() == "org.matrix.custom.html"
                            && formattedBody.isPresent -> formattedBody.get()
                        else -> try {
                            typedMatrixEvent.body
                        } catch (e: NullPointerException) {
                            // TODO find out, why NPEs can happen. Reproduce by loading backlog in #test:matrix.org
                            ""
                        }
                    }

                    val user = userRepo.getUser(typedMatrixEvent.sender)
                    runAsync(daemon = true) {
                        user.fetchMetadata()
                    }

                    val nameSender = userRepo.getUser(typedMatrixEvent.sender).name.value

                    MessageEvent(typedMatrixEvent.id,
                        Instant.ofEpochMilli(typedMatrixEvent.time),
                        typedMatrixEvent.sender,
                        when (msgType?.asString) {
                            "m.emote" -> "${nameSender ?: ""} $body"
                            else -> body
                        },
                        url,
                        image)
                }

                is MatrixJsonRoomTopicEvent -> {
                    TopicEvent(typedMatrixEvent.id,
                        Instant.ofEpochMilli(typedMatrixEvent.time),
                        typedMatrixEvent.sender,
                        typedMatrixEvent.topic.orElse(null))
                }

                is MatrixJsonRoomNameEvent -> {
                    NameEvent(typedMatrixEvent.id,
                        Instant.ofEpochMilli(typedMatrixEvent.time),
                        typedMatrixEvent.sender,
                        typedMatrixEvent.name.orElse(null))
                }

                is MatrixJsonRoomMembershipEvent -> {

                    /*
                     * prevContent is currently unspecced. Because of this, it won't be added to the SDK, yet.
                     * This parsing parsing part can be replaced as soon as it is specced and added to the SDK.
                     */
                    val prevContent = (typedMatrixEvent.json["unsigned"] as JsonObject?)?.get("prev_content") as JsonObject?

                    val prevMembership = when {
                        prevContent != null && prevContent["membership"] is JsonPrimitive -> prevContent["membership"].asString
                        else -> null
                    }

                    val prevAvatarUrl = when {
                        prevContent != null && prevContent["avatar_url"] is JsonPrimitive -> prevContent["avatar_url"].asString
                        else -> null
                    }

                    val prevDisplayName = when {
                        prevContent != null && prevContent["displayname"] is JsonPrimitive -> prevContent["displayname"].asString
                        else -> null
                    }

                    MembershipEvent(typedMatrixEvent.id,
                        Instant.ofEpochMilli(typedMatrixEvent.time),
                        typedMatrixEvent.sender,
                        typedMatrixEvent.membership,
                        typedMatrixEvent.invitee,
                        typedMatrixEvent.displayName.orElse(null),
                        typedMatrixEvent.avatarUrl.orElse(null),
                        prevMembership,
                        prevAvatarUrl,
                        prevDisplayName
                    )
                }

                is MatrixJsonRoomPowerLevelsEvent -> {
                    /*
                     * prevContent is currently unspecced. Because of this, it won't be added to the SDK, yet.
                     * This parsing parsing part can be replaced as soon as it is specced and added to the SDK.
                     */
                    val prevContent = (typedMatrixEvent.json["unsigned"] as JsonObject?)?.get("prev_content") as JsonObject?

                    val prevUsers = prevContent?.getAsJsonObject("users")?.entrySet()
                        ?.map { (it.key ?: "") to it.value.asDouble }
                        ?.toMap()

                    val users = typedMatrixEvent.users.filter { prevUsers?.get(it.key) != it.value }

                    if (users.size > 1) {
                        logger.error("More than one power level change in one event!")
                    }

                    if (users.size == 1) {
                        val user = users.keys.first()
                        val level = users[user]

                        if (user != null && level != null) {
                            PowerLevelEvent(typedMatrixEvent.id,
                                Instant.ofEpochMilli(typedMatrixEvent.time),
                                typedMatrixEvent.sender,
                                user,
                                level
                            )
                        } else null
                    } else null
                }

                is MatrixJsonRoomHistoryVisibilityEvent -> {
                    HistoryVisibilityEvent(
                        typedMatrixEvent.id,
                        Instant.ofEpochMilli(typedMatrixEvent.time),
                        typedMatrixEvent.sender,
                        typedMatrixEvent.historyVisibility
                    )
                }

                else -> null
            }

            if (event != null) {
                eventRepo.addEvent(event)
            }

            return event
        }

        return null
    }
}