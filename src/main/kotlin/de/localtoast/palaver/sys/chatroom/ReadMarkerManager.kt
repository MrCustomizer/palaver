/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.sys.chatroom

import de.localtoast.palaver.gui.chatroom.backlog.BacklogManager
import de.localtoast.palaver.sys.events.EventRepository
import io.kamax.matrix._MatrixID
import java.time.Instant

/*
 * The read markers can't be directly displayed, for several reasons:
 * First, there are no read marker events for the messages of the client's user itself. If you send messages, the read
 * marker stays on the last message sent by someone else.
 * Second, if the read marker is currently on the newest message, the client doesn't want to display it.
 *
 * This class implements an algorithm which interprets the read markers sent from the server and takes into account the
 * current state of the client, to decide if a read marker should be displayed or not and where it should be displayed.
 */
class ReadMarkerManager(
    private val backlogManager: BacklogManager,
    private val eventRepo: EventRepository,
    private val currentUser: _MatrixID?
) {

    // Actual read markers sent from server
    private val originalReadMarkers = mutableMapOf<_MatrixID, Pair<String, Instant>>()

    // Interpreted read markers, transformed to what has to be displayed in the client
    private val visibleReadMarkers = mutableMapOf<_MatrixID, Pair<String, Instant>>()
    private val visibleReadMarkersEventIdMap = mutableMapOf<String, MutableList<Pair<_MatrixID, Instant>>>()

    private val eventIdClientUserListener = mutableMapOf<String, () -> Unit>()
    private val eventIdOtherContactsListener = mutableMapOf<String, () -> Unit>()

    fun updateReadMarker(eventId: String, user: _MatrixID, timestamp: Instant) {
        originalReadMarkers[user] = Pair(eventId, timestamp)

        val oldEventId = visibleReadMarkers[user]?.first
        visibleReadMarkers.remove(user)

        visibleReadMarkersEventIdMap[oldEventId]?.removeIf { it.first == user }

        val newEventId = backlogManager.getLastConsecutiveEventOfUser(eventId, user) ?: eventId

        if (user == currentUser && backlogManager.isLastKnownEvent(newEventId)) {
            callListeners()
            return
        }

        visibleReadMarkers[user] = Pair(newEventId, timestamp)

        val newUser = Pair(user, timestamp)
        visibleReadMarkersEventIdMap.getOrPut(newEventId) { mutableListOf() }.add(newUser)

        callListeners()
    }

    private fun callListeners() {
        eventIdClientUserListener.forEach { it.value() }
        eventIdOtherContactsListener.forEach { it.value() }
    }

    fun updateAllReadMarkers() {
        originalReadMarkers.forEach { updateReadMarker(it.value.first, it.key, it.value.second) }
    }

    fun onChangeClientUser(eventId: String, listener: () -> Unit) {
        eventIdClientUserListener[eventId] = listener
    }

    fun removeListenerClientUser(eventId: String) {
        eventIdClientUserListener.remove(eventId)
    }

    fun onChangeOtherContacts(eventId: String, listener: () -> Unit) {
        eventIdOtherContactsListener[eventId] = listener
    }

    fun removeListenerOtherContacts(eventId: String) {
        eventIdOtherContactsListener.remove(eventId)
    }

    fun showReadMarker(eventId: String, user: _MatrixID?): Boolean {
        return visibleReadMarkers[user]?.first == eventId
    }

    /*
     * Adds a read marker from the client itself, before the server has informed us about this specific read marker
     */
    fun addLocalReadMarker(eventId: String, user: _MatrixID): Boolean {
        val newEvent = eventRepo.getEvent(eventId)
        val oldEventId = originalReadMarkers[user]?.first
        if (oldEventId != null) {
            val oldEvent = eventRepo.getEvent(oldEventId)
            if (oldEvent != null &&
                newEvent != null &&
                oldEvent.originServerTimestamp < newEvent.originServerTimestamp) {
                updateReadMarker(eventId, user, Instant.now())
                return true
            }
        }
        return false
    }

    fun isReadMarkerInRange(user: _MatrixID, range: IntRange): Boolean {
        val eventId = visibleReadMarkers[user]?.first
        if (eventId != null) {
            val rowIndex = backlogManager.getRowIndex(eventId)
            if (range.contains(rowIndex)) {
                return true
            }
        }
        return false
    }

    fun getReadMarkersOfOtherUsers(eventId: String): MutableList<Pair<_MatrixID, Instant>>? {
        return visibleReadMarkersEventIdMap[eventId]
    }
}