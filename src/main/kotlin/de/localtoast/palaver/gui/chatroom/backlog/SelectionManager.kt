/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom.backlog

import javafx.beans.property.SimpleObjectProperty
import javafx.scene.input.Clipboard
import javafx.scene.input.ClipboardContent
import org.fxmisc.flowless.Cell
import org.fxmisc.flowless.VirtualFlow

class SelectionManager(private val list: VirtualFlow<BacklogRow, Cell<BacklogRow, *>?>) {
    private val selection = SimpleObjectProperty<Selection?>(null)

    init {
        selection.addListener { _, oldValue, newValue ->
            val oldStart = min(oldValue?.from, oldValue?.to)
            val oldEnd = max(oldValue?.from, oldValue?.to)

            if (newValue?.from != null && newValue.to == null) {
                // User has clicked somewhere without dragging the mouse
                when {
                    oldStart != null && oldEnd != null -> IntRange(oldStart.cellIndex, oldEnd.cellIndex)
                        .forEach {
                            (list.getCell(it) as BacklogCell).clearSelection()
                        }
                    oldStart != null -> ((list.getCell(oldStart.cellIndex)) as BacklogCell).clearSelection()
                    oldEnd != null -> ((list.getCell(oldEnd.cellIndex)) as BacklogCell).clearSelection()
                }
            } else {

                val newStart = min(newValue?.from, newValue?.to)
                val newEnd = max(newValue?.from, newValue?.to)

                clearOldSelectionParts(oldStart, oldEnd, newStart, newEnd)

                updateSelection(newValue?.from, newValue?.to)
            }
        }
    }

    private fun clearOldSelectionParts(
        oldStart: SelectionIndex?,
        oldEnd: SelectionIndex?,
        newStart: SelectionIndex?,
        newEnd: SelectionIndex?
    ) {
        val oldCellRange = if (oldStart != null && oldEnd != null) {
            IntRange(oldStart.cellIndex, oldEnd.cellIndex)
        } else IntRange.EMPTY
        val newCellRange = if (newStart != null && newEnd != null) {
            IntRange(newStart.cellIndex, newEnd.cellIndex)
        } else IntRange.EMPTY

        oldCellRange.subtract(newCellRange)
            .forEach {
                (list.getCell(it) as BacklogCell).clearSelection()
            }
    }

    fun setSelectionStart(index: SelectionIndex?) {
        selection.value = Selection(index, selection.value?.to)
    }

    fun setSelectionEnd(index: SelectionIndex?) {
        selection.value = Selection(selection.value?.from, index)
    }

    fun updateSelection(index: Int, cell: BacklogCell) {
        val startIndex = selection.value?.from
        val endIndex = selection.value?.to
        updateCell(startIndex, endIndex, cell, index, getCellRange(startIndex, endIndex))
    }

    private fun updateSelection(start: SelectionIndex?, end: SelectionIndex?) {
        if (start != null && end != null) {
            val cellRange = getCellRange(start, end)
            cellRange.forEach {
                val cell = list.getCell(it) as BacklogCell?
                updateCell(start, end, cell, it, cellRange)
            }
        }
    }

    private fun updateCell(
        start: SelectionIndex?,
        end: SelectionIndex?,
        cell: BacklogCell?,
        cellIndex: Int,
        cellRange: IntRange
    ) {

        val smallerIndex = min(start, end)
        val biggerIndex = max(start, end)
        if (cell != null && cellRange.contains(cellIndex)) {
            if (smallerIndex != null && biggerIndex != null) {
                val startIndexInCell = when (cellIndex) {
                    cellRange.first() -> smallerIndex.indexInCell
                    else -> SelectionIndexInCell(0, 0)
                }

                val endIndexInCell = when (cellIndex) {
                    cellRange.last() -> biggerIndex.indexInCell
                    else -> {
                        val lastTextFlowIndex = cell.lastTextFlowIndex()
                        val lastTextLength = cell.lastTextLength()
                        if (lastTextFlowIndex != null && lastTextLength != null) {
                            SelectionIndexInCell(lastTextFlowIndex, lastTextLength)
                        } else null
                    }
                }

                if (endIndexInCell != null) {
                    cell.setSelection(startIndexInCell, endIndexInCell)
                }
            } else {
                cell.clearSelection()
            }
        }
    }

    private fun getCellRange(start: SelectionIndex?, end: SelectionIndex?): IntRange {
        val smallerIndex = min(start, end)
        val biggerIndex = max(start, end)

        return if (smallerIndex != null && biggerIndex != null) {
            IntRange(smallerIndex.cellIndex, biggerIndex.cellIndex)
        } else IntRange.EMPTY
    }

    private fun max(value1: SelectionIndex?, value2: SelectionIndex?): SelectionIndex? {
        if (value1 != null && value2 != null) {
            return if (value1 > value2) value1 else value2
        } else if (value1 != null) {
            return value1
        } else if (value2 != null) {
            return value2
        }
        return null
    }

    private fun min(value1: SelectionIndex?, value2: SelectionIndex?): SelectionIndex? {
        if (value1 != null && value2 != null) {
            return if (value1 < value2) value1 else value2
        } else if (value1 != null) {
            return value1
        } else if (value2 != null) {
            return value2
        }
        return null
    }

    fun clearSelection() {
        selection.value = null
    }

    fun copyCurrentSelectionToClipboard() {
        val start = min(selection.value?.from, selection.value?.to)
        val end = max(selection.value?.from, selection.value?.to)
        if (start != null && end != null) {
            var text = ""
            for (cellIndex in start.cellIndex..end.cellIndex) {
                val cell = list.getCell(cellIndex) as BacklogCell

                if (text.isNotBlank()) {
                    text += "\n"
                }

                text += cell.getText(if (cellIndex == start.cellIndex) start else null,
                    if (cellIndex == end.cellIndex) end else null)
            }
            if (text.isNotBlank()) {
                val clipboard = Clipboard.getSystemClipboard()
                val content = ClipboardContent()
                content.putString(text)
                clipboard.setContent(content)
            }
        }
    }
}