/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.stylesheet

import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.c
import tornadofx.cssclass
import tornadofx.mixin
import tornadofx.multi
import tornadofx.pt
import tornadofx.px

class SideBarStyle : Stylesheet() {
    companion object {
        val cellPaneWidth = 235.px

        val paneBackground by cssclass()

        val view by cssclass()
        val icon by cssclass()
        val iconBorder by cssclass()
        val cellText by cssclass()

        val shortcut by cssclass()
        val counterUnread by cssclass()
        val counterHighlight by cssclass()

        val selectionMarker by cssclass()

        val contactCategoryTitleFont by cssclass()

        val filterBox by cssclass()
        val filterBoxPadding by cssclass()
        val filterBoxInputField by cssclass()
    }

    init {
        paneBackground {
            prefWidth = cellPaneWidth
            maxWidth = cellPaneWidth
            minWidth = cellPaneWidth
            backgroundColor += PalaverStyle.darkBackground
        }

        view {
            +PalaverStyle.scrollBarStyleMixin
            +PalaverStyle.scrollBarDarkBackgroundMixin

            // Remove list view's border
            backgroundInsets += box(0.px)

            cell {
                backgroundInsets += box(0.px)
                padding = box(0.px)
                borderWidth += box(0.px, 0.px, 0.px, 0.px)
                backgroundColor += PalaverStyle.darkBackground

                selectionMarker {
                    minWidth = 3.px
                    prefWidth = 3.px
                    prefHeight = 25.px
                    backgroundColor += Color.TRANSPARENT
                }

                cellText {
                    opacity = 0.7
                    +FontStyle.regularMixin
                    fontSize = 12.px
                    textFill = Color.WHITE
                }

                shortcut {
                    opacity = 0.7
                    +FontStyle.boldMixin
                    fontSize = 9.px
                    textFill = Color.WHITE
                    borderWidth += box(1.px)
                    borderColor += box(Color.WHITE)
                    borderRadius += box(10.px)

                    padding = box(0.px, 3.px, 0.px, 4.px)
                }

                val counterMixin = mixin {
                    +FontStyle.boldMixin
                    fontSize = 10.px
                    textFill = Color.WHITE

                    backgroundRadius += box(15.px)
                    padding = box(2.px, 6.px, 2.px, 6.px)
                }

                counterUnread {
                    +counterMixin
                    backgroundColor += c("#7e7e7e")
                }

                counterHighlight {
                    +counterMixin
                    backgroundColor += c("#006fb5")
                }

                icon {
                    PalaverStyle.iconColor.value = Color.GREY
                }
                iconBorder {
                    fill = c("#737980")
                }

                and(selected) {
                    cellText {
                        opacity = 1.0
                    }
                    shortcut {
                        opacity = 1.0
                    }
                    backgroundColor = multi(c("#404953"))
                    selectionMarker {
                        backgroundColor = multi(PalaverStyle.lightBlue)
                    }
                }

                and(filled) {
                    and(hover) {
                        backgroundColor += c("#404953")
                    }
                }
            }
        }

        contactCategoryTitleFont {
            +FontStyle.boldMixin
            fontSize = 9.pt
            padding = box(10.px, 0.px, 5.px, 10.px)
            textFill = c("#e8e8e8")
        }

        filterBox {
            borderWidth += box(1.px)
            borderColor += box(c("b3b3b3"))
            borderRadius += box(15.px)
            borderInsets += box(10.px)
            backgroundColor += Color.TRANSPARENT
        }

        filterBoxPadding {
            padding = box(1.px, 12.px, 1.px, 12.px)
        }

        filterBoxInputField {
            +FontStyle.regularMixin
            textFill = c("#b3b3b3")
            fontSize = 12.px

            backgroundColor += PalaverStyle.darkBackground

            textBoxBorder = Color.TRANSPARENT

            and(focused) {
                borderWidth += box(0.px)
                focusColor = Color.TRANSPARENT
                faintFocusColor = Color.TRANSPARENT
            }
        }
    }
}