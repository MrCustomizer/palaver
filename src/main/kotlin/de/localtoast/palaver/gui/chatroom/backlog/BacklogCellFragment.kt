/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.chatroom.backlog

import de.localtoast.palaver.gui.FadingController
import de.localtoast.palaver.gui.OverlayWithPointer
import de.localtoast.palaver.gui.stylesheet.ChatRoomStyle
import de.localtoast.palaver.gui.stylesheet.TextStyles
import de.localtoast.palaver.gui.text.EventNode
import de.localtoast.palaver.sys.DateTimeHelper
import de.localtoast.palaver.sys.PalaverHttpClient
import de.localtoast.palaver.sys.chatroom.ReadMarkerManager
import de.localtoast.palaver.sys.events.Event
import de.localtoast.palaver.sys.users.User
import de.localtoast.palaver.sys.users.UserRepository
import javafx.application.Platform
import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Pos
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.image.ImageView
import javafx.scene.layout.FlowPane
import javafx.scene.layout.HBox
import javafx.scene.layout.Pane
import javafx.scene.layout.Priority
import javafx.scene.layout.Region
import javafx.scene.layout.VBox
import javafx.util.Duration
import org.fxmisc.flowless.Cell
import org.fxmisc.flowless.VirtualFlow
import tornadofx.DefaultScope
import tornadofx.Fragment
import tornadofx.addClass
import tornadofx.box
import tornadofx.find
import tornadofx.hboxConstraints
import tornadofx.onChange
import tornadofx.onChangeOnce
import tornadofx.px
import tornadofx.removeClass
import tornadofx.style
import tornadofx.vbox
import tornadofx.vboxConstraints
import java.time.Instant

class BacklogCellFragment : Fragment() {
    val row: BacklogRow by param()
    val parentList: VirtualFlow<BacklogRow, Cell<BacklogRow, *>?> by param()
    val readMarkerManager: ReadMarkerManager by param()
    val readMarkerOverlayParam: OverlayWithPointer? by param()
    val showReadMarkerOverlay: (OverlayWithPointer, Int) -> Unit by param()

    private val userRepo: UserRepository by inject()
    private val client: PalaverHttpClient by inject()

    private val messageRow: MessageRow?
    private val eventRow: EventRow?
    private val dayRow: DayRow?

    private val eventNode: EventNode?

    private var readMarkerBox: FlowPane? = null
    private var index = 0
    private var readMarkerOverlay: OverlayWithPointer? = readMarkerOverlayParam

    init {
        val tempRow = row

        messageRow = tempRow as? MessageRow
        eventRow = tempRow as? EventRow
        dayRow = tempRow as? DayRow

        eventNode = if (tempRow is EventRow) {
            find(tornadofx.DefaultScope,
                mapOf(EventNode::eventParam to tempRow.event))
        } else null
    }

    override val root: Pane = createPane()

    fun updateIndex(index: Int) {
        this.index = index
    }

    fun getOverlayTarget(index: Int): Node? {
        val box = readMarkerBox
        return when {
            box != null && box.children.size > index -> box.children[index]
            else -> null
        }
    }

    fun getRowInternal(): BacklogRow {
        return row
    }

    fun lastTextFlowIndex(): Int? {
        return eventNode?.lastTextFlowIndex()
    }

    fun lastTextLength(): Int? {
        return eventNode?.lastTextLength()
    }

    fun nodeHit(screenX: Double, screenY: Double): Int? {
        return eventNode?.nodeHit(screenX, screenY)
    }

    fun characterHit(screenX: Double, screenY: Double): Int? {
        return eventNode?.characterHit(screenX, screenY)
    }

    fun setSelection(start: SelectionIndexInCell, end: SelectionIndexInCell) {
        eventNode?.setSelection(start, end)
    }

    fun clearSelection() {
        eventNode?.clearSelection()
    }

    fun getText(start: SelectionIndex?, end: SelectionIndex?): String {
        val text = if (messageRow != null && messageRow.header.value) {
            val sender = messageRow.event.sender
            "${userRepo.getUser(sender).name.value}: ${System.lineSeparator()}"
        } else ""
        return if (eventNode != null) text + eventNode.getText(start, end) else dayRow?.day ?: ""
    }

    fun dispose() {
        readMarkerOverlay?.pointerTarget?.value = null

        if (eventRow != null) {
            readMarkerManager.removeListenerClientUser(eventRow.event.id)
            readMarkerManager.removeListenerOtherContacts(eventRow.event.id)
        }
    }

    private fun createPane(): HBox {
        val hBox = HBox()
        val avatarHBox = HBox()

        val vBox = VBox().apply {
            hboxConstraints {
                hGrow = Priority.ALWAYS
            }
        }

        when {
            messageRow != null -> {
                avatarHBox.addClass(ChatRoomStyle.backlogAvatarBig)
                if (messageRow.header.value) {
                    createMessageRowHeader(messageRow, vBox, hBox, avatarHBox)
                    hBox.addClass(ChatRoomStyle.backlogHeaderPadding)
                } else {
                    // placeholder for padding
                    hBox.add(avatarHBox)
                }

                vBox.add(createContentRow(messageRow.event))
                vBox.add(createReadMarkerBoxClientUser(messageRow))
            }

            eventRow != null -> {
                avatarHBox.addClass(ChatRoomStyle.backlogAvatarMini)
                hBox.addClass(ChatRoomStyle.backlogStateRowPadding)

                val sender = userRepo.getUser(eventRow.event.sender)
                val imageView = ImageView().apply {
                    imageProperty().bind(sender.scaledAndCutOutAvatarMini)
                }
                avatarHBox.add(imageView)
                hBox.add(avatarHBox)

                vBox.add(createContentRow(eventRow.event))
                vBox.add(createReadMarkerBoxClientUser(eventRow))
            }

            dayRow != null -> {
                vBox.alignment = Pos.CENTER
                vBox.add(Label(dayRow.day).apply {
                    addClass(ChatRoomStyle.backlogDayRow)
                })
            }
        }
        hBox.add(vBox)
        return hBox
    }

    private fun createMessageRowHeader(messageRow: MessageRow, vBox: VBox, hBox: HBox, avatarHBox: HBox) {
        val user = userRepo.getUser(messageRow.event.sender)

        val usernameLabel = Label().apply {
            user.name.onChange {
                Platform.runLater {
                    text = it
                }
            }
            text = user.name.value
            addClass(ChatRoomStyle.backlogUser)
        }
        vBox.add(usernameLabel)

        hBox.add(avatarHBox)
        val imageView = ImageView().apply {
            imageProperty().bind(user.scaledAndCutOutAvatar)
        }
        avatarHBox.add(imageView)
        // TODO automatic fetching, if avatar is accessed?
        user.fetchMetadata(true, true)

        /*
         * Needed when loading backlog. It can happen, that some messages of the same
         * block get fetched and therefore the current header looses its header status.
         */
        messageRow.header.onChangeOnce {
            if (it == false) {
                val offset = usernameLabel.height
                vBox.children.remove(usernameLabel)
                avatarHBox.children.remove(imageView)
                hBox.removeClass(ChatRoomStyle.backlogHeaderPadding)

                // Correct the scrolling position
                parentList.showAtOffset(this@BacklogCellFragment.index, 2 * offset)
            }
        }
    }

    private fun createReadMarkerBoxClientUser(row: EventRow): HBox {
        val readMarkerBox = HBox().apply {
            vboxConstraints {
                vGrow = Priority.ALWAYS
            }
            addClass(ChatRoomStyle.readMarkerBox)
        }

        when {
            readMarkerManager.showReadMarker(row.event.id, client.user) -> readMarkerBox.opacityProperty().value = 1.0
            else -> readMarkerBox.opacityProperty().value = 0.0
        }
        val readMarkerFadingController = FadingController(readMarkerBox)
        readMarkerManager.onChangeClientUser(row.event.id) {
            Platform.runLater {
                val showMarker = readMarkerManager.showReadMarker(row.event.id, client.user)
                when {
                    readMarkerBox.opacity != 1.0 && showMarker ->
                        readMarkerFadingController.fadeIn(Duration.millis(800.0))
                    readMarkerBox.opacity != 0.0 && !showMarker ->
                        readMarkerFadingController.fadeOut(Duration.millis(400.0))
                }
            }
        }
        return readMarkerBox
    }

    private fun createContentRow(event: Event): HBox {

        return HBox().apply {
            hboxConstraints {
                hGrow = Priority.ALWAYS
            }

            add(createContentBox(eventNode))
            add(createTimestampBox(event))
            add(createAvatarReadMarkerBox(event))
        }
    }

    private fun createContentBox(eventNode: EventNode?): HBox {
        return HBox().apply {
            hboxConstraints {
                hGrow = Priority.ALWAYS
            }

            addClass(ChatRoomStyle.backlogMessagePadding)

            if (eventNode != null) {
                vbox {
                    add(eventNode)
                }
            }
        }
    }

    private fun createTimestampBox(event: Event): HBox {
        return HBox().apply {
            val widthVal = 80.0
            minWidth = widthVal
            maxWidth = widthVal
            prefWidth = widthVal

            val timestampLabel = Label(DateTimeHelper.formatTimestampTime(event.originServerTimestamp))
            timestampLabel.addClass(ChatRoomStyle.backlogTimestamp)
            add(timestampLabel)
        }
    }

    private fun createAvatarReadMarkerBox(event: Event): FlowPane {
        val box = FlowPane().apply {
            val widthVal = 120.0
            minWidth = widthVal
            maxWidth = widthVal
            prefWidth = widthVal

            alignment = Pos.BOTTOM_LEFT

            style {
                padding = box(0.px, 10.px, 0.px, 0.px)
            }
        }

        readMarkerBox = box

        updateAvatarReadMarkers(event.id, box)

        readMarkerManager.onChangeOtherContacts(event.id) {
            Platform.runLater {
                box.children.clear()
                updateAvatarReadMarkers(event.id, box)
            }
        }

        return box
    }

    private fun updateAvatarReadMarkers(eventId: String, box: FlowPane) {
        val users = readMarkerManager.getReadMarkersOfOtherUsers(eventId)
            ?.filter { it.first != client.user }
            ?.map { Pair(userRepo.getUser(it.first), it.second) }

        users?.forEach {
            tornadofx.runAsync(daemon = true) {
                it.first.fetchMetadata(true, true)
            }
        }

        val maxAvatarCount = 5

        users?.take(maxAvatarCount)?.forEach {
            val imageHBox = HBox().apply {
                style {
                    padding = box(0.px, 1.px, 0.px, 0.px)
                }
            }
            val imageView = ImageView().apply {
                imageProperty().bind(it.first.scaledAndCutOutAvatarMini)
            }
            imageHBox.add(imageView)
            box.children.add(imageHBox)
            imageHBox.setOnMousePressed {
                createReadMarkerOverlayWindow(imageHBox, box.children.indexOf(imageHBox), users)
            }
        }

        if (users != null && users.size > maxAvatarCount) {
            val label = Label("+${users.size - maxAvatarCount}").apply {
                addClass(TextStyles.message)
                style {
                    padding = box(0.px, 0.px, 0.px, 3.px)
                }
            }
            box.children.add(label)

            label.setOnMousePressed {
                createReadMarkerOverlayWindow(label, box.children.indexOf(label), users)
            }
        }

        if (box.children.isEmpty()) {
            // no avatars left, the avatar window can point to
            readMarkerOverlay?.dispose()
        }
    }

    private fun createReadMarkerOverlayWindow(
        target: Region,
        targetIndex: Int,
        users: List<Pair<User, Instant>>
    ) {
        val readMarkerList = tornadofx.find<ReadMarkerListPanel>(DefaultScope,
            mapOf(ReadMarkerListPanel::users to users))

        val overlayPanelHeight = Math.min(350.0, users.size * ReadMarkerListPanel.cellHeight + 95.0)

        val overlay = tornadofx.find<OverlayWithPointer>(DefaultScope,
            mapOf(OverlayWithPointer::pointerTarget to SimpleObjectProperty<Region?>(target),
                OverlayWithPointer::prefWidth to 300.0,
                OverlayWithPointer::prefHeight to overlayPanelHeight,
                OverlayWithPointer::content to readMarkerList.root))

        readMarkerOverlay = overlay

        showReadMarkerOverlay(overlay, targetIndex)
    }
}