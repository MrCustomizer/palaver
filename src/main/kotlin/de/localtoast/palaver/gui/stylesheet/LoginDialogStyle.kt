/*
 * Palaver - A Matrix client written in Kotlin
 * Copyright (C) 2018 Arne Augenstein
 *
 * https://gitlab.com/MrCustomizer/palaver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

package de.localtoast.palaver.gui.stylesheet

import javafx.scene.layout.BackgroundSize
import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.c
import tornadofx.cssclass
import tornadofx.pt
import tornadofx.px
import java.net.URI

class LoginDialogStyle : Stylesheet() {
    companion object {
        val backgroundImage by cssclass()
        val imageTitle by cssclass()
        val imageText by cssclass()

        val font by cssclass()
        val errorFont by cssclass()
    }

    init {
        backgroundImage {
            val imagePath = "/de/localtoast/palaver/gui/login/loginImage.jpg"
            backgroundImage += URI(imagePath)
            backgroundSize += BackgroundSize(100.0, 100.0,
                true, true, false, true)
        }

        imageTitle {
            +FontStyle.lightMixin
            textFill = Color.WHITE
            fontSize = 25.pt
            padding = box(60.px, 0.px, 0.px, 0.px)
        }

        imageText {
            +FontStyle.lightMixin
            textFill = Color.WHITE
            fontSize = 13.pt
        }

        font {
            +FontStyle.regularMixin
        }

        errorFont {
            +FontStyle.regularMixin
            fill = c("#b50000")
        }
    }
}